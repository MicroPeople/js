// setTimeout(() => {
//     console.log("1 second has passed");
// }, 1000);

// setTimeout(function() {
//     console.log("2 seconds have passed");
// }, 2000);

// var time = 0;

// var timer = setInterval(() => {
//     time += 2;
//     console.log(time + " seconds have passed");
//     if (time > 5) {
//         clearInterval(timer);
//     }
// }, 1000);

// function sayHi() {
//     console.log("Hi");
// }

// sayHi();

// var sayBye = function() {
//     console.log("Bye");
// };

// sayBye();

// function callFunction(fun, name) {
//     fun(name);
// }

// callFunction(function(name) {
//     console.log(name + " Bye");
// }, "rails365");

// var stuff = require("./count.js");
// var pi = require("./count.js").pi;

// console.log(stuff.counter(["ruby", "nodejs", "react"]));
// console.log(stuff.adder(3, 2));
// console.log(pi);

//-----------------------event-----------------------
// element.on("click", function() {
// 	console.log("clicked");
// });

var events = require("events");
var util = require("util");

var Person = function(name) {
	this.name = name;
};
util.inherits(Person, events.EventEmitter);

var xioaming = new Person("xioaming");
var Lucy = new Person("Lucy");
var Lili = new Person("Lili");

var person = [xioaming, Lucy, Lili];

person.forEach(function(person) {
	person.on("speak", function(message) {
		console.log(person.name + " said: " + message);
	});
});

xioaming.emit("speak", "Hi");
Lucy.emit("speak", "I want a curry");

var myEmitter = new events.EventEmitter();
myEmitter.on("someEvent", function(message) {
	console.log(message);
});

myEmitter.emit("someEvent", "the event was emitted");
var fs = require("fs");
var readme = fs.readFileSync("README.txt", "utf8");
var readme = fs.readFile("README.txt", "utf8", function(err, data) {
	console.log(data);
});
console.log(readme);
fs.writeFileSync("writeMe.txt", readme);

var waitTill = new Date(new Date().getTime() + 4 * 1000);
while (waitTill > new Date()) {}
console.log("finished");

fs.unlink("writeMe.txt", function() {
	console.log("delete");
});

var myReadStream = fs.createReadStream(__dirname + "/README.txt", "utf8");
var myWriteStream = fs.createWriteStream(__dirname + "/writeMe.txt", "utf8");
myReadStream.pipe(myWriteStream);
myReadStream.on("data", function(chunk) {
	console.log("new chunk received");
	console.log(chunk);
});

var zlib = require("zlib");

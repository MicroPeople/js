while (!device.isScreenOn()) {
    device.wakeUp();
    sleep(500)
    swipe(700, 1900, 700, 450, 300);//上滑
    sleep(400);
    //解锁 密码123456
    desc(1).findOne().click();
    desc(2).findOne().click();
    desc(3).findOne().click();
    desc(4).findOne().click();
    desc(5).findOne().click();
    desc(6).findOne().click();
    sleep(400);
}

var w = floaty.rawWindow(
    <frame gravity="center" bg="#CC999999">
        <text padding="5 0 5 0" w="auto" h="auto" id="text" textColor='#FFFFFF'></text>
    </frame>
);

w.setPosition(150, 5);
w.setTouchable(false);
ui.run(function () {
    w.text.setText("正在打开知到");
});

while (currentPackage().search(/wisdomtree/i) == -1) { //如果没有找到任何匹配的子串，则返回 -1。
    launchApp("知到");
    sleep(1000);
}

if (id('tv_skip').findOne().click()) {
    ui.run(function () {
        w.text.setText('跳过广告');
    });
    sleep(500);
}

var widget = text("学习").findOne();
//获取其中心位置并点击
click(widget.bounds().centerX() + random(0, 10), widget.bounds().centerY() - 40);
ui.run(function () {
    w.text.setText('点击学习')
});
sleep(4000);

if (id('rl_credit_all_info').findOne().click()) {
    ui.run(function () {
        w.text.setText('点击形式与政策');
    });
    sleep(2000);
}

sleep(500);
id("continue_study_btn").findOne().click();
sleep(500);
click(600, 600);

ui.run(function () {
    w.text.setText('继续播放');
});
sleep(1000);

var number = 0//设置音量
device.setMusicVolume(device.getMusicMaxVolume() / 100 * number)

click(800, 800); sleep(500);
click(1000, 800);

ui.run(function () {
    w.text.setText('放大');
});

/*思路：
问题一：弹题
    一直检测，是否有题，有题就答

问题二：第二次倍速
    1.间隔时间段后检测倍速
    2.不调倍速

问题三：怎么判断两个视频播放完毕
    3.第三次调倍速就退出脚本
*/
n = 1; t = 0;  //n弹题个数 t播放视频的集数（调倍速的次数）
while (true) {
    if (text('A').findOnce()) {
        sleep(500)
        click("A");
        sleep(1000)
        click("关闭")
        sleep(1000)

        ui.run(function () {
            w.text.setText('关闭' + n + '个弹题');
        });
        n = n + 1;
    }
    if (text('1.0x').findOnce()) {
        click("1.0x")
        t += 1;
        sleep(500);
        click("1.25x"); sleep(500);
        ui.run(function () {
            w.text.setText('第' + t + '个视频');
        });
    }
    if (t == 4) {

        ui.run(function () {
            w.text.setText('第4个视频，准备退出');
        });

        back()
        sleep(500);
        back()
        sleep(500);
        if (id('positiveButton').text("确定").findOnce()) {
            ui.run(function () {
                w.text.setText('确定退出');
            });
            click("确定")
        }
        back()
        sleep(500);
        back()
        sleep(500);
        home()
        exit()

    }
}

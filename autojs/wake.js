closeP();

function closeP() {
    var h = confirm("是否要进入息屏挂机模式吗？可按下音量上键停止辅助。并不能锁定屏幕！！！通知栏会正常显示（oled屏可用)");
    if (h) {
        var w = floaty.rawWindow(
            <frame gravity="center" bg="#000000" />
        );

        w.setSize(-1, -1);
        w.setTouchable(true);
        //保持脚本运行
        setInterval(() => { }, 1000);
    }
}

var pass = "1024" //解锁密码
while (true) {
    Unlock();
}

function Unlock() {
    if (!device.isScreenOn()) {
        device.wakeUp();
    } else if (packageName("com.android.systemui").text("输入密码").findOnce()) {
        sleep(500);
        for (var i = 0; i < pass.length; i++) {
            var aa = text(pass[i].toString()).findOne().bounds();
            click(aa.centerX(), aa.centerY());
        }
    } else if (packageName("com.android.systemui").findOnce()) {
        sleep(500)
        swipe(360, 600, 360, 0, 100);
    }


    //怎么用其他按键强行停止脚本
    events.onKeyDown("home", function (event) {//按home停止
        toast("程序结束")
        console.hide()
        threads.shutDownAll()

    })

    events.onKeyDown("volume_down", function (event) {//按音量下停止
        toast("程序结束")
        console.hide()
        threads.shutDownAll()

    })


    //遇到ui堵塞怎么办?
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
//之后你sleep(5000),这类就不会再提示你堵塞了

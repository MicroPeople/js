#include<stdio.h>
//标准输入输出
#include<stdlib.h>
//标准库函数头文件malloc()、free()、system()、rand()、srand()、exit()
#include <string.h>
//strcpy()
#define INF 99999
#define didian_num 5

//图的邻接表存储表示
typedef struct didian {
	int id;					//地点编号
	int juli;			//据顶点的距离
	struct didian *next;	//指针
}node;

node *Map[didian_num];// 地点 数组

//先创建 地图的邻接表  通过读取文件
void InitMap() {

	FILE *fp;
	fp = fopen("ditu.txt", "r"); //r 打开一个已有的文本文件，允许读取文件。
	if (fp == NULL) {
		printf("文件打开错误!\n");
		exit(1);		//表示异常退出
	}

	int num = 0;	//记录文件里有几行，与didian_num相同//链表的总数，即文件的行数，即有几个顶点
	int first = 0;	//标记是不是头结点	0是 1否

	while (!feof(fp)) {
		node *V;
		V = (node*)malloc(sizeof(node));      //申请头结点空间 V指向这个空间

		char c = fgetc(fp);	//读取一个字符
		if (feof(fp))break;	//到文件结尾就要及时退出循环
		V->id = c - '0';	//将char型变为int型
		//printf(" id:%d ", V->id);
		V->next = NULL;

		c = fgetc(fp);
		if (feof(fp))break;
		if ((int)c == 32)	//如果是空格，说明该数据结束
		{
			c = fgetc(fp);
			if (feof(fp))break;
			V->juli = c - '0';//将char型变为int型
		}
		else {	//未遇见空格，id未结束
			while ((int)c != 32) {	//id若不是个位数，字符就会被分开读取
				V->id = V->id * 10 + (c - '0');	//遇到空格之前的字符，都是该id的字符

				c = fgetc(fp);
				if (feof(fp))break;
				//读取到空格，说明id数据结束，跳出循环
			}
			c = fgetc(fp);
			if (feof(fp))break;
			V->juli = c - '0';//将char型变为int型
		}
		printf(" id:%d \n", V->id);

		c = fgetc(fp);
		if (feof(fp))break; 
		while ((int)c != 32){	//距离若不是个位数，字符就会被分开读取
			V->juli = V->juli * 10 + (c - '0');	//遇到空格之前的字符，都是该距离的字符
			//printf(" juli:%d \n", V->juli);

			c = fgetc(fp);
			if (feof(fp))break;
			if ((int)c == 10)break;//读取到回车 或 空格，说明juli数据结束，跳出循环
		}
		printf(" juli:%d ", V->juli);//该节点结束，插入链表

		//将其他点插入 初始点之后 且逆序
		if (first == 1) { //非头结点
			V->next = Map[num]->next;
			Map[num]->next = V;
		}
		else {//头结点插入的时候 Map[num]=V

			Map[num] = V; 
			first = 1;	//头结点标记 为1 ，之后的都不是头结点
		}

		if (c == '\n')//如果是回车，说明该链表结束
		{
			num++;		//链表的总数，即文件的行数，即有几个顶点
			first = 0;	//头结点标记 为0 ，下一个数据是下一个链表的头结点
			printf("|\n");
		}
 
		if (feof(fp))break;	//到文件结尾就要及时退出循环
	}

	printf(" num:%d\n ", num);
	fclose(fp);	//关闭文件


	printf(" 输出邻接表：\n");

	for (int i = 0; i < didian_num; i++) {
		printf("id:%d ", Map[i]->id);
		printf("juli:%d ", Map[i]->juli);
		node *t = Map[i]->next;
		while (t) {

			printf(" -id:%d juli:%d ", t->id, t->juli);
			t = t->next;
		}
		printf("\n\n");
	}	
}
/*
*	获取中间结点到其他节点的距离
*/
int getJuli(int middle, int j) {
	node *p = Map[middle];

	while (p) {
		if (p->id == j)  return p->juli; 	//中间点和其他点 有路，返回距离
		p = p->next;
	}
	//printf("noway!\n");
	return INF;	//中间点和其他点 没有路，返回INF
}
/*
*	迪杰斯特拉算法-最短路径
*	parent要传参，不能在函数体内定义返回
*/
void ShortestPath_DIJ(int start, int parent[didian_num]) {
	//给数组赋初值
	int visited[didian_num], dist[didian_num];
	for (int i = 0; i < didian_num; i++)
	{
		visited[i] = 0;	//是否访问过，就是是否已经找到它到初始点的最短距离
		dist[i] = INF;	//每个点到初始点的最短距离
		parent[i] = -1;	//找到最短距离后，该点到初始点的路上 经过的第1个点
	}

	//初始点
	node *p = Map[start];
	while (p) {
		dist[p->id] = p->juli;		//其他点到初始点的距离
		//printf("dist%d:%d \n", p->id,  dist[p->id]);
		parent[p->id] = start;		//走最短距离的父节点
		p = p->next;		//遍历 其他点
	}
	visited[start] = 1;		//标记起始点已访问
	dist[start] = 0;		//初始点到初始点的距离为0
	parent[start] = -1;		//


	int middle = 0;	//中间节点
	int min_dist = INF;//中间节点：dist最小-距离起始点最近

	for (int i = 0; i < didian_num; i++)
	{
		//在所有未访问的节点中找中间节点：dist最小-距离起始点最近
		min_dist = INF;
		for (int j = 0; j < didian_num; j++)//寻找未被访问节点中 距离起始点最近的节点
		{
			if (visited[j] == 0 && dist[j] < min_dist)
			{
				min_dist = dist[j];//找到最小dist
				middle = j;			//找到中间节点
			}
		}
		visited[middle] = 1;	//标记 中间节点 已访问

		//找 起始点 经过中间节点 到其他可达点 的距离


		for (int j = 0; j < didian_num; j++)//经过 中间节点 ，找（最短）距离
		{
			if (visited[j] == 0 && dist[middle] + getJuli(middle,j) < dist[j])//dist[middle] + 从中间点到其他点的距离 < dist[j]
			{
				dist[j] = dist[middle] + getJuli(middle, j);
				parent[j] = middle;
			}
			//else printf("%d+%d    %d\n", dist[middle], getJuli(middle, j), dist[j]);
		}
	}
	/*printf("某点的父节点：\n");
	printf("该点有父节点，说明它找到了到初始点的最短路径，通过父节点就能到初始点\n");
	for (int i = 0; i < didian_num; i++)
		printf(" --- id:%d parent:%d \n",i, parent[i]);*/
}
/*
*	得到最短路径
*	通过迪杰斯特拉算法得到的parent数组，能得到最短路径的倒序
*	本函数将最短路径的正序放入path
*/
void Path(int start, int end, int parent[didian_num], int* path) {

	int t;	//临时，存储父节点的id
	int n=0;	//记录最短路径上有几个点，用来动态开辟数组空间

	//先判断两点间是否有通路
	if (parent[end] != -1) {	//该点有父节点，说明它找到了到初始点的最短路径，即有通路
	
		n++;
		t = parent[end];	//t = 它的父节点
		while (t != -1) {	//它的父节点不为-1，即不是初始点 就一直往回追溯
			n++;
			t = parent[t];	//t = 它的父节点
		}

	}
	else {
		printf("No Way!");	//没有通路
		return;
	}

	int *nixu_path = (int*)malloc(sizeof(int) * n);	//动态开辟空间-逆序

	/*上边是计算 n ，下边将倒序路径 变 正序*/
	int i = 1;

	nixu_path[0] = end;	//先将终止点存入路径
	t = parent[end];	//t = 终止点的父节点
	while (t != -1) {	//它的父节点不为-1，即不是初始点 就一直往回追溯

		nixu_path[i++] = t;	//所以t都是最短路径上的点，都需要存
		t = parent[t];//t = 它的父节点
	}

	path = (int*)malloc(sizeof(int)*n);			//动态开辟空间-正序
	for (i = 0; i < n; i++) {
		path[i] = nixu_path[n-i-1];		//将nixu_path数组逆序
	}

	free(nixu_path);	//释放逆序空间


	printf("最短路径：");
	for (i = 0;  i < n; i++) {
		printf(" %d ", path[i]);
		if(i == n-1) printf("\n");
		else printf("->");
	}
	free(path);//释放空间
}
/*
*	释放空间，十分重要
*	释放链表的空间，需要循环
*/
void Free(node *head)
{
	
		//printf(" id:%d ", head->id);
		node *p;
		while (head != NULL) {
			p = head;
			head = head->next;
			free(p);
		}
	
}
void main() {

	int parent[didian_num];	//父节点数组
	int *path = parent;		//指针需要初始化
	int start = -1, end = -1;	//起止点

	//创建 地图的邻接表
	InitMap();
	//打印地图

	//
	while (1) {
		printf("\n注：起点 = 终点，退出查询。\n");
		do {
			printf("请输入要查询的地点（起点 终点）：");
			scanf("%d %d", &start, &end);
			if (start == end)break;
			else if (start <0 || end <0 || start >=didian_num || end >=didian_num){
				printf("\n输入不合法，请重新输入\n");
				char c; 
				while ((c = getchar()) != '\n');//清空输入缓冲区
				start = -1; end = -2;
			}
		} while (start <0 || end <0 || start >didian_num || end >didian_num);
		if (start == end)break;

		ShortestPath_DIJ(start, parent);//找到了单源点到其他所有点的最短距离，但不一定存在通路
		Path(start, end, parent, path);	//判断两点间是否有通路，有就正序存放
					
		system("pause");//暂停
	}
	for (int i = 0; i < didian_num; i++)
		Free(Map[i]);	//释放空间,释放有问题的话，可以单次运行程序，循环就会有问题

	return;
}


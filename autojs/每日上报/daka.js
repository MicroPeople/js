
auto(); // 自动打开无障碍服务
    
/*判断屏幕锁定，解锁屏幕（数字密码）*/
if (!device.isScreenOn()) {//息屏状态将屏幕唤醒
    device.wakeUp();//唤醒设备
    sleep(1000); // 等待屏幕亮起

    swipe(700, 1900, 700, 450, 300);//上滑
    sleep(400);
   
    //解锁 密码123456
    desc(1).findOne().click();
    desc(2).findOne().click();
    desc(3).findOne().click();
    desc(4).findOne().click();
    desc(5).findOne().click();
    desc(6).findOne().click();
    //等待解锁完成，返回并退出
}
sleep(400);
if(currentActivity().search("com.tencent.mm") == -1){//当前 APP 不是微信

    launchApp("微信");//launch("com.tencent.mm");
    toast('微信已打开');
    sleep(6000);
}

click(100, 150);
sleep(500);
click(100, 150);
sleep(500);
click(100, 150);
sleep(500);//点击返回的位置，确保在主窗口，否则需要判断是否有打开的窗口

while(!click("通讯录"));sleep(500);    //这里是需要点击的text，自行修改
while(!click("中国海大"));sleep(1000); //因为是while()，所以只要没有点击到文字，就会一直循环
while(!click("每日上报"));sleep(4000); //
while(!click("每日填报"));sleep(4000); //这个sleep时间根据个人手机打开页面的速度调整

swipe(700, 2000, 700, 450, 2000);//下滑到获取位置的文本框
sleep(1500);

//click(350, 1519);             //之前是靠绝对？坐标点击的，因为控件点击那个文本框会出错
                                //但是前几天风险地区增加了，导致文本框的位置变了，于是又改为了相对坐标点击

weizhi=text("当前体温范围").findOnce().bounds()       //需要保证下滑露出文字
click(weizhi.centerX()+500,weizhi.centerY()-200);   //相对于text的坐标点击，自己微调一下
toast('位置');
sleep(2000);


click(735, 1373);       //点击确定
toast('已确定');
sleep(2000);

while(!click("提交信息"));sleep(500);       //点击提交信息
if(text("确认").findOnce()){
	text("确认").findOnce().click();
}
else if(text("确定").findOnce()){
	text("确定").findOnce().click();
}
sleep(1000);
click(100, 150);        
sleep(500);
click(100, 150);
sleep(500);
back();
home();             
exit();

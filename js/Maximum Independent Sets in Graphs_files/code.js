/// Copyright (c) 2016, Dr. Edmund Weitz. All rights reserved.

// the maximal number of vertices we can cope with; this is limited
// due to the fact that we're using bitwise operators below and
// JavaScript can't do bitwise arithmetic with larger integers
var MAX = 32;

// D3 selection of the SVG area
var svg;

// D3 selection of the table right of the SVG area
var board;

// the D3 object used for automatic graph layout
var layout;

// D3 drag behavior; needed so that we can move around vertices with the mouse although the layout moves them around as well
var drag;

// in case a user selected "Add/remove edge" from the context menu,
// this is the ID of the node that belonged to that menu
var lastNode;

// the list of all nodes (i.e. vertices); each node is an object that
// has properties like "id" (the number shown in the circle),
// "selected" (whether it is rendered in orange) and for example its x
// and y coordinates
var nodes = [];

// the list of all links (i.e. edges); each link is a two-element
// array of nodes as described above
var links = [];

// the menu which is shown if you right-click the SVG background
var svgMenu = [
  {
    title: 'Add a vertex',
    action: function (e, d) {
      SVGaddVertex.call(e, d);
      lastNode = null;
      newBoard();
    }
  },
  {
    title: 'Unfix all',
    action: function () { fixOrUnfixAll(false); }
  },
  {
    title: 'Fix all',
    action: function () { fixOrUnfixAll(true); }
  }
];

// functions to show or hide the help DIV
function closeHelp() {
  d3.select("#info").style("display", "none");
}

function showHelp() {
  d3.select("#info").style("display", "block");
}

// the following two function are called from the sliders used to
// modify the layout parameters
function gravityUpdate () {
  var gravity = parseInt(d3.select("#gravRange").property("value")) / 100;
  layout.gravity(gravity);
  layout.resume();
}

function distanceUpdate () {
  var distance = parseInt(d3.select("#distRange").property("value"));
  layout.distance(distance);
  update();
}

// callback for the SVG size HTML select element
function svgUpdate () {
  var index = parseInt(d3.select("#svgSize").property("value"));
  var width = [480, 600, 800, 1000, 1200][index - 1];
  // all choices have the same aspect ratio
  var height = width / 4 * 3;
  svg.attr("width", width)
    .attr("height", height);
  layout.size([width, height]);
  update();
}

// three function for the vertex counter in the lower right corner
function setCounter (value) {
  d3.select("#counter").text(value);
}

function getCounter () {
  return parseInt(d3.select("#counter").text());
}

function setCounterColor (color) {
  d3.select("#counter").style("color", color);
}

// this function is called once after the page has been loaded
function init () {
  // initialize counter, sliders, and select
  setCounter(0);
  setCounterColor("black");
  d3.select("#probRange").attr({
    min: 0,
    max: 100
  }).property("value", 30);
  d3.select("#avgRange").attr({
    min: 3,
    max: MAX
  }).property("value", 8);
  d3.select("#tolRange").attr({
    min: 0,
    max: 10
  }).property("value", 0);
  d3.select("#gravRange").attr({
    min: 0,
    max: 100
  }).property("value", 30)
    .on("change", gravityUpdate).on("input", gravityUpdate);
  d3.select("#distRange").attr({
    min: 100,
    max: 400
  }).property("value", 200)
    .on("change", distanceUpdate).on("input", distanceUpdate);
  d3.select("#svgSize").node().selectedIndex = 2;

  // create SVG area, D3 force layout, drag behavior, and edge table
  // and store them in global variables
  var width = 800, height = 600;
  svg = d3.select("#graph")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .on("contextmenu", d3.contextMenu(svgMenu));
  layout = d3.layout.force()
    .linkDistance(200)
    .charge(-800)
    .gravity(0.3)
    .size([width, height])
    .on("tick", draw);
  drag = layout.drag()
    .on("dragend", dragEnd);
  board = d3.select("#board");

  // create a random graph, display it, show the edge table as well
  randomGraph();
  newBoard();
}

// nodeInput must be a list of vertices as numbers like [1, 2, 3, 5]
// and linkInput must be a list of edges like [[1, 3], [5, 2]]; the
// global variables "nodes" and "links" will be set to these values,
// the SVG area will be updated accordingly (but not the edge table)
function newGraph (nodeInput, linkInput) {
  svg.selectAll("*").remove();
  nodes = [];
  links = [];
  lastNode = null;
  for (var i = 0; i < nodeInput.length; i++)
    addNode({ id: nodeInput[i] });
  for (i = 0; i < linkInput.length; i++)
    addLink(linkInput[i][0], linkInput[i][1]);
  update();
  setCounter(0);
  setCounterColor("black");
}

// returns the sorted list of all node (vertex) IDs
function nodeIndices () {
  return nodes.map(function (n) { return n.id; }).sort(function (a, b) { return a - b; });
}

// draws an interactive HTML table to control the edges of the current
// graph; the current table is removed first
function newBoard () {
  board.selectAll("*").remove();
  var nodesLength = nodes.length;
  var indices = nodeIndices();
  var linkPairs = links.map(function (link) { return [link.source.id, link.target.id]; });
  // whether the i-th and the j-th node (and NOT the nodes with these
  // IDs) are connected with an edge
  var isLink = function (i, j) {
    i = indices[i];
    j = indices[j];
    for (var k = 0; k < linkPairs.length; k++) {
      var pair = linkPairs[k];
      if (pair[0] == i && pair[1] == j)
        return true;
      if (pair[1] == i && pair[0] == j)
        return true;
    }
    return false;
  };
  // super-secret formula to compute the size of the table cells so
  // that the whole table always has roughly the same size no matter
  // how many vertices we have
  var size = Math.min(Math.floor(300 / Math.max(nodes.length, 1)) - 2, 20);
  var tr = board.append("tr");
  if (nodesLength <= 20) {
    for (var j = 0; j < nodes.length; j++) {
      tr.append("td").style({
        width: size + "px",
        height: size + "px",
        "text-align": "center",
        "font-size": "50%"
      }).text(indices[j]);
    }
    tr.append("td").style({
      width: size + "px",
      height: size + "px"
    });
  }
  for (var i = 0; i < nodesLength; i++) {
    tr = board.append("tr");
    for (j = 0; j < nodesLength; j++) {
      var active = i < j;
      var td = tr.append("td").style({
        width: size + "px",
        height: size + "px",
        border: active ? "1px solid black" : ""
      });
      if (active) {
        // each active cell has a datum attached to it which will be
        // used by the "lineToggle" callback: it "knows" the vertex
        // pair it represents and whether it is "on" (black) or not
        td.datum({
          indices: [indices[i], indices[j]],
          on: isLink(i, j)
        })
          .style("background", isLink(i, j) ? "black" : "white")
          .on("click", lineToggle);
      }
    }
    if (nodesLength <= 20)
      tr.append("td").style({
        width: size + "px",
        height: size + "px",
        "text-align": "center",
        "font-size": "50%"
      }).text(indices[i]);
  }
}

// callback for the edge table on the right; adds or removes an edge;
// see the "newBoard" function for the format of the "d" argument
function lineToggle (d) {
  lastNode = null;
  if (d.on)
    removeLink(d.indices[0], d.indices[1]);
  else 
    addLink(d.indices[0], d.indices[1]);
  d.on = !d.on;
  d3.select(this).style("background", d.on ? "black" : "white");
  update();
  colorLinks();
}

// the context menu which is shown if you right-click a vertex
var nodeMenu = [
  {
    title: 'Add to/remove from candidate set',
    action: function (e, d) {
      markNode.call(e, d);
    }
  },
  {
    title: 'Remove this vertex',
    action: function (e, d) {
      removeNode.call(e, d);
    }
  },
  {
    title: 'Add/remove edge',
    action: function(e, d, i) {
      // note that the only thing we do is to remember the node; the
      // actual work will be done in the click handler while all other
      // actions will reset "lastNode" to null
      lastNode = d.id;
    }
  },
  {
    title: 'Fix/Unfix this vertex',
    action: function(e, d, i) {
      fixUnfix.call(e, d);
    }
  }
];

// adds a node (vertex) to "nodes" as well as the SVG area; returns
// the SVG object which was created; the "node" argument must be an
// object with at least the "id" property; it is assumed that a node
// with this ID doesn't exist already
function addNode (node) {
  lastNode = null;
  if (nodes.length >= MAX) {
    alert("Too many vertices, sorry...");
    return null;
  }
  nodes.push(node);
  var g = svg.append("g")
      .datum(node)
      .on("click", nodeClick)
      .on("dblclick", fixUnfix)
      .on("contextmenu", d3.contextMenu(nodeMenu))
      .style("font-size", "small")
      .call(drag);
  var text = g.append("text")
      .text(node.id);
  var box = text.node().getBBox();
  text.attr("dx", - box.width/2)
    .attr("dy", box.height/2);
  box = text.node().getBBox();
  var circle = g.insert("circle", ":last-child")
      .attr("cx", box.x + box.width / 2)
      .attr("cy", box.y + box.height / 2)
      .attr("r", (node.id < 10 ? 8 : 4) + Math.sqrt(box.width*box.width, box.height*box.height) / 2)
      .attr("fill", "white")
      .attr("stroke", "black")
      .attr("class", "node");
  return g;
}

// given an ID, returns the corresponding object from the "nodes"
// array
function findNode (id) {
  var i;
  for (i = 0; i < nodes.length; i++)
    if (nodes[i].id === id)
      return nodes[i];
  return null;
}

// adds a new link (edge) to "links" and to the SVG area; the two
// arguments must be IDs; it is assumed that they are different and
// that such a link doesn't exist already
function addLink (from, to) {
  from = findNode(from);
  to = findNode(to);
  if (from && to) {
    var link = { source: from, target: to };
    links.push(link);
    svg.insert("line", "g")
       .attr("stroke", "black")
       .datum(link);
  }
}

// removes the link connecting the nodes with IDs "from" and "to" from
// "links" and from the SVG area
function removeLink (from, to) {
  links = links.filter(function (link) {
    return !((link.source.id == from && link.target.id == to) || (link.source.id == to && link.target.id == from));
  });
  svg.selectAll("line").filter(function (d) {
    return ((d.source.id == from && d.target.id == to) || (d.source.id == from && d.target.id == to)) ? this : null;
  }).remove();
}

// draws the vertices and the edges; this is called by the force
// layout on each "tick"
function draw () {
  svg.selectAll("g")
    .attr("transform", function (d) {
      return "translate(" + d.x + "," + d.y + ")";
    });
  svg.selectAll("line")
    .attr("x1", function (d) { return d.source.x; } )
    .attr("y1", function (d) { return d.source.y; } )
    .attr("x2", function (d) { return d.target.x; } )
    .attr("y2", function (d) { return d.target.y; } );
  clearSelection();
}

// utility function to remove a possible selection the browser might
// show
function clearSelection() {
  if (document.selection) {
    document.selection.empty();
  } else if (window.getSelection) {
    window.getSelection().removeAllRanges();
  }
}

// updates the automatic layout of the graph in case there were any
// changes
function update () {
  layout.nodes(nodes).links(links).start();
}

// this function assumes that it is called with an SVG "g" element in
// "this" and the corresponding datum in "d"; it will switch the
// "fixed" state of the corresponding vertex
function fixUnfix (d) {
  lastNode = null;
  d.fixed = !d.fixed;
  d3.select(this).select("circle").classed("fixed", d.fixed);
  update();
}

// this function will set the "fixed" state of ALL vertices to the
// boolean value "what"
function fixOrUnfixAll (what) {
  lastNode = null;
  svg.selectAll("circle").classed("fixed", what).each(function (d) {
    d.fixed = what;
  });
  update();
}

// handler for mouse clicks on vertices
function nodeClick (d) {
  // we only do something if "lastNode" denotes another vertex
  if (lastNode && d.id != lastNode) {
    var oldLength = links.length;
    // try to remove the link
    removeLink(d.id, lastNode);
    // if nothing was removed, add a link instead
    if (oldLength == links.length)
      addLink(d.id, lastNode);
    // update everything
    colorLinks();
    update();
    newBoard();
  }
  lastNode = null;
}

// marks or unmarks a node (vertex); "this" must hold the SVG "g"
// element; the function updates the color of the circle and the
// "selected" property of the node object
function markNode (d) {
  d3.event.preventDefault();
  d3.event.stopPropagation();
  lastNode = null;
  d.selected = !d.selected;
  d3.select(this).select("circle").style("fill", d.selected ? "orange" : "white");
  setCounter(getCounter() + (d.selected ? 1 : -1));
  colorLinks();
}

// colors all links (edges) so that the users is notified (by red
// edges) if his "candidate set" is not an independent set; updates
// the color of the counter as well
function colorLinks () {
  var red = false;
  d3.selectAll("line").attr("stroke", function (d) {
    var link = d.source.selected && d.target.selected;
    if (link)
      red = true;
    return link ? "red" : "black";
  }).attr("stroke-width", function (d) {
    return d.source.selected && d.target.selected ? 2 : 1;
  });
  setCounterColor(red ? "red" : "black");
}

// removes a node (vertex); "this" must hold the SVG "g" object while
// "d" is the node object; nodes (edges) adjacent to this vertex will
// also be removed; finally, the edge table will also be updated
function removeNode (d) {
  lastNode = null;
  d3.event.preventDefault();
  d3.event.stopPropagation();
  if (nodes.length <= 1) {
    alert("Hey, we need at least one vertex!");
    return;
  }
  var id = d.id;
  links = links.filter(function (link) {
    return link.source.id != id && link.target.id != id;
  });
  nodes = nodes.filter(function (node) {
    return node.id != id;
  });
  svg.selectAll("line").filter(function (d) {
    return d.source.id == id || d.target.id == id ? this : null;
  }).remove();
  d3.select(this).remove();
  update();
  newBoard();
}

// called from SVG context menu to add a vertex; "this" must be a
// reference to the SVG area itself
function SVGaddVertex () {
  lastNode = null;
  d3.event.preventDefault();
  var mouse = d3.mouse(this);
  // find the smallest index not used
  var indices = nodeIndices();
  for (var i = 0, id = 1; i < indices.length; i++, id++)
    if (indices[i] != id)
      break;
  // add node with starting position at mouse and initially fixed
  var n = addNode({
    x: mouse[0],
    y: mouse[1],
    id: id,
    fixed: true
  });
  // if a node was added (maybe it wasn't because of "MAX"), then show
  // that it's fixed
  if (n) {
    n.select("circle").classed("fixed", true);
    update();
  }
}

// called when dragging of a vertex ends; makes the vertex a "fixed"
// one, but only if we dragged with the left mouse button (otherwise
// this will be confused with the context menu)
function dragEnd (d) {
  if (d3.event.sourceEvent.button == 0) {
    d3.select(this).select("circle").classed("fixed", d.fixed = true);
    update();
  }
}

// creates and displays a random graph based on the parameters set with the sliders
function randomGraph() {
  var nodeInput = [];
  // probability for an edge
  var prob = parseInt(d3.select("#probRange").property("value")) / 100;
  // the "average" number of vertices (if "tolerance" is zero, then
  // this IS the number of vertices)
  var average = parseInt(d3.select("#avgRange").property("value"));
  // the actual number of vertices is allowed to deviate from
  // "average" by "tolerance" in both directions
  var tolerance = parseInt(d3.select("#tolRange").property("value"));
  // number of vertices; must be at least 3 and at most "MAX"
  var n = Math.max(3, Math.min(MAX, Math.floor(2 * tolerance * Math.random() + 0.1) + average - tolerance));
  for (var i = 1; i <= n; i++)
    nodeInput.push(i);
  var linkInput = [];
  // iterate through all possible edges and for each one decide
  // whether it exists based on the given probability
  for (i = 1; i <= n; i++)
    for (var j = i + 1; j <= n; j++)
      if (Math.random() <= prob)
        linkInput.push([i, j]);
  newGraph(nodeInput, linkInput);
  newBoard();
}

// the most interesting part of this file is probably this algorithm
// to find a maximum independent set; the parameters are a list of all
// edges in the form [[2, 3], [0, 4], [3, 4]] and a list of all
// isolated vertices in the form [1, 5]; vertices MUST be numbered 0,
// 1, 2, and so on with NO gaps
function solve (links, isolated) {
  // the heart of the function is the inner function "prune" which
  // calls itself recursively; its arguments are a list of links
  // (edges) and sets of "green" and "red" vertices; "links" is a
  // subset of the set of ALL links of the graph (see "allLinks"
  // below) and comprises only those edges where not both vertices are
  // colored yet; if a vertex belongs to the "green" vertices, it is
  // meant to be a candidate for a maximum independent set; if a
  // vertex belongs to the "red" set, it is meant to NOT be such a
  // candidate

  // note that within "prune" ALL vertices are represented as single
  // bits in the form 2^n where n is their original number; we can
  // thus represent sets like "greens" and "reds" as ONE number and
  // perform lots of tests using simple bitwise operations
  var prune = function (links, greens, reds) {
    // first check whether the proposal in "greens" wouldn't result in
    // two green vertices being connected
    for (var i = 0; i < links.length; i++)
      // note that something like "(V & greens)" means: "if vertex V
      // would be colored green"
      if ((links[i][0] & greens) && (links[i][1] & greens))
        // in that case, give up
        return;
    // this is important: we make a copy of "links" because we'll
    // modify it below; if we don't make a copy, we'll break the
    // decision tree
    links = links.slice();

    // we will now iterate through the edges until we find one with
    // both vertices uncolored; the main idea of the algorithm is to
    // always color at least two vertices per decision

    // we're iterating as long as there's no longer a change (where we
    // color edges "automatically"); that's what this boolean is for
    var change = true;
    // these two vertices will, at the end of the loop, denote the
    // edge we're looking for
    var from, to;
    while (change && links.length > 0) {
      change = false;
      // if possible, let's just use the first edge
      var link = links[0];
      from = link[0];
      to = link[1];
      // lump green and red together so that we can easily check for
      // "has one of the two colors"
      var greensOrReds = greens | reds;
      // if both vertices belonging to this edge are already colored,
      // we remove it from "links" and restart the iteration
      if ((from & greensOrReds) && (to & greensOrReds)) {
        links.splice(0, 1);
        change = true;
        continue;
      }
      // now the "complicated" case where one vertex has a color and
      // the other one hasn't
      if ((!(from & greensOrReds) && (to & greensOrReds)) || (!(to & greensOrReds) && (from & greensOrReds))) {
        // make sure "from" is always the colored vertex (otherwise
        // swap)
        if (!(from & greensOrReds)) {
          var temp = from;
          from = to;
          to = temp;
        }
        // if "from" is colored green, then "to" MUST be red, so we
        // just do that, remove the edge, and restart the iteration
        if (from & greens) {
          reds |= to;
          links.splice(0, 1);
          change = true;
          continue;
        }
        // if we're here, "from" is red; we now look at all neighbors
        // (except for "from") of "to" and store them in "toTargets";
        // note that we need "allLinks" and not "links" here
        var toTargets = [];
        for (i = 0; i < allLinks.length; i++) {
          if (allLinks[i][0] == to && allLinks[i][1] != from)
            toTargets.push(allLinks[i][1]);
          else if (allLinks[i][1] == to && allLinks[i][0] != from)
            toTargets.push(allLinks[i][0]);
        }
        // if "to" doesn't have any neighbors except for "from", we
        // can definitely color it green, remove the edge, and restart
        // the iteration
        if (toTargets.length == 0) {
          greens |= to;
          links.splice(0, 1);
          change = true;
          continue;
        }
        // at this point, "from" is red and "to" has other neighbors
        // as well; we iterate through them
        var other = null;
        for (i = 0; i < toTargets.length; i++) {
          // if one of the neighbors is green, then "to" MUST be red,
          // so we remove the edge and restart the iteration
          if (toTargets[i] & greens) {
            reds |= to;
            links.splice(0, 1);
            change = true;
            break;
          }
          // if one of the neighbors is neither green nor red, we
          // remember it and stop the"for" loop; that's because we
          // can now use the (uncolored) edge from "to" to this neighbor
          if (!(toTargets[i] & reds)) {
            other = toTargets[i];
            break;
          }
        }
        // this is triggered by the "break" statement above
        if (change)
          continue;
        // this is from the second case in the "for" loop
        if (other) {
          from = other;
          break;
        }
        // if we're here, then"from" is red and all of the neighbors
        // of "to" are red as well, so we can safely color "to"
        // green, remove the link, and restart the iteration
        greens |= to;
        links.splice(0, 1);
        change = true;
        continue;
      }
      // if we end up here, "link" is an edge with both vertices
      // uncolored so far, so we can make our decision
    }
    // note that there's some room for improvement at this point: we
    // don't have to take the first edge which is completely
    // uncolored; it would be more efficient (but uglier to code) to
    // see if there are more edges we can color automatically first;
    // but the code seems fast enough for this demo

    // if there are no links left, this means we have colored all
    // vertices and we thus have an independent set:
    if (links.length == 0) {
      // count how many bits are set in "greens", i.e. how many
      // vertices are in the candidate set "greens"
      var greensLength = 0;
      var greensCopy = greens;
      while (greensCopy) {
        greensLength += greensCopy & 1;
        greensCopy = greensCopy >>> 1;
      }
      // if we have more than the best one so far, we replace it
      if (greensLength > candidateLength) {
        candidate = greens;
        candidateLength = greensLength;
      }
      return;
    }
    // remove from "links" the edge from "from" to "to"
    links = links.filter(function (link) {
      return !((link[0] == from && link[1] == to) || (link[0] == to && link[1] == from));
    });
    // try one of three possibilities: color both vertices red, color
    // one of them red and the other one green; the fourth case (and
    // that's the main point of the algorithm) is impossible - we
    // can't color two neighboring vertices both green
    prune(links, greens, reds | from | to);
    prune(links, greens | from, reds | to);
    prune(links, greens | to, reds | from);
  };

  // the recursion will work on these two variables which represent
  // the best candidate so far
  var candidate = 0;
  var candidateLength = 0;

  // convert the input into the "2^n" format described above
  links = links.map(function (link) {
    return [1 << link[0], 1 << link[1]];
  });
  var allLinks = links;
  var greens = 0;
  for (var i = 0; i < isolated.length; i++)
    greens |= 1 << isolated[i];
  // call "prune" and let it do all the work
  prune(links, greens, 0);
  // convert the result back to the usual representation
  i = 0;
  var result = [];
  while (candidate) {
    if (candidate & 1)
      result.push(i);
    candidate = candidate >>> 1;
    i++;
  }
  return result;
}

// this function is called if the user presses the "Solve" button; it
// prepares the input for the "solve" function, calls it, converts the
// return value of this function, and finally draws it
function showSolution () {
  // like "links", but only the ID pairs
  var rawLinks = links.map(function (l) { return [l.source.id, l.target.id]; });
  // "rawNodes" will initially be a list of all IDs appearing in
  // "rawLinks"
  var rawNodes = [];
  for (var i = 0; i < rawLinks.length; i++) {
    if (rawNodes.indexOf(rawLinks[i][0]) == -1)
      rawNodes.push(rawLinks[i][0]);
    if (rawNodes.indexOf(rawLinks[i][1]) == -1)
      rawNodes.push(rawLinks[i][1]);
  }
  // "isolated" holds the IDs of all nodes which aren't in "rawNodes"
  // yet; these are called isolated vertices in graph theory
  var isolated = nodes.map(function (d) { return d.id; })
      .filter(function (i) { return rawNodes.indexOf(i) == -1; });
  // add them to "rawNodes" array
  rawNodes = rawNodes.concat(isolated);
  // sort it
  rawNodes = rawNodes.sort(function (a, b) { return a - b; });
  
  // we will now use "rawNodes" to map from node IDs to indices and
  // vice versa; for example, [3, 4, 1] would be mapped to [0, 1, 2]
  rawLinks = rawLinks.map(function (l) {
    return [rawNodes.indexOf(l[0]), rawNodes.indexOf(l[1])];
  });
  isolated = isolated.map(function (n) {
    return rawNodes.indexOf(n);
  });

  // compute the solution which will be returned in terms of indices
  var solution = solve(rawLinks, isolated);

  // the inverse mapping
  solution = solution.map(function (n) {
    return rawNodes[n];
  });

  // decide which circles should be orange (and which nodes will be
  // marked "selected")
  for (i = 0; i < nodes.length; i++) {
    nodes[i].selected = solution.indexOf(nodes[i].id) != -1;
  }
  svg.selectAll("circle").style("fill", function (d) { return d.selected ? "orange" : "white"; });
  setCounter(solution.length);
  // if the algorithm doesn't have a mistake, then nothing should be
  // red...
  colorLinks();
}

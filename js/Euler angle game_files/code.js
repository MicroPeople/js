// Copyright (c) 2015, Dr. Edmund Weitz.  All rights reserved.

var div, centerX, centerY, controls, mouseIsDown = false, lastValue, lastMouse;

function random (min, max) {
  return Math.random() * (max - min) + min;
}

function closeInfo () {
  document.getElementById("info").style.display = 'none';
}

function makeCrosshairs (color, radius, arcFactor) {
  innerRadius = arcFactor ? .05 : .2;
  var maybeHalf = arcFactor ? 1 : .5;    
  arcFactor = (arcFactor || 0.1);
  var result = new THREE.Object3D();
  var material = new THREE.MeshLambertMaterial({color: color});
  var torus = new THREE.Mesh(
    new THREE.TorusGeometry(radius, innerRadius, 10, 50, arcFactor*Math.PI),
    material
  );
  torus.rotation.x = .5*Math.PI;
  torus.rotation.z = -.5*arcFactor*Math.PI;
  result.add(torus);
  torus = new THREE.Mesh(
    new THREE.TorusGeometry(radius, innerRadius, 10, 50, maybeHalf*arcFactor*Math.PI),
    new THREE.MeshLambertMaterial({color: color})
  );
  torus.rotation.z = -.5*maybeHalf*arcFactor*Math.PI;
  result.add(torus);
  return result;
}

function mouseAngle (mousePos) {
  return Math.atan2(
    centerY - mousePos[1],
    mousePos[0] - centerX
  );
}

function mouseMove () {
  var move;
  if (mouseIsDown && controls.which <= 3) {
    switch (controls.current()) {
      case "x": case "X":
        move = (180 / Math.PI) * (mouseAngle(d3.mouse(this)) - mouseAngle(lastMouse));
        break;
      case "y": case "Y":
        move = .3 * (d3.mouse(this)[0] - lastMouse[0]);
        break;
      case "z": case "Z":
        move = .3 * (d3.mouse(this)[1] - lastMouse[1]);
        break;
    }
    controls[controls.which] = lastValue + move;
    controls.rotate();
  }
}

function mouseUp () {
  mouseIsDown = false;
}

function mouseDown () {
  if (controls.which <= 3) {
    lastValue = controls[controls.which];
    lastMouse = d3.mouse(this);
    mouseIsDown = true;
  }
}

function init () {
  div = d3.select("#output")
    .on("mousemove", mouseMove)
    .on("mouseup", mouseUp)
    .on("mousedown", mouseDown);
  
  var renderer = new THREE.WebGLRenderer();
  renderer.setClearColor(new THREE.Color(0xEEEEEE));
  renderer.setSize(window.innerWidth, window.innerHeight);
  
  var scene = new THREE.Scene();
  
  var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);

  var sphereRadius = 20;

  var pointLight = new THREE.PointLight(0xffffff, 1, 100);
  pointLight.intensity = .8;
  scene.add(pointLight);  

  var blackMaterial = new THREE.MeshLambertMaterial({color: 0x000000});
  var sphere = new THREE.Object3D();

  var circle, circleGeometry;
  for (var angle = -Math.PI; angle <= Math.PI; angle += (Math.PI / 18)) {
    var circleRadius = sphereRadius * Math.cos(angle);
    circleGeometry = new THREE.TorusGeometry(circleRadius, .03, 10, 80);
    circle = new THREE.Mesh(circleGeometry, blackMaterial);
    circle.rotation.y = .5*Math.PI;
    circle.position.x = sphereRadius * Math.sin(angle);
    sphere.add(circle);
  }
  circleGeometry = new THREE.TorusGeometry(sphereRadius, .03, 10, 80);
  for (var angle = -Math.PI; angle <= Math.PI; angle += (Math.PI / 18)) {
    circle = new THREE.Mesh(circleGeometry, blackMaterial);
    circle.rotation.x = angle;
    sphere.add(circle);
  }
  scene.add(sphere);

  var crosshairs = makeCrosshairs(0x00ff00, sphereRadius - 0.11);
  scene.add(crosshairs);

  var target = makeCrosshairs(0xff0000, sphereRadius + 0.21);
  scene.add(target);

  var largeTarget = makeCrosshairs(0xff6666, sphereRadius + 0.11, 2);

  var matrix = new THREE.Matrix4();
  controls = {
    '1': 0,
    '2': 0,
    '3': 0,
    which: 1,
    order: 'XYZ',
    helpLines: false,
    showSphere: true,
    current: function () {
      return controls.order.charAt(controls.which - 1);
    },
    reset: function () {
      mouseIsDown = false;
      controls.which = 1;
      controls['1'] = 0;
      controls['2'] = 0;
      controls['3'] = 0;
      controls.rotate();
    },
    shuffle: function () {
      target.rotation.x = random(0, 2 * Math.PI);
      target.rotation.y = random(.2, .8);
      if (Math.random() > .5)
        target.rotation.y = -target.rotation.y;
      target.rotation.z = random(.2, .7);;
      if (Math.random() > .5)
        target.rotation.z = -target.rotation.z;
      largeTarget.rotation.copy(target.rotation);
      matrix.makeRotationFromEuler(target.rotation);

      controls.reset();
    },
    next: function () {
      controls.which++;
      controls.action = actionString();
      actionGUI.updateDisplay();
    },
    rotate: function () {
      var m = new THREE.Matrix4();
      for (var i = 1; i <= 3; i++) {
        var n = new THREE.Matrix4();
        var angle = controls[i] * Math.PI / 180;
        switch (controls.order.charAt(i-1)) {
        case "x": case "X": n.makeRotationX(angle); break;
        case "y": case "Y": n.makeRotationY(angle); break;
        case "z": case "Z": n.makeRotationZ(angle); break;
        };
        m.multiply(n);
      }
      crosshairs.rotation.setFromRotationMatrix(m);
      var lookAt = (new THREE.Vector3()).fromArray(m.applyToVector3Array([0.98*sphereRadius,0,0]));
      camera.position.x = -lookAt.x;
      camera.position.y = -lookAt.y;
      camera.position.z = -lookAt.z;
      matrix.makeRotationFromEuler(crosshairs.rotation);
      camera.up.fromArray(matrix.applyToVector3Array([0,1,0]));
      camera.lookAt(lookAt);

      controls.action = actionString();
      actionGUI.updateDisplay();
      
      render();
    },
    action: 'ROLL'
  };

  var actionString = function () {
    if (controls.which > 3)
      return "";
    switch (controls.current()) {
      case "X": case "x": return "ROLL";
      case "Y": case "y": return "YAW";
      case "Z": case "z": return "PITCH";
    }
  };
  
  var gui = new dat.GUI();
  gui.add(controls, 'next');
  gui.add(controls, 'order', ['XYZ', 'YZX', 'ZXY', 'XZY', 'YXZ', 'ZYX',
                              'XYX', 'XZX', 'YXY', 'YZY', 'ZXZ', 'ZYZ']).onChange(controls.reset);
  gui.add(controls, 'reset');
  gui.add(controls, 'shuffle');
  gui.add(controls, 'helpLines').onChange(function (newValue) {
    if (newValue)
      scene.add(largeTarget);
    else
      scene.remove(largeTarget);
    controls.rotate();
  });
  gui.add(controls, 'showSphere').onChange(function (newValue) {
    if (newValue)
      scene.add(sphere);
    else
      scene.remove(sphere);
    controls.rotate();
  });
  var actionGUI = gui.add(controls, 'action').onChange(function () {
    controls.action = actionString();
    actionGUI.updateDisplay();
  });
  gui.close();

  document.getElementById("output").appendChild(renderer.domElement);
  var render = function () {
    renderer.render(scene, camera);
  };
  var rect = div.node().getBoundingClientRect();
  centerX = rect.left + 0.5 * rect.width;
  centerY = rect.top + 0.5 * rect.height;
  
  controls.shuffle();
}

window.onload = init;

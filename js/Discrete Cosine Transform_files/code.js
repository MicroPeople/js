// Copyright (c) 2016, Dr. Edmund Weitz. All rights reserved.

// function to disable standard event handling (like SVG
// drag-and-drop in Firefox and right-click menu)
function prevent () {
  d3.event.preventDefault();
}

// helper function to disable the text selection if one was made
// inadvertently; unfortunately, this doesn't always work
function clearSelection() {
  if (document.selection) {
    document.selection.empty();
  } else if (window.getSelection) {
    window.getSelection().removeAllRanges();
  }
}

// helper function to compute the sum from 1 to n
function gaussSum (n) {
  return n * (n + 1) / 2;
}

// returns the number "x" (which is supposed to be between -1 and 1)
// as a number with three decimal places; if "noSpan" is false,
// positive numbers will be preceded by an invisible span element the
// size of a minus sign
function formatCoeff (x, noSpan) {
  x = Math.round(1000 * x);
  var s = x < 0 ? "-" : (noSpan ? '' : '<span style="visibility: hidden">-</span>');
  x = Math.abs(x);
  s += Math.floor(x / 1000) + ".";
  x = x % 1000;
  if (x < 10)
    s += "00";
  else if (x < 100)
    s += "0";
  s += x;
  return s;
}

// global variables used by function "threeD", see below;
// the view frustum of the camera
var frustum = {
  left: -0.1,
  right: 3,
  bottom: -2.3,
  top: 0.4,
  near: -0.2,
  far: 5
};

// the matrix used to project the frustum onto
// normalized device coordinates (NDC)
var projectionMatrix = matrixMatrix(
  scalingMatrix(
    2 / (frustum.right - frustum.left),
    2 / (frustum.top - frustum.bottom),
    2 / (frustum.far - frustum.near)
  ),
  translationMatrix(
      - (frustum.left + frustum.right) / 2,
      - (frustum.top + frustum.bottom) / 2,
      - (frustum.far + frustum.near) / 2
  )
);

// the angle about which the camera is rotated about the x axis
var xAngle = 0.3;
// the angle about which the camera is rotated about the y axis
var yAngle = -0.6;

// the factors determining which of the nine two-dimensional cosine
// functions is currently being shown
var factorX = 1, factorY = 1;

// D3 function to translate a sequence of points into a smooth line
var lineFunction3D = d3.svg.line()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]; })
      .interpolate("linear");

// the function which draws the contents of the 3D view, the surface
// of a two-dimensional cosine function and the corresponding pattern
// underneath it
function threeD () {
  // matrix describing the movement from the camera (which starts in
  // the origin looking towards the z axis)
  var cameraWorldMatrix = matrixMatrix(
    yRotationMatrix(yAngle),
    matrixMatrix(
      xRotationMatrix(xAngle),
      // move back a bit
      translationMatrix(0, 0, -10)
    ));
  // the inverse of the above; "the world according to the camera"
  var cameraWorldMatrixInverse = invMatrix(cameraWorldMatrix);

  // the main matrix used to project arbitrary 3D coordinates onto normalized device coordinates
  var matrix = matrixMatrix(projectionMatrix, cameraWorldMatrixInverse);

  // clear SVG
  target = svgs[9];
  svgs[9].selectAll("*").remove();

  // the function to draw; we can't use "cosVals" here because we need
  // intermediate values
  var cosFunction = function (x, y) {
    // invert y direction so that what we see fits the patterns we
    // draw later
    y = Math.PI - y;
    return Math.cos(factorX * x) * Math.cos(factorY * y);
  };

  var step = Math.PI / 6;

  // draw the pattern (at the bottom) first
  for (var i = 0; i < 6; i += 2)
    for (var j = 0; j < 6; j += 2) {
      // each of the nine blocks is a polygon projected by "matrix"
      var P = matrixVector(matrix, i * step, -1.2, j * step);
      var line = P[0] + "," + P[1];
      P = matrixVector(matrix, (i + 2) * step, -1.2, j * step);
      line += " " + P[0] + "," + P[1];
      P = matrixVector(matrix, (i + 2) * step, -1.2, (j + 2) * step);
      line += " " + P[0] + "," + P[1];
      P = matrixVector(matrix, i * step, -1.2, (j + 2) * step);
      line += " " + P[0] + "," + P[1];
      var cosValue = cosFunction((i + 1) * step, (j + 1) * step);
      var gray = grayValue(cosValue);
      target.append("polygon")
        .attr({
          points: line,
          fill: d3.rgb(gray, gray, gray)
        });
    }

  // now draw the red "pillars"
  for (var i = 0; i < 6; i += 2)
    for (var j = 0; j < 6; j += 2) {
      cosValue = cosFunction((i + 1) * step, (j + 1) * step);
      var P1 = matrixVector(matrix, (i + 1) * step, -1.2, (j + 1) * step);
      var P2 = matrixVector(matrix, (i + 1) * step, cosValue, (j + 1) * step);
      target.append("line")
        .attr({
          x1: P1[0],
          y1: P1[1],
          x2: P2[0],
          y2: P2[1],
          stroke: "red",
          "stroke-width": 0.015
        });
      target.append("circle")
        .attr({
          cx: P2[0],
          cy: P2[1],
          r: 0.04,
          fill: "red"
        });
    }

  // the blue cosine lines
  for (j = 0; j <= 6.1; j += 0.5) {
    var lineData = [];
    for (var x = 0; x <= Math.PI + 0.1; x += 0.1)
      lineData.push(matrixVector(matrix, x, cosFunction(x, j * step), j * step));
    target.append("path")
      .attr({
        d: lineFunction3D(lineData),
        stroke: "blue",
        "stroke-width": 0.02,
        fill: "none"
      });
  }
  // the green cosine lines, perpendicular to the blue ones
  for (i = 0; i <= 6.1; i += 0.5) {
    lineData = [];
    for (var z = 0; z <= Math.PI + 0.1; z += 0.1)
      lineData.push(matrixVector(matrix, i * step, cosFunction(i * step, z), z));
    target.append("path")
      .attr({
        d: lineFunction3D(lineData),
        stroke: "green",
        "stroke-width": 0.02,
        fill: "none"
      });
  }
}

// global variable used by mouse handlers below
var lastPoint = null;

// called when mouse is clicked on 3D area
function mouseDown () {
  // remember where we clicked
  lastPoint = d3.mouse(this);
  // remember angles we had when manipulation started because what we
  // change should be relative to these angles
  lastXAngle = xAngle;
  lastYAngle = yAngle;
}

// called when mouse if move over 3D area
function mouseMove () {
  // only do something if button is down
  if (lastPoint) {
    var point = d3.mouse(this);
    // horizontal movement changes "xAngle" within certain limits
    var xMove = point[0] - lastPoint[0];
    yAngle = Math.max(Math.min(lastYAngle + xMove / 200, 0), -1.6);
    // vertical movement changes "yAngle" within certain limits
    var yMove = point[1] - lastPoint[1];
    xAngle = Math.max(Math.min(lastXAngle + yMove / 150, 1), 0.1);
    // redraw
    requestAnimationFrame(threeD);
    clearSelection();
  }
}

// callback to end mouse interaction over 3D area
function mouseUp () {
  lastPoint = null;
}

// global array to store D3 selections for all SVG areas in the page
// (or rather for the major "g" part of each); the SVG with id
// "output7" will be in svgs[7], and so on
var svgs = [];

// specs for the coordinate transformations for the SVGs mentioned
// above; [x1, x2, y1, y2] means the horizontal axis extends from x1
// (left) to x2 (right) and the vertical axis from y1 (bottom) to y2
// (top)
var svgSpecs = [
  [-0.2, Math.PI + 0.2, -1.45, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  [-0.2, Math.PI + 0.2, -1.5, 1.3],
  // the 3D area, NDC
  [-1.5, 2.5, -1.5, 2.5],
  [-20, 150, -20, 150],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40],
  [-10, 40, -10, 40]
];

// global cosine values, see next function definition
var cosVals = [];

// precomputes all relevant cosine values for an NxN pattern;
// cosVals[N][f][i] will hold the value
// $\cos(f \cdot \frac{\pi}{2N} \cdot (2i + 1))$
function generateCosArray (N) {
  var c = [];
  var piPart = Math.PI / (2 * N);
  for (var u = 0; u < N; u++) {
    var cc = [];
    for (var i = 1; i <= 2 * N; i += 2)
      cc.push(Math.cos(u * i * piPart));
    c.push(cc);
  }
  cosVals[N] = c;
}

// ensures the right order if the coefficient are taken from a 3x3
// array, i.e. the elements of the arrays are traversed diagonally
// from upper left (0,0) to lower right (2,2)
var pairs = [[0,0], [0,1], [1,0], [0,2], [1,1], [2,0], [1,2], [2, 1], [2, 2]];

// initializes one SVG area and stores it in the global "svgs" array
function initSVG (i) {
  var svg = d3.select("#output" + i);
  var rect = svg.node().getClientRects()[0];
  // use specs from above for transformation
  var specs = svgSpecs[i];
  svg =  svg.append("g")
    .attr("transform", "scale(" + (rect.width / (specs[1] - specs[0])) + "," + (-rect.height / (specs[3] - specs[2])) + ")"
          + "translate(" + (-specs[0]) + "," + -specs[3] + ")");
  
  // disable standard SVG interaction
  ["contextmenu", "drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave"].forEach(function (event) {
    svg.on(event, prevent);
  });

  svgs[i] = svg;
}

// converts the numerical value "x" (between -1 and 1) into a gray
// value between 0 and 255
function grayValue (x) {
  return Math.round(127.5 * x + 127.5);
}

// draws the cosine curve $x \mapsto \cos(factor \cdot x)$ from $0$ to
// $\pi$ using the color "color" in the SVG area "target"; rectangles
// with height "blockHeight" with the corresponding gray tones are
// shown below the curve; if "showText" is true, the numerical values
// and ticks are also shown
function drawCos1D (target, n, factor, color, blockHeight, showText) {
  // draw x and y axes
  [[-0.1, 0, Math.PI + 0.1, 0], [0, -1.1, 0, 1.1]].forEach(
    function (coords) {
      target.append("line")
        .attr({
          x1: coords[0],
          y1: coords[1],
          x2: coords[2],
          y2: coords[3],
          stroke: "black",
          "stroke-width": 0.008,
          "marker-end": "url(#arrow)"
        });
    });

  // tick for pi
  target.append("line")
  .attr({
    x1: Math.PI,
    y1: -0.05,
    x2: Math.PI,
    y2: 0.05,
    stroke: "black",
    "stroke-width": 0.008
  });
  if (showText)
    target.append("text")
    .attr({
      x: Math.PI,
      y: -0.05,
      dy: -0.025,
      dx: -0.025,
      transform: "scale(1, -1)",
      "font-size": 0.08
    })
    .html("&pi;");

  // the dashed lines for 1 and -1
  [[1, "&nbsp;1"], [-1, "-1"]].forEach(
    function (specs) {
      target.append("line")
        .attr({
          x1: 0,
          y1: specs[0],
          x2: 3,
          y2: specs[0],
          stroke: "#666666",
          "stroke-width": 0.008,
          "stroke-dasharray": ".03,.03"
        });
      if (showText)
        target.append("text")
        .attr({
          x: 3.1,
          y: -specs[0],
          dy: 0.025,
          transform: "scale(1, -1)",
          "fill": "#666666",
          "font-size": 0.08
        })
        .html(specs[1]);
    });

  // the cosine function, including factor
  var cosFunction = function (x) {
    return Math.cos(factor * x);
  };

  // plot the function
  var lineFunction = d3.svg.line()
        .x(function(d) { return d; })
        .y(function(d) { return cosFunction(d); })
        .interpolate("linear");
  var lineData = [];
  for (var x = -0.05; x <= Math.PI; x += 0.1)
    lineData.push(x);
  target.append("path")
    .attr({
      d: lineFunction(lineData),
      stroke: color,
      "stroke-width": 0.02,
      fill: "none"
    });

  // the gray rectangles below and the red markers for the function
  // values
  blockHeight = blockHeight || .2;
  var step = Math.PI / (2 * n);
  for (i = 1; i < 2 * n; i += 2) {
    x = i * step;
    target.append("line")
      .attr({
        x1: x,
        y1: 0,
        x2: x,
        y2: cosFunction(x),
        stroke: "red",
        "stroke-width": 0.012
      });
    target.append("circle")
      .attr({
        cx: x,
        cy: cosFunction(x),
        r: 0.05,
        fill: "red"
      });
    var gray = grayValue(cosFunction(x));
    target.append("rect")
      .attr({
        x: x - step,
        y: -1.15 - blockHeight,
        width: 2 * step,
        height: blockHeight,
        fill: d3.rgb(gray, gray, gray),
        stroke: color,
        "stroke-width": 0.005
      });
    if (showText)
      target.append("text")
      .attr({
        x: x,
        y: 1.35,
        "text-anchor": "middle",
        transform: "scale(1, -1)",
        "fill": "#666666",
        "font-size": 0.08
      })
      .html(i == 3 ? "0" : "&asymp;" + formatCoeff(cosFunction(x), true));
  }
}

// interprets "pic" (a 3x3 array) as a picture with gray pixels
// (values from -1 to 1) and draws these pixels as 10x10 blocks into
// the SVG area "target"; assumes that coordinate (0,0) is lower left
// of the SVG area while pic[0][0] is upper left pixel
function drawPicture (target, pic) {
  for (var i = 0; i < 3; i++)
    for (var j = 0; j < 3; j++) {
      var gray = grayValue(pic[i][j]);
      target.append("rect")
        .attr({
          y: 20 - i * 10,
          x: j * 10,
          width: 10,
          height: 10,
          fill: d3.rgb(gray, gray, gray)
        });
    }
}

// draws the frequency components in the one-dimensional array
// "coeffs" into the SVG area "target"; coefficients are expected to
// have values between -1 and 1 and will be colored according to their
// absolute values from dark blue to very light blue; "coeffs" is
// expected to be ordered according to the global array "pairs"
function drawCoeffs (target, coeffs) {
  coeffs = coeffs.map(Math.abs);

  for (var i = 0; i < 9; i++) {
    // values between 0 and 240
    var gray = 240 * (1 - coeffs[i]);
    // one 10x10 block per coefficient
    target.append("rect")
      .attr({
        // "pairs" determines position
        y: 20 - pairs[i][1] * 10,
        x: pairs[i][0] * 10,
        width: 10,
        height: 10,
        // 255 ensures this will be blue
        fill: d3.rgb(gray, gray, 255)
      });
  }
  // draw a grid
  for (i = 0; i < 40; i += 10) {
    target.append("line")
      .attr({
        x1: i,
        y1: 0,
        x2: i,
        y2: 30,
        stroke: "gray",
        "stroke-width": 1
      });
    target.append("line")
      .attr({
        x1: 0,
        y1: i,
        x2: 30,
        y2: i,
        stroke: "gray",
        "stroke-width": 1
      });
  }
}

// draws one of the basic 3x3 patterns into the SVG area "target"
// using nine 10x10 blocks; this will be the pattern for the function
// $\cos(factorX \cdot x) * \cos(factorY \cdot y)$ and it will
// be drawn at position "pos"
function drawPattern (target, factorX, factorY, pos) {
  for (var i = 0; i < 3; i++)
    for (var j = 0; j < 3; j++) {
      var gray = grayValue(cosVals[3][factorX][i] * cosVals[3][factorY][j]);
      target.append("rect")
        .attr({
          y: (2 - i) * 10 + (pos ? pos[0] : (2 - factorX) * 50),
          x: j * 10 + (pos ? pos[1] : factorY * 50),
          width: 10,
          height: 10,
          fill: d3.rgb(gray, gray, gray)
        });
    }
}

// draws all nine 3x3 basic patterns into the SVG area "target" and
// makes them clickable
function drawPatterns (target) {
  for (var x = 0; x < 3; x++)
    for (var y = 0; y < 3; y++)
      drawPattern(target, x, y);
  target.on("click", function () {
    var point = d3.mouse(this);
    var x = point[0];
    var y = (130 - point[1]);
    for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
        if (x >= i * 50 && x <= i * 50 + 30 && y >= j * 50 && y <= j * 50 + 30)
          patternCallback([i, j]);
  });
}

// callback to update the three-dimensional rendition of the patterns;
// the function is supposed to be with a tuple d = [fx, fy] which
// holds the factors of the respective cosine functions
function patternCallback (d) {
  d3.selectAll("td.click")
    .style("background-color", function (dd) {
      return (dd[0] == d[0] && dd[1] == d[1]) ? "black" : "white";
    })
    .style("color", function (dd) {
      return (dd[0] == d[0] && dd[1] == d[1]) ? "white" : "black";
    });
  // update global variables used by "threeD" function
  factorX = d[0];
  factorY = d[1];
  // redraw
  requestAnimationFrame(threeD);
  clearSelection();
}

// callback which is called when a slider has been moved; "i" is the
// number of the slider
function rangeUpdate (i) {
  if (d3.select("#range" + i).node()) {
    var val = parseInt(d3.select("#range" + i).property("value"));
    requestAnimationFrame(function () {
      // redraw everything
      drawOnCanvas("basis" + i, basis[val]);
      drawOnCanvas("basisMult" + i, basisMult[i][val]);
      drawOnCanvas("components" + i, components[i][val]);
      drawOnSmallCanvas("componentsSmall" + i, components[i][val]);
    });
  }
}

// returns a call to "rangeUpdate" with the right parameter; this is
// needed to capture the "i" correctly
function rangeUpdater (i) {
  return function () {
    rangeUpdate(i);
  };
}

// global variables which will be populated once (from "init") for the
// slider animation at the bottom
var basis;
var basisMult = [];
var components = [];

// called once after page has loaded
function init () {
  // precompute the cosine values we'll need
  generateCosArray(3);
  generateCosArray(32);

  // initialize all SVG areas
  for (var i = 0; i < svgSpecs.length; i++)
    initSVG(i);

  // draw the (one-dimensional) cosine curves
  drawCos1D(svgs[0], 3, 1, "blue", .1, true);
  drawCos1D(svgs[1], 3, 0, "brown");
  drawCos1D(svgs[2], 3, 1, "blue");
  drawCos1D(svgs[3], 3, 2, "green");
  drawCos1D(svgs[4], 5, 0, "brown");
  drawCos1D(svgs[5], 5, 1, "blue");
  drawCos1D(svgs[6], 5, 2, "green");
  drawCos1D(svgs[7], 5, 3, "orange");
  drawCos1D(svgs[8], 5, 4, "purple");

  // make the 3D SVG area interactive
  var svg9 = d3.select("#output9");
  svg9.on("mousemove", mouseMove);
  svg9.on("mousedown", mouseDown);
  svg9.on("mouseup", mouseUp);
  d3.select("body").on("mouseup", mouseUp);

  // make the numbers below the 3D area clickable
  d3.selectAll("td.click").data(pairs)
    .on("click", patternCallback);
  d3.select("#five").style({
    "background-color": "black",
    "color": "white"
  });

  // draw the basic 3x3 patterns and make them clickable
  drawPatterns(svgs[10]);

  // draw the headline of the table, one pattern per column, ordered
  // as by the global variable "pairs"
  i = 11;
  pairs.forEach(function (pair) {
    drawPattern(svgs[i], pair[1], pair[0], [0, 0]);
    i++;
  });

  // now for the six rows of the table
  i = 20;
  [
    // these are the six "pictures" A to F
    [[Math.sqrt(.75), 0, -Math.sqrt(.75)], [Math.sqrt(.75), 0, -Math.sqrt(.75)], [Math.sqrt(.75), 0, -Math.sqrt(.75)]],
    [[.5, .5, .5], [1, 1, 1], [.5, .5, .5]],
    [[.9, .6, -.3], [.9, .6, -.3], [.9, .6, -.3]],
    [[.9, .6, -.3], [.9, .6, -.3], [.6, .3, -.6]],
    [[-0.31458235, 0.55822444, 0.83942104],
     [0.32197404, -0.28957248, -0.73676777],
     [0.8205354, 0.20882654, 0.39306593]],
    [[-0.31108235,  0.55122444,  0.84292104],
     [ 0.31497404, -0.27557248, -0.74376777],
     [ 0.8240354 ,  0.20182654,  0.39656593]]
  ].forEach(function (pic) {
    // the picture in the leftmost column
    drawPicture(svgs[i], pic);
    var coeffs = computeCoeffs(3, pic);
    // the (blue) coefficients in the rightmost column
    drawCoeffs(svgs[i + 6], coeffs);

    // populate the row with the computed coefficients
    d3.selectAll(".td" + i)
      .data(coeffs)
      .html(function (x) {
        // this indirection is necessary because of the "noSpan" parameter
        return formatCoeff(x);
      })
    // only relevant coefficients will be red
      .style("color", function (x) {
        return Math.abs(Math.round(100 * x)) > 0 ? "red" : "LightGray";
      });
    i++;
  });
  
  // start 3D animation
  threeD();

  // precompute the 1024 basis patterns once
  basis = computeBasis();

  for (i = 0; i < 3; i++) {
    // set up the sliders
    d3.select("#range" + i).attr({
      min: 0,
      max: 1023
    }).property({
      value: 0
      // we need both "change" and "input" due to a Firefox bug
    }).on("change", rangeUpdater(i)).on("input", rangeUpdater(i));
    
    // the three "pictues"; see separate file
    var arr = [gauss, football, randomPic][i];

    // draw the first basis element
    drawOnCanvas("basis" + i, basis[0]);

    // draw original picture twice
    drawOnCanvas("pic" + i, arr);
    drawOnSmallCanvas("picSmall" + i, arr);

    // compute coefficients for this picture and draw the blue square
    // at the right; this is done only once per picture
    var coeffs = computeCoeffs(32, arr);
    drawCoeffsOnCanvas("coeffs" + i, coeffs);
    
    // precompute the basis patterns multiplied with the corresponding
    // coefficients
    basisMult[i] = multBasis(basis, coeffs);
    // draw the first one
    drawOnCanvas("basisMult" + i, basisMult[i][0]);
    
    // precompute the sequence of the initial sums of basis patterns
    // multiplied with the corresponding coefficients
    components[i] = computeComponents(basisMult[i], coeffs);
    // draw the first one twice
    drawOnCanvas("components" + i, components[i][0]);
    drawOnSmallCanvas("componentsSmall" + i, components[i][0]);
  }
}

// computes the coefficients to reconstruct the picture "pic" (a NxN
// array with values between -1 and 1); return value is a
// one-dimensional array with the coefficients in the "right order"
function computeCoeffs (N, pic) {
  // this is essentially DCT-II, but without the usual factors to make
  // the basis matrixes orthogonal
  var result = [];
  var factorInit = 4 / (N * N);
  var twoN = 2 * N - 2;

  // compute the result in the right order, i.e. by working diagonally
  // from upper left, (u, v) = (0, 0), to lower right,
  // (u, v) = (N - 1, N -1);
  // "sum" is the sum of u and v and is constant per diagonal
  for (var sum = 0; sum <= twoN; sum++) {
    var min = Math.min(sum, N - 1);
    for (var v = 0; v <= min; v++) {
      var u = sum - v;
      if (u >= 0 && u < N) {
        var factor = factorInit;
        factor *= (v == 0 ? .5 : 1);
        factor *= (u == 0 ? .5 : 1);
        var s = 0;
        for (var y = 0; y < N; y++)
          for (var x = 0; x < N; x++)
            s += pic[x][y] * cosVals[N][v][y] * cosVals[N][u][x];
        result.push(factor * s);
      }
    }
  }

  return result;
}

// computes the 1024 32x32 basis matrixes and returns them
// as a one-dimensional array "in the right order"
function computeBasis () {
  var b = [];
  // see comments in "computeCoeffs"
  for (var sum = 0; sum <= 64; sum++) {
    var min = Math.min(sum, 31);
    for (var v = 0; v <= min; v++) {
      var u = sum - v;
      if (u >= 0 && u < 32) {
        // compute one 32x32 array below
        var arr = [];
        for (var y = 0; y < 32; y++) {
          var row = [];
          for (var x = 0; x < 32; x++)
            row.push(cosVals[32][v][x] * cosVals[32][u][y]);
          arr.push(row);
        }
        b.push(arr);
      }
    }
  }
  return b;
}

// computes the products of the 1024 32x32 basis matrixes (see above)
// in "basis" with the corresponding coefficients in "coeffs"
function multBasis (basis, coeffs) {
  var b = [];
  for (var i = 0; i < 1024; i++) {
    var arr = [];
    for (var j = 0; j < 32; j++) {
      var row = [];
      for (var k = 0; k < 32; k++)
        row.push(coeffs[i] * basis[i][j][k]);
      arr.push(row);
    }
    b.push(arr);
  }
  return b;
}

// computes the 1024 steps towards the reconstructed image by adding
// up the initial segments of the sequence computed by "multBasis"
function computeComponents (basisMult, coeffs) {
  var b = [];
  // s keeps the current sum (a matrix) during the computation
  var s = new Array(32);
  for (var i = 0; i < 32; i++)
    s[i] = new Array(32).fill(0);
  for (var i = 0; i < 1024; i++) {
    var arr = [];
    for (var j = 0; j < 32; j++) {
      var row = [];
      for (var k = 0; k < 32; k++) {
        s[j][k] += basisMult[i][j][k];
        row.push(s[j][k]);
      }
      arr.push(row);
    }
    b.push(arr);
  }
  return b;
}

// draws the 32x32 array "img" (with values from -1 to 1) on the HTML5
// canvas with ID "id" and dimensions 32x32
function drawOnSmallCanvas (id, img) {
  var c = document.getElementById(id);
  var ctx = c.getContext("2d");
  var imgData = ctx.getImageData(0, 0, 32, 32);
  var data = imgData.data;
  var k = 0;
  for (var i = 0; i < 32; i++) {
    for (var j = 0; j < 32; j++) {
      var gray = grayValue(img[i][j]);
      data[k++] = gray;
      data[k++] = gray;
      data[k++] = gray;
      data[k++] = 255;
    }
  }
  ctx.putImageData(imgData, 0, 0);
}

// like "drawOnSmallCanvas", but for a 128x128 canvas element; each
// pixel is rendered as a 2x2 block
function drawOnCanvas (id, img) {
  var c = document.getElementById(id);
  var ctx = c.getContext("2d");
  var imgData = ctx.getImageData(0, 0, 128, 128);
  var data = imgData.data;
  // the following loop isn't exactly pretty, but hopefully efficient
  // enough even for lots of slider movements
  var k1 = 0, k2 = 512, k3 = 1024, k4 = 1536;
  for (var i = 0; i < 32; i++) {
    for (var j = 0; j < 32; j++) {
      var gray = grayValue(img[i][j]);
      for (var n = 0; n < 4; n++) {
        data[k1++] = gray;
        data[k1++] = gray;
        data[k1++] = gray;
        data[k1++] = 255;
        data[k2++] = gray;
        data[k2++] = gray;
        data[k2++] = gray;
        data[k2++] = 255;
        data[k3++] = gray;
        data[k3++] = gray;
        data[k3++] = gray;
        data[k3++] = 255;
        data[k4++] = gray;
        data[k4++] = gray;
        data[k4++] = gray;
        data[k4++] = 255;
      }
    }
    k1 += 1536;
    k2 += 1536;
    k3 += 1536;
    k4 += 1536;
  }
  ctx.putImageData(imgData, 0, 0);
}

// draws the blue square for the coefficients in "coeffs" onto the
// HTML5 canvas element with ID "id"
function drawCoeffsOnCanvas (id, coeffs) {
  // see also comments in "drawCoeffs" function
  coeffs = coeffs.map(Math.abs);
  var c = document.getElementById(id);
  var ctx = c.getContext("2d");
  var imgData = ctx.getImageData(0, 0, 128, 128);
  var data = imgData.data;
  // see comment in "drawOnCanvas" function...
  var k1 = 0, k2 = 512, k3 = 1024, k4 = 1536;
  // the complication here is that we have to compute the index into
  // "coeffs" from the x and y coordinates of each block (because of
  // the "diagonal order" mentioned previously)
  for (var y = 0; y < 32; y++) {
    for (var x = 0; x < 32; x++) {
      // "sum": which diagonal are we in
      var sum = x + y;
      // "previous": how many coefficients have been in the previous
      // (completed) diagonals plus the ones we already had in this one
      var previous = sum > 32 ?
            gaussSum(32) + gaussSum(31) - gaussSum(64 - sum) + x - (sum - 32):
            gaussSum(sum - 1) + x;
      var gray = 240 * (1 - coeffs[previous]);
      for (var n = 0; n < 4; n++) {
        data[k1++] = gray;
        data[k1++] = gray;
        data[k1++] = 255;
        data[k1++] = 255;
        data[k2++] = gray;
        data[k2++] = gray;
        data[k2++] = 255;
        data[k2++] = 255;
        data[k3++] = gray;
        data[k3++] = gray;
        data[k3++] = 255;
        data[k3++] = 255;
        data[k4++] = gray;
        data[k4++] = gray;
        data[k4++] = 255;
        data[k4++] = 255;
      }
    }
    k1 += 1536;
    k2 += 1536;
    k3 += 1536;
    k4 += 1536;
  }
  ctx.putImageData(imgData, 0, 0);
}

window.onload = init;

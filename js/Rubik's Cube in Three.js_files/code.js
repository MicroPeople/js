// Copyright (c) 2021, Dr. Edmund Weitz.  All rights reserved.

// note that the order is important here!
const colorNames = ["blue", "green", "white", "yellow", "red", "orange"];
const colorDirs = [[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]];

// assigns color codes to "color characters"
const colors = {
  "b": "#2362FD", // blue
  "g": "#018A46", // green
  "w": "#F6F9FD", // white
  "y": "#FFE456", // yellow
  "r": "#DF2F31", // red
  "o": "#F36726"  // orange
};
// in the SVG drawing, the six middle squares are fixed; these
// structures assign to each one the one above it and the one to the
// right of it
const up = {
  "b": "r",
  "g": "o",
  "w": "g",
  "y": "g",
  "r": "g",
  "o": "b"
};
const right = {
  "b": "w",
  "g": "w",
  "w": "o",
  "y": "r",
  "r": "w",
  "o": "w"
};
// helper array used in index searches
const indices = [0,1,2];

// some global variables for three.js (needed by mouse handlers)
let camera, raycaster, renderer;
// the 26 cube data structures, and the two closures used to re-render
// the three.js output and to re-arrange the screen
let cubes, renderFunction, resize;
// whether the right half should be shown
let showSVG = true;
// the size of one cube
const size = 5;
// used to map indices 0,1,2 to axis names
const chars = ["x", "y", "z"];
// the last character the user typed
let lastChar = false;
// whether an animation is currently running
let animation = false;
// the viewport values of the SVG area
const svgWidth = 11.25;
const svgHeight = 17;
// assign arrays of nine SVG rectangles each to color characters
const squares = {};
// SVG triangle information;
const triangles = {};
let showTriangles = false;
// remember where mouse click was and on what
let lastMouse = null;
// length of three.js diagonal
let diag = 1000;
// how much the mouse has to move to initiate an action
let moveDist = 50;
// time of last touch
let lastTouch = performance.now();
// for storing moves
let store = false;
let invert = false;
let storedMoves = [];

// default event handler which does nothing and disables the default
// event
function prevent (event) {
  event.preventDefault();
}

// makes sure x is between m and M
function clip(x, m = 0, M = 1) {
  return Math.max(m, Math.min(M, x));
}

// increasing easing function, turns [0,1] inputs into [0,1] outputs
function quadUp(t) {
  return clip(t < 0.5 ? 2*t*t : -1+(4-2*t)*t);
}

// calls setter repeatedly for duration milliseconds with values from
// 0 to 1 and eventually calls finish
function runAction(duration, setter, finish) {
  let start = performance.now();
  let run = function() {
    let value = performance.now() - start;
    if (value >= duration) {
      setter(1);
      finish();
    } else {
      setter(quadUp(value/duration));
      window.requestAnimationFrame(run);
    };
  };
  run();
}

// returns a unique number if x, y, and z are all in {-1,0,1}
function cubeIndex(x,y,z) {
  return (x+1) + 3*(y+1) + 9*(z+1);
}

// assuming side is a 3-array where only one component is non-zero,
// checks if coords denote a cube on the same side
function sameSide(side, coords) {
  return coords.every((coord, i) => side[i] == 0 || side[i] == coord);
}

function rectTo(vec, coords) {
  return coords[vec.findIndex(el => el != 0)] == 0;
}

// whether side i (0 to 5 of BoxGeometry) is a side pointing outward
// for a cube in position [x,y,z]
function showSide(i,x,y,z) {
  switch(i) {
    case 0:
    return x == 1;
    case 1:
    return x == -1;
    case 2:
    return y == 1;
    case 3:
    return y == -1;
    case 4:
    return z == 1;
    default:
    return z == -1;
  };
}

// loads images and creates textures for the three.js boxes
function createSides() {
  const sides = [];
  let i = 0;
  let counter = 0;
  for (let colorName of colorNames) {
    const j = i;
    new THREE.TextureLoader().load(
      `${colorName}.png`,
      // callback which is called once the texture is loaded
      texture => {
        const material = new THREE.MeshLambertMaterial({
          map: texture,
          side: THREE.DoubleSide
        });
        sides[j] = material;
        counter++;
        // we're finished once all six are done
        if (counter >= 6) {
          createCubes(sides);
        }
      }
    );
    i++;
  }
}

// translates coordinates of the "original" cube back to colors
function origColor(x,y,z) {
  switch(cubeIndex(x,y,z)) {
  case 14: return "b"; // [1,0,0] is blue
  case 12: return "g"; // [-1,0,0] is green
  case 16: return "w"; // [0,1,0] is white
  case 10: return "y"; // [0,-1,0] is yellow
  case 22: return "r"; // [0,0,1] is red
  case 4: return "o"; // [0,0,-1] is orange
    // only for "pure" vectors (with two zeros)
  default: return null;
  }
}

// this function is called once the texttures are loaded
function createCubes(sides) {
  cubes = [];
  const blackSide = new THREE.MeshLambertMaterial({
    color: 0x000000,
    side: THREE.DoubleSide
  });
  const cubeGeometry = new THREE.BoxGeometry(0.98*size, 0.98*size, 0.98*size);
  for (let x of [-1,0,1]) {
    for (let y of [-1,0,1]) {
      for (let z of [-1,0,1]) {
        // skip middle cube
        if (cubeIndex(x,y,z) == 13)
          continue;
        const materials = [];
        for (let i = 0; i < 6; i++)
          materials.push(showSide(i,x,y,z) ? sides[i] : blackSide);
        const cubeMesh = new THREE.Mesh(cubeGeometry, materials);
        cubeMesh.position.x = size*x;
        cubeMesh.position.y = size*y;
        cubeMesh.position.z = size*z;
        cubes.push({
          mesh: cubeMesh,
          // "simplified coordinates" where x, y, and z are always -1, 0, or 1
          coords: [x,y,z],
          // the six "middle cubes" can be asked for their color so we
          // can find them
          color: origColor(x,y,z)
        });
      }
    }
  }
  init();
}

function resetCubes() {
  store = false;
  let i = 0;
  for (let x of [-1,0,1]) {
    for (let y of [-1,0,1]) {
      for (let z of [-1,0,1]) {
        // skip middle cube
        if (cubeIndex(x,y,z) == 13)
          continue;
        const cubeMesh = cubes[i].mesh;
        cubeMesh.position.x = size*x;
        cubeMesh.position.y = size*y;
        cubeMesh.position.z = size*z;
        cubeMesh.rotation.x = 0;
        cubeMesh.rotation.x = 0;
        cubeMesh.rotation.y = 0;
        cubeMesh.rotation.z = 0;
        cubes[i].coords = [x,y,z];
        cubes[i].color = origColor(x,y,z);
        i += 1;
      }
    }
  }
  // reset triangle rotations
  for (let name of ["g", "y", "r", "w", "b", "o"])
    triangles[name][3] = 0;
  renderFunction();
  updateSquares();
}

// returns new coordinates of cube if rotated by angle around the axis
// where the coordinate with index keep is the only non-zero one; the
// rotation is done as if this coordinate were 1, even if it is -1
function turn(cube, keep, angle) {
  const xIndex = (keep+1) % 3;
  const yIndex = (keep+2) % 3;
  const c = Math.cos(angle);
  const s = Math.sin(angle);
  let newX = c*cube.coords[xIndex] - s*cube.coords[yIndex];
  let newY = s*cube.coords[xIndex] + c*cube.coords[yIndex];
  const result = [0,0,0];
  result[keep] = cube.coords[keep];
  result[xIndex] = newX;
  result[yIndex] = newY;
  return result;
}

// called once to set up everything once cubes are created
function init() {
  const moves = localStorage.getItem("moves");
  if (moves)
    storedMoves = JSON.parse(moves);

  document.getElementById("info").ontouchstart = hideInfo;
  document.onkeydown = keyHandler;
  const output = document.getElementById("output");
  output.onmousedown = mouseDown;
  output.onmouseup = mouseUp;
  output.onmousemove = mouseMove;
  output.ontouchstart = touchStart;
  output.ontouchend = mouseUp;
  output.ontouchmove = touchMove;

  raycaster = new THREE.Raycaster();

  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor(new THREE.Color(0x0));
  renderer.antialias = true;

  const scene = new THREE.Scene();
  
  camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.x = -30;
  camera.position.y = 20;
  camera.position.z = 17;
  camera.lookAt(scene.position);

  const directionalLight = new THREE.DirectionalLight("#ffffff");
  directionalLight.position.set(-25, 30, 25);
  scene.add(directionalLight);
  scene.add(new THREE.AmbientLight(0x606060));

  for (let cube of cubes)
    scene.add(cube.mesh);

  output.appendChild(renderer.domElement);

  const svg = document.getElementById("svg");
  svg.ontouchstart = svgTouchStart;
  for (let el of [svg, renderer.domElement])
    ["contextmenu", "drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave", "drop"].forEach(function (event) {
      el.addEventListener(event, prevent);
    });

  // functions are defined here in order to close over variables
  renderFunction = () => {renderer.render(scene, camera);};
  // called when SVG visibility is toggled or when size changes
  resize = () => {
    const h = window.innerHeight;
    const svgW = (h < window.innerWidth && showSVG) ? svgWidth/svgHeight*h : 0;
    const thrW = window.innerWidth - svgW;
    svg.setAttribute("width", svgW);
    svg.setAttribute("height", h);
    svg.setAttribute("viewBox", `0 0 ${svgWidth} ${svgHeight}`);
    svg.style.display = showSVG ? "block" : "none";

    renderer.setSize(thrW, h);
    diag = Math.min(thrW,h);
    moveDist = diag / 20;
    camera.aspect = thrW / h;
    camera.updateProjectionMatrix();
    renderFunction();
  };
  window.onresize = resize;
  resize();
  draw();
}

// utility function to set several attributes of an element at once
function setAttributes (el, attrs) {
  for (let [name, val] of attrs) {
    el.setAttribute(name, val);
  }
}

// draw rectangle into SVG area and groups them into squares data structure
function draw() {
  const svg = document.getElementById("svg");
  const svgNS = svg.namespaceURI;
  for (let [name, cx, cy] of [
    ["g",5.25,2],
    ["y",2,5.25],
    ["r",5.25,5.25],
    ["w",8.5,5.25],
    ["b",5.25,8.5],
    ["o",5.25,11.75]
  ]) {
    squares[name] = [];
    for (let y = -1; y <= 1; y++) {
      for (let x = -1; x <= 1; x++) {
        const sq = svg.appendChild(document.createElementNS(svgNS, "rect"));
        setAttributes(sq, [
          ["fill", "white"],
          ["x", cx+x],
          ["y", cy+y+1.25],
          ["width", 0.9],
          ["height", 0.9]
        ]);
        squares[name].push(sq);
      }
    }
    cx += 0.45;
    cy += 1.7;
    const triangle = svg.appendChild(document.createElementNS(svgNS, "polygon"));
    triangles[name] = [triangle, cx, cy, 0];
    setAttributes(triangle, [
      ["fill", "none"],
      ["points", `${cx+0.2},${cy+0.3} ${cx-0.2},${cy+0.3} ${cx},${cy-0.3}`]
    ]);
  }
  updateSquares();
}

// updates colors of all SVG squares
function updateSquares() {
  for (let name of ["g", "y", "r", "w", "b", "o"]) {
    // all cubes on the side of middle square with this color
    const c = findAllCubes(name);
    // the direction this color "looks at"
    const v = axisFromChar(name);
    // for this to work findAllCubes must return the cubes in a
    // specific order
    for (let i = 0; i < 9; i++) {
      squares[name][i].setAttribute("fill", colorInDir(c[i],v));
      if (i == 4) {
        const [triangle, cx, cy, r] = triangles[name];
        triangle.setAttribute("transform", `rotate(${r*90}, ${cx}, ${cy})`);
        triangle.setAttribute("fill", showTriangles ? "black" : "none");
      }
    }
  }
}

// initiates an animated 90 degree rotation around vec where plus
// means counterclockwise; if all is true, the whole cube is rotated,
// otherwise only the nine cubes on the side indicated by vec; and
// here's a hack I added later: if all is the number 1, the middle
// layer perpendicular to vec is rotated...
function rotate(vec, plus = true, all = false, duration = 1000, next = null) {
  // do nothing if another animation is running
  if (animation)
    return;
  if (store)
    store.push([vec, plus, all]);
  // reverse direction if the non-zero component is negative
  const rev = vec.some(comp => comp < 0);
  if (rev)
    plus = !plus;
  // angle is for the cube positions, angle2 is for the local
  // rotations
  const angle = plus ? Math.PI/2 : -Math.PI/2;
  const angle2 = rev ? -angle : angle;
  // update triangle rotations
  if (all === false)
    triangles[findCubeAt(vec).color][3] += angle2 > 0 ? -1 : 1;
  else if (all == 1) {
    triangles[findCubeAt(vec).color][3] += angle2 > 0 ? 1 : -1;
    triangles[findCubeAt([-vec[0], -vec[1], -vec[2]]).color][3] += angle2 > 0 ? -1 : 1;
  }
  // the index of the non-zero component
  const index = vec.findIndex(el => el != 0);
  const vec3 = new THREE.Vector3(...vec);
  // remember local rotations before this action
  for (let cube of cubes) {
    cube.oldQuaternion = new THREE.Quaternion();
    cube.oldQuaternion.copy(cube.mesh.quaternion);
  }
  animation = true;
  runAction(
    duration,
    value => {
      const q = new THREE.Quaternion();
      q.setFromAxisAngle(vec3, value*angle2);
      for (let cube of cubes) {
        if (all === true || (all === 1 && rectTo(vec, cube.coords)) || (!all && sameSide(vec, cube.coords))) {
          const turned = turn(cube, index, value*angle);
          for (let i = 0; i < 3; i++)
            cube.mesh.position[chars[i]] = size*turned[i];
          const qq = new THREE.Quaternion();
          qq.multiplyQuaternions(q, cube.oldQuaternion);
          qq.normalize();
          cube.mesh.setRotationFromQuaternion(qq);
        }
      }
      renderFunction();
    },
    () => {
      // recompute the "simplified coordinates" after rotation
      for (let cube of cubes)
        if (all === true || (all === 1 && rectTo(vec, cube.coords)) || (!all && sameSide(vec, cube.coords)))
          cube.coords = turn(cube, index, angle).map(Math.round);
      // update SVG if this was a move (and not a rotation of the
      // whole cube)
      if (all !== true)
        updateSquares();
      animation = false;
      if (next)
        next();
    }
  );
}

// random integer between from and toInc (inclusive)
function randomInt(from, toIncl) {
  return from + Math.floor((toIncl - from + 1) * Math.random());
}

// random 3-vector where two components are zero and one is 1 or -1
function randomVec() {
  const vec = [0,0,0];
  vec[randomInt(0,2)] = Math.random() < 0.5 ? 1 : -1;
  return vec;
}

// performs 10 to 60 random movements in a row
function randomMovement() {
  store = false;
  const n = randomInt(20,30);
  const moves = [];
  for (let i = 0; i < n; i++)
    moves.push([rotate, randomVec(), Math.random() < 0.5, Math.random() < 0.2 ? true : Math.random() < 0.4 ? 1 : false, 400]);
  performMoves(moves);
}

// performs a sequence of moves (in reverse) where each move is of the
// form [function, ...aruments]; note that moves is destroyed!
function performMoves(moves) {
  let nextMove = () => {
    if (moves.length <= 0)
      return;
    const [fn, ...args] = moves.pop();
    fn(...args, nextMove);
  };
  nextMove();
}

// run the moves stored under digit n
function runStoredMoves(n) {
  if (!storedMoves[n])
    return;
  performMoves(storedMoves[n].map(move => [rotate, ...move, 400]).reverse());
}

// compute the inverse of one particular move
function reverseMove([vec, plus, all]) {
  if (all === true)
    return [vec, !plus, true];
  else if (all === 1)
    return [[-vec[0], -vec[1], -vec[2]], plus, 1];
  else
    return [vec, !plus, false];
}

// run the inverses of the moves stored under digit n in reverse order
function runStoredMovesReverse(n) {
  if (!storedMoves[n])
    return;
  performMoves(storedMoves[n].map(move => [rotate, ...reverseMove(move), 400]));
}

// save stored moves to local storage
function storeMoves() {
  localStorage.setItem("moves", JSON.stringify(storedMoves));
}

// realizes a turn of the middle layer which is perpendicular to the
// axis v; the direction depends on the sign of v
function middleMove(v, duration = 1000, nextMove = null) {
  rotate(v, true, 1, duration, nextMove);
}

// find the middle cube with color c
function findCube(c) {
  return cubes.find(cube => cube.color == c);
}

// returns the "simplified coordinates" of middle cube with color c
function axisFromChar(c) {
  return findCube(c).coords;
}

// checks is two 3-arrays are equal component-wise
function vecEq(v1,v2) {
  return v1[0] == v2[0] && v1[1] == v2[1] && v1[2] == v2[2];
}

function findCubeAt(v) {
  return cubes.find(cube => vecEq(cube.coords,v));
}

// returns all nine cubes on the side where the middle cube has color
// c; the cubes are returned in the right order for the SVG area,
// i.e. top left to bottom right
function findAllCubes(c) {
  const mainAxis = axisFromChar(c);
  const mainIndex = indices.find(i => mainAxis[i] != 0);
  const upAxis = axisFromChar(up[c]);
  const upIndex = indices.find(i => upAxis[i] != 0);
  const upValue = upAxis[upIndex];
  const rightAxis = axisFromChar(right[c]);
  const rightIndex = indices.find(i => rightAxis[i] != 0);
  const rightValue = rightAxis[rightIndex];
  const vec = [0,0,0];
  vec[mainIndex] = mainAxis[mainIndex];
  const result = [];
  for (let y of [upValue,0,-upValue]) {
    vec[upIndex] = y;
    for (let x of [-rightValue,0,rightValue]) {
      vec[rightIndex] = x;
      result.push(cubes.find(cube => vecEq(vec,cube.coords)));
    }
  }
  return result;
}

// returns the color of cube which currently shows into direction vec
function colorInDir(cube, vec) {
  const rot = new THREE.Quaternion();
  rot.copy(cube.mesh.quaternion);
  rot.invert();
  const dir = new THREE.Vector3(...vec);
  dir.applyQuaternion(rot);
  dir.round();
  return colors[origColor(...dir.toArray())];
}

function keyHandler (e) {
  lastMouse = null;
  let c = e.keyCode ? e.keyCode : e.charCode;
  if (c != 112) 
    hideInfo();
  switch(c) {
  case 84: // t
    showTriangles = !showTriangles;
    updateSquares();
    break;
  case 83: // s
    store = [];
    break;
  case 49: // 1
  case 50: // 2
  case 51: // 3
  case 52: // 4
  case 53: // 5
  case 54: // 6
  case 55: // 7
  case 56: // 8
  case 57: // 9
    const n = c - 48;
    if (store) {      
      storedMoves[n] = store;
      storeMoves();
      store = false;
    } else if (!animation) {
      if (invert)
        runStoredMovesReverse(n);
      else
        runStoredMoves(n);
    }
    break;
  case 73: // i
    invert = true;
    store = false;
    break;
  case 66: // b
    lastChar = "b";
    break;
  case 79: // o
    lastChar = "o";
    break;
  case 71: // g
    lastChar = "g";
    break;
  case 82: // r
    lastChar = "r";
    break;
  case 87: // w
    lastChar = "w";
    break;
  case 89: // y
    lastChar = "y";
    break;
  case 67: // c
    lastChar = "c";
    break;
  case 77: // m
    lastChar = "m";
    break;
  case 48: // 0 (zero)
    if (!animation)
      resetCubes();
    break;
  case 65: // a
    if (!animation)
      randomMovement();
    break;
  case 39: // ->
    if (lastChar == "c")
      rotate([0,1,0], true, true);
    else if (lastChar == "m")
      middleMove([0,1,0]);
    else if (lastChar)
      rotate(axisFromChar(lastChar), false);
    break;
  case 37: // <-
    if (lastChar == "c")
      rotate([0,1,0], false, true);
    else if (lastChar == "m")
      middleMove([0,-1,0]);
    else if (lastChar)
      rotate(axisFromChar(lastChar));
    break;
  case 38: // up arrow
    if (lastChar == "m")
      middleMove([0,0,-1]);
    else if (lastChar == "c")
      rotate([0,0,1], false, true);
    break;
  case 40: // down arrow
    if (lastChar == "m")
      middleMove([0,0,1]);
    else if (lastChar == "c")
      rotate([0,0,1], true, true);
    break;
  case 33: // page up
    if (lastChar == "c")
      rotate([1,0,0], true, true);
    else if (lastChar == "m")
      middleMove([-1,0,0]);
    break;
  case 34: // page down
    if (lastChar == "c")
      rotate([1,0,0], false, true);
    else if (lastChar == "m")
      middleMove([1,0,0]);
    break;
  case 112: // F1
    toggleInfo();
    break;
  case 72: // h
    showSVG = !showSVG;
    resize();
    break;
  }
  if (c != 73)
    invert = false;
}

// updates lastTouch and returns true if double touch; must only be
// called once per touchStart handler
function doubleTouch() {
  const start = performance.now();
  const since = start - lastTouch;
  const result = since > 0 && since < 300;
  lastTouch = start;
  return result;
}

// touch handler for SVG area
function svgTouchStart() {
  if (doubleTouch()) {
    showSVG = false;
    resize();
  }
}

// just delegate to mouseDown
function touchStart(event) {
  event.preventDefault();
  mouseDown(event, event.touches);
}

// handler for mouse click
function mouseDown(event, touch) {
  // "special": right button or more than one finger
  let special = false;
  if (touch) {
    special = event.touches.length > 1;
    event = event.touches[0];
  } else {
    special = event.button == 2;
  }
  hideInfo();
  if (animation) {
    lastMouse = null;
    return;
  }
  const dx = event.clientX - renderer.domElement.offsetLeft;
  const dy = renderer.domElement.offsetTop - event.clientY;
  // normalized device coordinates
  let mouseNDC = new THREE.Vector2( 
    dx / renderer.domElement.width * 2 - 1,
    dy / renderer.domElement.height * 2 + 1
  );
  raycaster.setFromCamera(mouseNDC, camera);
  const intersects = raycaster.intersectObjects(cubes.map(cube => cube.mesh));
  // lastMouse remembers starting position and mouse button (0 is
  // left, 2 is right); first element is cube nearest to camera (or
  // nothing)
  if (intersects.length > 0)
    lastMouse = [intersects[0], event.clientX, event.clientY, special];
  else {
    const double = touch && doubleTouch();
    // double touch in upper left corner?
    if (double && Math.hypot(dx,dy) < diag/4) {
      resetCubes();
      return;
      // double touch in lower left corner?
    } else if (double && Math.hypot(dx,renderer.domElement.height+dy) < diag/4) {
      randomMovement();
      return;
      // right side
    } else if (double && !showSVG && Math.abs(renderer.domElement.width-dx) < diag/4) {
      showSVG = true;
      resize();
      return;
    }
    lastMouse = [null, event.clientX, event.clientY, special];
  }
}

// don't track mouse if no button is pressed
function mouseUp() {
  lastMouse = null;
}

// the 3-vector vec matches pattern component-wise either if pattern's
// component is null or if the two components are equal
function vecMatch(pattern, vec) {
  return pattern.every((pat,i) => pat == null || pat == vec[i]);
}

// returns the minimal "distance" between two angles given in radians
function angleDist(a1, a2) {
  a1 = a1 % (2*Math.PI);
  a2 = a2 % (2*Math.PI);
  return Math.min(Math.abs(a1-a2),Math.abs(a1+2*Math.PI-a2));
}

// returns an index from 0 to 3 for right, left, up, down depending on
// the movement indicated by the 2-vector [dx, dy]; r and u are the
// given angles for the right and up movements; note that angles have
// to be given in degrees and in "math direction" (positive y is up) while dy
// is in "computer direction" (where positive y is down)
function findDir(r, u, dx, dy) {
  r = (r/180*Math.PI);
  u = (u/180*Math.PI);
  const angle = Math.atan2(-dy, dx);
  let min = 1000;
  let index = -1;
  let i = 0;
  for (let given of [r, r+Math.PI, u, u+Math.PI]) {
    const dist = angleDist(angle,given);
    if (dist < min) {
      min = dist;
      index = i;
    }
    i += 1;
  }
  return index;
}

// calls is an array of four function calls in the order right, left,
// up, and down - direction is determined by the other arguments as in
// findDir
function directedCall(r, u, dx, dy, calls) {
  const [fn, ...rest] = calls[findDir(r, u, dx, dy)];
  fn(...rest);
}

function faceIndexToVec(faceIndex, mesh) {
  const dir = new THREE.Vector3(...colorDirs[Math.floor(faceIndex/2)]);
  dir.applyQuaternion(mesh.quaternion);
  dir.round();
  return dir.toArray();
}

// delegate to mouseMove
function touchMove(event) {
  event.preventDefault();
  mouseMove(event, event.touches);
}

function mouseMove(event, touch) {
  if (!lastMouse)
    return;
  if (animation) {
    lastMouse = null;
    return;
  }
  if (touch)
    event = event.touches[0];
  const [thing, oldX, oldY, special] = lastMouse;
  const dx = event.clientX - oldX;
  const dy = event.clientY - oldY;
  if (Math.hypot(dx,dy) < moveDist)
    return;
  if (!thing) {
    if (!special)
      directedCall(0, 90, dx, dy, [[0,1,0],[0,-1,0],[0,0,-1],[0,0,1]].map(vec => [rotate, vec, true, true]));
    else
      directedCall(0, 90, dx, dy, [[0,1,0],[0,-1,0],[-1,0,0],[1,0,0]].map(vec => [rotate, vec, true, true]));
  } else {
    const faceVec = faceIndexToVec(thing.faceIndex, thing.object);
    const coords = cubes.find(cube => cube.mesh == thing.object).coords;
    switch(cubeIndex(...coords)) {
      // front cubes are [-1,vert,horiz]
      case cubeIndex(-1,-1,-1): // bottom left
      directedCall(-20, 95, dx, dy, [
        [rotate, [0,-1,0], false],
        [rotate, [0,-1,0], true],
        [rotate, [0,0,-1], true],
        [rotate, [0,0,-1], false]
      ]);
      break;
      case cubeIndex(-1,-1,0): // bottom middle
      directedCall(-20, 93, dx, dy, [
        [rotate, [0,-1,0], false],
        [rotate, [0,-1,0], true],
        [middleMove, [0,0,-1]],
        [middleMove, [0,0,1]]
      ]);
      break;
      case cubeIndex(-1,-1,1): // bottom right
      if (vecEq(faceVec,[-1,0,0]))
        // front face
        directedCall(-20, 93, dx, dy, [
          [rotate, [0,-1,0], false],
          [rotate, [0,-1,0], true],
          [rotate, [0,0,1], false],
          [rotate, [0,0,1], true]
        ]);
      else
        // must be right face
        directedCall(50, 85, dx, dy, [
          [rotate, [0,-1,0], false],
          [rotate, [0,-1,0], true],
          [rotate, [-1,0,0], true],
          [rotate, [-1,0,0], false]
        ]);
      break;
      case cubeIndex(-1,0,-1): // middle left
      directedCall(-20, 95, dx, dy, [
        [middleMove, [0,1,0]],
        [middleMove, [0,-1,0]],
        [rotate, [0,0,-1], true],
        [rotate, [0,0,-1], false]
      ]);
      break;
      case cubeIndex(-1,0,0): // center
      directedCall(-20, 93, dx, dy, [
        [middleMove, [0,1,0]],
        [middleMove, [0,-1,0]],
        [middleMove, [0,0,-1]],
        [middleMove, [0,0,1]]
      ]);
      break;
      case cubeIndex(-1,0,1): // middle right
      if (vecEq(faceVec,[-1,0,0]))
        // front face
        directedCall(-20, 93, dx, dy, [
          [middleMove, [0,1,0]],
          [middleMove, [0,-1,0]],
          [rotate, [0,0,1], false],
          [rotate, [0,0,1], true]
        ]);
      else
        // must be right face
        directedCall(50, 85, dx, dy, [
          [middleMove, [0,1,0]],
          [middleMove, [0,-1,0]],
          [rotate, [-1,0,0], true],
          [rotate, [-1,0,0], false]
        ]);
      break;
      case cubeIndex(-1,1,-1): // top left
      if (vecEq(faceVec,[-1,0,0]))
        // front face
        directedCall(-20, 93, dx, dy, [
          [rotate, [0,1,0], true],
          [rotate, [0,1,0], false],
          [rotate, [0,0,-1], true],
          [rotate, [0,0,-1], false]
        ]);
      else
        // must be top face
        directedCall(-10, 25, dx, dy, [
          [rotate, [-1,0,0], false],
          [rotate, [-1,0,0], true],
          [rotate, [0,0,-1], true],
          [rotate, [0,0,-1], false]
        ]);
      break;
      case cubeIndex(-1,1,0): // top middle
      if (vecEq(faceVec,[-1,0,0]))
        // front face
        directedCall(-20, 93, dx, dy, [
          [rotate, [0,1,0], true],
          [rotate, [0,1,0], false],
          [middleMove, [0,0,-1]],
          [middleMove, [0,0,1]]
        ]);
      else
        // must be top face
        directedCall(-10, 25, dx, dy, [
          [rotate, [-1,0,0], false],
          [rotate, [-1,0,0], true],
          [middleMove, [0,0,-1]],
          [middleMove, [0,0,1]]
        ]);
      break;
      case cubeIndex(-1,1,1): // top right
      if (vecEq(faceVec,[-1,0,0]))
        // front face
        directedCall(-20, 93, dx, dy, [
          [rotate, [0,1,0], true],
          [rotate, [0,1,0], false],
          [rotate, [0,0,1], false],
          [rotate, [0,0,1], true]
        ]);
      else if (vecEq(faceVec,[0,0,1]))
        // right face
        directedCall(50, 85, dx, dy, [
          [rotate, [0,1,0], true],
          [rotate, [0,1,0], false],
          [rotate, [-1,0,0], true],
          [rotate, [-1,0,0], false]
        ]);
      else
        // must be top face
        directedCall(-10, 25, dx, dy, [
          [rotate, [-1,0,0], false],
          [rotate, [-1,0,0], true],
          [rotate, [0,0,1], false],
          [rotate, [0,0,1], true]
        ]);
      break;
      // top cubes are [vert,1,horiz]
      case cubeIndex(1,1,-1): // top left
      directedCall(-10, 25, dx, dy, [
        [rotate, [1,0,0], true],
        [rotate, [1,0,0], false],
        [rotate, [0,0,-1], true],
        [rotate, [0,0,-1], false]
      ]);
      break;
      case cubeIndex(1,1,0): // top middle
      directedCall(-10, 25, dx, dy, [
        [rotate, [1,0,0], true],
        [rotate, [1,0,0], false],
        [middleMove, [0,0,-1]],
        [middleMove, [0,0,1]]
      ]);
      break;
      case cubeIndex(1,1,1): // top right
      if (vecEq(faceVec,[0,1,0]))
        // top face
        directedCall(-10, 25, dx, dy, [
          [rotate, [1,0,0], true],
          [rotate, [1,0,0], false],
          [rotate, [0,0,1], false],
          [rotate, [0,0,1], true]
        ]);
        // must be right face
        directedCall(50, 85, dx, dy, [
          [rotate, [0,1,0], true],
          [rotate, [0,1,0], false],
          [rotate, [1,0,0], false],
          [rotate, [1,0,0], true]
        ]);
      break;
      case cubeIndex(0,1,-1): // middle left
      directedCall(-10, 25, dx, dy, [
        [middleMove, [1,0,0]],
        [middleMove, [-1,0,0]],
        [rotate, [0,0,-1], true],
        [rotate, [0,0,-1], false]
      ]);
      break;
      case cubeIndex(0,1,0): // center
      directedCall(-10, 25, dx, dy, [
        [middleMove, [1,0,0]],
        [middleMove, [-1,0,0]],
        [middleMove, [0,0,-1]],
        [middleMove, [0,0,1]]
      ]);
      break;
      case cubeIndex(0,1,1): // middle right
      if (vecEq(faceVec,[0,1,0]))
        // top face
        directedCall(-10, 25, dx, dy, [
          [middleMove, [1,0,0]],
          [middleMove, [-1,0,0]],
          [rotate, [0,0,1], false],
          [rotate, [0,0,1], true]
        ]);
        // must be right face
        directedCall(50, 85, dx, dy, [
          [rotate, [0,1,0], true],
          [rotate, [0,1,0], false],
          [middleMove, [-1,0,0]],
          [middleMove, [1,0,0]]
        ]);
      break;
      // right cubes are [horiz,vert,1]
      case cubeIndex(1,-1,1): // bottom right
      directedCall(50, 85, dx, dy, [
        [rotate, [0,-1,0], false],
        [rotate, [0,-1,0], true],
        [rotate, [1,0,0], false],
        [rotate, [1,0,0], true]
      ]);
      break;
      case cubeIndex(0,-1,1): // bottom middle
      directedCall(50, 85, dx, dy, [
        [rotate, [0,-1,0], false],
        [rotate, [0,-1,0], true],
        [middleMove, [-1,0,0]],
        [middleMove, [1,0,0]]
      ]);
      break;
      case cubeIndex(1,0,1): // middle right
      directedCall(50, 85, dx, dy, [
        [middleMove, [0,1,0]],
        [middleMove, [0,-1,0]],
        [rotate, [1,0,0], false],
        [rotate, [1,0,0], true]
      ]);
      break;
      case cubeIndex(0,0,1): // center
      directedCall(50, 85, dx, dy, [
        [middleMove, [0,1,0]],
        [middleMove, [0,-1,0]],
        [middleMove, [-1,0,0]],
        [middleMove, [1,0,0]]
      ]);
      break;
    }
  }
}

function hideInfo() {
  document.getElementById("info").style.display = "none";
}

function toggleInfo() {
  document.getElementById("info").style.display = document.getElementById("info").style.display == "none" ? "block" : "none";
}

// we first have to load the textures
window.onload = createSides;

/// Copyright (c) 2016, Dr. Edmund Weitz. All rights reserved.

/// A good explanation for some of the techniques used here is the
/// article "Advanced Character Physics" by Thomas Jakobsen

// lots of global variables:

// width and height of HTML canvas
var W;
var H;

// the image is represented as a rectangular grid with nx * ny supporting points;
// the nx to ny ratio should be approximately 3/2 and there shouldn't be much more of them
var nx = 112;
var ny = 72;
// total number of points (vertices)
var nPoints = nx * ny;
// if vertices are directly adjacent either horizontally or vertically, the pair forms
// a constraint, i.e. they strive to keep their original distance; this leads to the
// following number of constraints
var nConstraints = (nx - 1) * ny + (ny - 1) * nx;
// although we have a rectangular grid, WebGL will draw in terms of triangles; each
// rectangle of the grid will be represented by two triangles; this leads to the
// following number of triangles
var nTriangles = (nx - 1) * (ny - 1) * 2;
// some vertices at the top of the grid are fixed so that they aren't moved by
// gravitation or constraints satisfaction; this is their number
var nFixed = 17;
// the number of relaxations (global constraint satisfactions) per iteration
var nEnforcements = 3;
// a flag which is true if the WebGL animation is currently running
var running = false;
// the index (into the "points" array) of the vertex which is currently moved by the mouse
var movIndex = -1;
// the image (technically this will be the texture)
var image = new Image();
// the array were the vertices are stored; three floating point numbers (x, y, and z coordinate)
// per vertex, so the array has nPoints * 3 elements; all computations are done in NDC,
// i.e. all values are between -1.0 and +1.0
var points;
// the y coordinates of the last time step; this is used for Verlet integration, see below
var pointsPrevY;
// an array of nPoints integers which are technically used as booleans; if an entry is true
// (i.e. not zero), then the corresponding vertex must never be moved
var fixed;
// constraints describe distances adjacent vertices strive to achieve; this arrays hold two
// vertex indices per constraint
var constraintIndices;
// and these are the corresponding distances (or actually their
// squares), one floating point number per constraint
var constraintDist;
// vertex indices for the triangles; three indices per triangle, so we have 3 * nTriangles
// elements
var indices;
// array with normal vectors for the vertices; has the same number of elements as "points";
// note that these vectors are NOT normalized, the vertex shader will take care of that
var normals;
// global variable to hold the asm.js module
var fast;
// global variable holding the WebGL context
var ctx;
// buffers used by WebGL
var vertexBuffer, normalsBuffer;
// used to remember original NDC position of a vertex that is moved
var oldX, oldY;

// reads the text of the HTML element with the ID "id" and compiles it
// into a WebGL shader; the global variable "ctx" must have been set up already
function prepareShader (id) {
  var el = document.getElementById(id);
  // shader type is inferred from the element type; see HTML source
  var shader = ctx.createShader(el.type == "x-shader/x-fragment" ? ctx.FRAGMENT_SHADER : ctx.VERTEX_SHADER);
  ctx.shaderSource(shader, el.text);
  ctx.compileShader(shader);
  
  if (!ctx.getShaderParameter(shader, ctx.COMPILE_STATUS)) {
    console.log(ctx.getShaderInfoLog(shader));
    return null;
  }
  return shader;
}

// creates a WebGL program consisting of two shaders (see above);
// the global variable "ctx" must have been set up already
function prepareProgram (vertId, fragId) {
  var prg = ctx.createProgram();
  ctx.attachShader(prg, prepareShader(vertId, false));
  ctx.attachShader(prg, prepareShader(fragId, true));
  ctx.linkProgram(prg);
  
  if (!ctx.getProgramParameter(prg, ctx.LINK_STATUS)) {
    console.log("Problem with shaders...");
    return false;
  }
  
  ctx.useProgram(prg);

  return prg;
}

// creates a WebGL buffer of type "bufferType" and fills it with "data";
// if "varName" is specified, the buffer is associated with the vertex shader
// named by this parameter in the program "prg"; "ctx" must have been set up already
function prepareBuffer (prg, bufferType, data, varName, n, dynamic) {
  var buffer = ctx.createBuffer();
  ctx.bindBuffer(bufferType, buffer);
  ctx.bufferData(bufferType, data, dynamic ? ctx.DYNAMIC_DRAW : ctx.STATIC_DRAW);

  if (varName) {
    var loc = ctx.getAttribLocation(prg, varName);
    ctx.vertexAttribPointer(loc, n, ctx.FLOAT, false, 0, 0);
    ctx.enableVertexAttribArray(loc);
  }

  return buffer;
}

// instructs WebGL to draw the whole scene using the global context "ctx";
// needs certain global buffers to be set up
function drawScene () {
  ctx.clearColor(1.0, 1.0, 1.0, 1.0);
  ctx.enable(ctx.DEPTH_TEST);
  ctx.clear(ctx.COLOR_BUFFER_BIT | ctx.DEPTH_BUFFER_BIT);
  ctx.viewport(0, 0, W, H);

  // the following two buffers have to be updated on each rendering call
  // because their values might change
  ctx.bindBuffer(ctx.ARRAY_BUFFER, vertexBuffer);
  ctx.bufferData(ctx.ARRAY_BUFFER, points, ctx.DYNAMIC_DRAW);

  ctx.bindBuffer(ctx.ARRAY_BUFFER, normalsBuffer);
  ctx.bufferData(ctx.ARRAY_BUFFER, normals, ctx.DYNAMIC_DRAW);

  ctx.drawElements(ctx.TRIANGLES, nTriangles * 3, ctx.UNSIGNED_SHORT, 0);
}

// asm.js module to provide fast version of some functions which are
// called often
function ParticleSystem (stdlib, foreign, buffer) {
  "use asm";

  // the parameter "buffer" will point to a heap where all the
  // relavant global arrays like "points" where allocated; this module
  // shares these arrays with the rest of the program via the
  // following pointers and offsets
  var points = new stdlib.Float32Array(buffer);
  var pointsPrevY = new stdlib.Float32Array(buffer);
  var pointsPrevOffset = 0;
  var constraintIndices = new stdlib.Uint32Array(buffer);
  var constraintIndicesOffset = 0;
  var constraintDist = new stdlib.Float32Array(buffer);
  var constraintDistOffset = 0;
  var normals = new stdlib.Float32Array(buffer);
  var normalsOffset = 0;
  var indices = new stdlib.Uint16Array(buffer);
  var indicesOffset = 0;
  var fixed = new stdlib.Uint8Array(buffer);
  var fixedOffset = 0;

  // module versions of some global variables, set when then module is
  // initialized
  var nPoints = 0;
  var nConstraints = 0;
  var nEnforcements = 0;
  var nTriangles = 0;

  // the module has to be initialized once before it can be used
  function init (nP, nC, nE, nT) {
    nP = nP | 0;
    nC = nC | 0;
    nE = nE | 0;
    nT = nT | 0;

    var offset = 0;

    // "global" variables
    nPoints = nP;
    nConstraints = nC;
    nEnforcements = nE;
    nTriangles = nT;

    // prepare array access, see comments above; note that offsets are
    // measured as numbers of previous elements and not in octets; for
    // this to work, arrays must be arranged in decreasing order of
    // element size - four octets, two octets, one octet
    offset = (nPoints * 3) | 0;
    pointsPrevOffset = offset;

    offset = (offset + nPoints) | 0;
    constraintIndicesOffset = offset;

    offset = (offset + ((nConstraints * 2) | 0)) | 0;
    constraintDistOffset = offset;

    offset = (offset + nConstraints) | 0;
    normalsOffset = offset;

    offset = (offset + ((nPoints * 3)|0)) | 0;
    offset = (offset * 2) | 0;
    indicesOffset = offset;

    offset = (offset + ((nTriangles * 3)|0)) | 0;
    offset = (offset * 2) | 0;
    fixedOffset = offset;
  }

  // recomputes all vertex normals
  function computeNormals () {
    var normalsPtr = 0;
    var normalsEnd = 0;
    var indicesPtr = 0;
    var pointsPtr = 0;
    var i = 0;
    var k = 0;
    var p0x = 0.0;
    var p0y = 0.0;
    var p0z = 0.0;
    var v1x = 0.0;
    var v1y = 0.0;
    var v1z = 0.0;
    var v2x = 0.0;
    var v2y = 0.0;
    var v2z = 0.0;

    // first set all normals to zero
    normalsPtr = normalsOffset << 2;
    normalsEnd = (normalsOffset + ((nPoints * 3)|0)) << 2;
    while ((normalsPtr|0) < (normalsEnd|0)) {
      normals[normalsPtr >> 2] = 0.0;
      normalsPtr = (normalsPtr + 1) | 0;
      normals[normalsPtr >> 2] = 0.0;
      normalsPtr = (normalsPtr + 1) | 0;
      normals[normalsPtr >> 2] = 0.0;
      normalsPtr = (normalsPtr + 1) | 0;
    }

    // now use the "indices" array to walk through all triangles; pick
    // one of the three vertices and call it "p0", then compute the
    // vectors "v1" and "v2" pointing from "p0" to the other two
    // vertices
    while ((i|0) < (nTriangles|0)) {
      indicesPtr = (indicesOffset + ((3 * i)|0)) << 1;
      k = indices[indicesPtr >> 1]|0;
      pointsPtr = (k * 12)|0;
      p0x = +points[pointsPtr >> 2];
      pointsPtr = (pointsPtr + 4)|0;
      p0y = +points[pointsPtr >> 2];
      pointsPtr = (pointsPtr + 4)|0;
      p0z = +points[pointsPtr >> 2];

      indicesPtr = (indicesPtr + 2)|0;
      k = indices[indicesPtr >> 1]|0;
      pointsPtr = (k * 12)|0;
      v1x = (+points[pointsPtr >> 2]) - p0x;
      pointsPtr = (pointsPtr + 4)|0;
      v1y = (+points[pointsPtr >> 2]) - p0y;
      pointsPtr = (pointsPtr + 4)|0;
      v1z = (+points[pointsPtr >> 2]) - p0z;

      indicesPtr = (indicesPtr + 2)|0;
      k = indices[indicesPtr >> 1]|0;
      pointsPtr = (k * 12)|0;
      v2x = (+points[pointsPtr >> 2]) - p0x;
      pointsPtr = (pointsPtr + 4)|0;
      v2y = (+points[pointsPtr >> 2]) - p0y;
      pointsPtr = (pointsPtr + 4)|0;
      v2z = (+points[pointsPtr >> 2]) - p0z;

      // compute the cross product of "v1" and "v2" and store it in "p0"
      p0x = v1y * v2z - v1z * v2y;
      p0y = v1z * v2x - v1x * v2z;
      p0z = v1x * v2y - v1y * v2x;

      // update all three vertex normals by adding (!) "p0"; we need
      // to add here because most vertices will be influenced by
      // several triangles they belong to
      indicesPtr = (indicesOffset + ((3 * i)|0)) << 1;
      k = indices[indicesPtr >> 1]|0;
      normalsPtr = (normalsOffset + ((k * 3)|0)) << 2;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0x;
      normalsPtr = (normalsPtr + 4)|0;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0y;
      normalsPtr = (normalsPtr + 4)|0;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0z;

      indicesPtr = (indicesPtr + 2)|0;
      k = indices[indicesPtr >> 1]|0;
      normalsPtr = (normalsOffset + ((k * 3)|0)) << 2;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0x;
      normalsPtr = (normalsPtr + 4)|0;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0y;
      normalsPtr = (normalsPtr + 4)|0;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0z;

      indicesPtr = (indicesPtr + 2)|0;
      k = indices[indicesPtr >> 1]|0;
      normalsPtr = (normalsOffset + ((k * 3)|0)) << 2;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0x;
      normalsPtr = (normalsPtr + 4)|0;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0y;
      normalsPtr = (normalsPtr + 4)|0;
      normals[normalsPtr >> 2] = (+normals[normalsPtr >> 2]) + p0z;

      i = (i + 1)|0;
    }
    // note that we don't normalize at the end; the vertex shader will
    // do that
  }

  // given NDC coordinates "x" and "y" returns the index of the
  // nearest vertex (ignoring the "z" coordinate) if there is one
  // nearby
  function getNearestVertex (x, y) {
    x = +x;
    y = +y;
    var i = 0;
    var min = 0.0;
    var minIndex = 0;
    var pointsPtr = 0;
    var pointsEnd = 0;
    var vx = 0.0;
    var vy = 0.0;
    var dist = 0.0;

    min = 1000000.0;
    minIndex = -1;
    pointsPtr = 0;
    pointsEnd = (nPoints * 12) | 0;

    while ((pointsPtr|0) < (pointsEnd|0)) {
      vx = (+points[pointsPtr >> 2]) - x;
      vy = (+points[(pointsPtr + 4) >> 2]) - y;
      dist = vx * vx + vy * vy;
      if (dist < min)
        // the necessary proximity is hard-coded here:
        if (dist < 0.0002) {
          min = dist;
          minIndex = i;
        }
      i = (i + 1)|0;
      pointsPtr = (pointsPtr + 12)|0;
    }
    return minIndex|0;
  }

  // updates all vertex positions by taking gravity into account 
  function move () {
    var pointsPtr = 0;
    var pointsEnd = 0;
    var pointsPrevPtr = 0;
    var fixedPtr = 0;
    var tempY = 0.0;

    pointsPtr = 4;
    pointsEnd = (nPoints * 12) | 0;
    pointsPrevPtr = pointsPrevOffset << 2;
    fixedPtr = fixedOffset;

    // we are only interested in the "y" direction; this uses Verlet
    // integration and we thus need the previous position
    while ((pointsPtr|0) < (pointsEnd|0)) {
      // skip vertices which are fixed
      if (!(fixed[fixedPtr]|0)) {
        tempY = +points[pointsPtr >> 2];
        // gravity is hard-coded here; this is actually 0.0002 *
        // timeStep * timeStep, but if we "normalize" to timeStep = 1,
        // we can ignore this complication
        points[pointsPtr >> 2] = 2.0 * (+points[pointsPtr >> 2]) - (+pointsPrevY[pointsPrevPtr >> 2]) - 0.0002;
        pointsPrevY[pointsPrevPtr >> 2] = tempY;
      }
      pointsPtr = (pointsPtr + 12) | 0;
      pointsPrevPtr = (pointsPrevPtr + 4) | 0;
      fixedPtr = (fixedPtr + 1) | 0;
    }
  }

  // goes through all constraints and updates vertex positions in an
  // attempt to achieve the distances the vertices strive to achieve
  function enforceConstraints () {
    var constraintIndicesPtr = 0;
    var constraintIndicesEnd = 0;
    var constraintDistPtr = 0;
    var index = 0;
    var pointsPtrA = 0;
    var pointsPtrB = 0;
    var fixedPtrA = 0;
    var fixedPtrB = 0;
    var deltaX = 0.0;
    var deltaY = 0.0;
    var deltaZ = 0.0;
    var distSq = 0.0;
    var sp = 0.0;
    var diff = 0.0;

    constraintIndicesPtr = constraintIndicesOffset << 2;
    constraintIndicesEnd = (constraintIndicesOffset + (nConstraints << 1)) << 2;
    constraintDistPtr = constraintDistOffset << 2;

    while ((constraintIndicesPtr|0) < (constraintIndicesEnd|0)) {
      // get vertices A and B from constraints
      index = constraintIndices[constraintIndicesPtr >> 2]|0;
      pointsPtrA = (index * 12)|0;
      fixedPtrA = (fixedOffset + index)|0;
      constraintIndicesPtr = (constraintIndicesPtr + 4)|0;
      index = constraintIndices[constraintIndicesPtr >> 2]|0;
      pointsPtrB = (index * 12)|0;
      fixedPtrB = (fixedOffset + index)|0;
      constraintIndicesPtr = (constraintIndicesPtr + 4)|0;

      // compute the vector "delta" from A to B
      deltaX = (+points[pointsPtrB >> 2]) - (+points[pointsPtrA >> 2]);
      deltaY = (+points[(pointsPtrB + 4) >> 2]) - (+points[(pointsPtrA + 4) >> 2]);
      deltaZ = (+points[(pointsPtrB + 8) >> 2]) - (+points[(pointsPtrA + 8) >> 2]);
      // the square of the distance we want to achieve
      distSq = +constraintDist[constraintDistPtr >> 2];
      constraintDistPtr = (constraintDistPtr + 4)|0;
      // the scalar product of "delta" with itself, i.a. the square of
      // the current distance of A and B
      sp = deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ;
      // we now want to compute (in "diff") the relative deviation
      // from the real distance (or rather half of it), so that we can
      // update "delta" accordingly to get a "correction vector";
      // however, in order to do that we would need to compute the
      // square root of "sp" which is expensive; instead, we
      // approximate the square root near "distSq" using the first
      // Taylor expansion:
      diff = 0.5 * (sp - distSq) / (distSq + sp);
      deltaX = deltaX * diff;
      deltaY = deltaY * diff;
      deltaZ = deltaZ * diff;
      // only apply to vertices which aren't fixed
      if (!(fixed[fixedPtrA]|0)) {
        points[pointsPtrA >> 2] = (+points[pointsPtrA >> 2]) + deltaX;
        points[(pointsPtrA + 4) >> 2] = (+points[(pointsPtrA + 4) >> 2]) + deltaY;
        points[(pointsPtrA + 8) >> 2] = (+points[(pointsPtrA + 8) >> 2]) + deltaZ;
      }
      if (!(fixed[fixedPtrB]|0)) {
        points[pointsPtrB >> 2] = (+points[pointsPtrB >> 2]) - deltaX;
        points[(pointsPtrB + 4) >> 2] = (+points[(pointsPtrB + 4) >> 2]) - deltaY;
        points[(pointsPtrB + 8) >> 2] = (+points[(pointsPtrB + 8) >> 2]) - deltaZ;
      }
    }
  }

  // all updates in one function: first gravity, then constraints
  // (several times, i.e. relaxation), then recompute normals
  function update () {
    var i = 0;

    move();
    while ((i|0) < (nEnforcements|0)) {
      enforceConstraints();
      i = (i + 1)|0;
    }
    computeNormals();
  }

  // return the functions used by the main program
  return {
    init: init,
    getNearestVertex: getNearestVertex,
    computeNormals: computeNormals,
    update: update
  };
}

// computes NDC from a mouse click relative to the canvas
function getMousePos(canvas, event) {
  var rect = canvas.getBoundingClientRect();
  return [
    2 * (event.clientX - rect.left) / W - 1,
    -2 * (event.clientY - rect.top) / H + 1
  ];
}

// initializes the data; called whenever a new image has been loaded
// into the HTML page
function initSimulation (img) {
  // stop moving vertices
  movIndex = -1;

  // array for the UV coordinates for the texture
  var uvCoords = [];

  // "imgW" and "imgH" are the width and height of the image in NDC,
  // they are set up in such a way that the width is not greater than
  // 1.8, the height not greater than 1.6 and the width/height ratio
  // corresponds to the original image
  var imgW, imgH;
  var ratio = (H / W) * (img.width / img.height);
  if (ratio >= 1.8 / 1.6) {
    imgW = 1.8;
    imgH = imgW / ratio;
  } else {
    imgH = 1.6;
    imgW = imgH * ratio;
  }
  // step sizes for the NDC grid
  var xStep = imgW / nx;
  var yStep = imgH / ny;
  // step sizes for the UV texture grid
  var uStep = 1 / (nx - 1);
  var vStep = 1 / (ny - 1);
  var i = 0;
  var j = 0;
  var v = 0;
  // the following loop sets up the vertex positions in the grid and
  // the corresponding UV coordinates; image starts at height 0.9 in
  // NDC so that we have enough space at the bottom
  for (var y = 0.9 - yStep / 2; y > 0.9 - imgH; y -= yStep) {
    var u = 0;
    // image is centered horizontally
    for (var x = -imgW / 2 + xStep / 2; x < imgW / 2; x += xStep) {
      points[i++] = x;
      points[i++] = y;
      points[i++] = 0.0;
      pointsPrevY[j++] = y;
      uvCoords.push(u, v);
      u += uStep;
    }
    v += vStep;
  }

  // mark the vertices which are fixed; they'll be equidistant and in
  // the first row
  fixed.fill(0);
  for (var i = 0; i < nFixed - 1; i++)
    fixed[Math.round(i * nx / (nFixed - 1))] = 1;
  fixed[nx - 1] = 1;

  // set up the constraints; each vertex is paired with its right and
  // bottom neighbor (if it has one)
  var k = 0;
  for (i = 0; i < nx * ny; i++) {
    var j = i + nx;
    if (j < nx * ny) {
      constraintIndices[2 * k] = i;
      constraintIndices[2 * k + 1] = j;
      // we want the squared values (see algorithm above)
      constraintDist[k++] = yStep * yStep;
    }
    j = i + 1;
    if (j % nx != 0) {
      constraintIndices[2 * k] = i;
      constraintIndices[2 * k + 1] = j;
      constraintDist[k++] = xStep * xStep;
    }
  }

  // set up indices for the triangles
  var ii = 0;
  j = 0;
  i = 0;
  // nested loop to reach all rectangles
  while (j < ny - 1) {
    k = 0;
    while (k < nx - 1) {
      // two triangles per rectangle; counter-clockwise
      indices[ii++] = i;
      indices[ii++] = i + nx;
      indices[ii++] = i + 1;

      indices[ii++] = i + 1;
      indices[ii++] = i + nx;
      indices[ii++] = i + 1 + nx;

      i++;
      k++;
    }
    i++;
    j++;
  }

  // compute initial normals
  fast.computeNormals();

  // compile the shaders and prepare WebGL program; shader code is in HTML page
  var prg = prepareProgram("vertSrc", "fragSrc");
  // set up buffers for the vertex shader attributes
  vertexBuffer = prepareBuffer(prg, ctx.ARRAY_BUFFER, points, "aVertexPosition", 3, true);
  prepareBuffer(prg, ctx.ARRAY_BUFFER, new Float32Array(uvCoords), "aTextureCoord", 2);
  normalsBuffer = prepareBuffer(prg, ctx.ARRAY_BUFFER, normals, "aVertexNormal", 3, true);
  prepareBuffer(prg, ctx.ELEMENT_ARRAY_BUFFER, indices);

  // prepare the texture (which is the image)
  var texture = ctx.createTexture();
  ctx.bindTexture(ctx.TEXTURE_2D, texture);
  ctx.texImage2D(ctx.TEXTURE_2D, 0, ctx.RGBA, ctx.RGBA, ctx.UNSIGNED_BYTE, img);
  ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MAG_FILTER, ctx.NEAREST);
  ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.NEAREST);
  ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_S, ctx.CLAMP_TO_EDGE);
  ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_T, ctx.CLAMP_TO_EDGE);
  ctx.bindTexture(ctx.TEXTURE_2D, null);
  
  ctx.activeTexture(ctx.TEXTURE0);
  ctx.bindTexture(ctx.TEXTURE_2D, texture);
  ctx.uniform1i(ctx.getUniformLocation(prg, "uSampler"), 0);

  // the direction of the light (needed to show some shadows);
  // hard-coded value
  ctx.uniform3f(ctx.getUniformLocation(prg, "uLightDirection"), 0.0, -0.7, -1.0);
  
  // draw once, then wait a bit before we really start
  drawScene();
  running = true;
  // initial shuffling of constraints, see below
  shuffleConstraints(400);
  setTimeout(animate, 1000);
}

// called when we right-click into the canvas; if we clicked near one
// of the vertices, change its "z" coordinate and fix it temporarily;
// this results in a cheap simulation of a push
function nudgeNearestVertex (x, y) {
  var minIndex = fast.getNearestVertex(x, y);
  if (minIndex != -1) {
    if (!fixed[minIndex]) {
      // push "intensity" is hard-coded here
      points[minIndex * 3 + 2] -= .07;
      tempFix(minIndex);
    }
  }
}

// mark vertex numbered by "index" as fixed and then half a second
// later "unfix" it again
function tempFix (index) {
  fixed[index] = 1;
  setTimeout(function () {
    fixed[index] = 0;
  }, 500);
}

// the result of the relaxation above depends on the order of the
// constraints, so we shuffle them a bit every now and then
function shuffleConstraints (n) {
  n = n | 50;
  for (var i = 0; i < n; i++) {
    var j = Math.round(nConstraints * Math.random());
    var k = Math.round(nConstraints * Math.random());
    if (j != k) {
      var temp = constraintIndices[2 * j];
      constraintIndices[2 * j] = constraintIndices[2 * k];
      constraintIndices[2 * k] = temp;
      temp = constraintIndices[2 * j + 1];
      constraintIndices[2 * j + 1] = constraintIndices[2 * k + 1];
      constraintIndices[2 * k + 1] = temp;
      temp = constraintDist[j];
      constraintDist[j] = constraintDist[k];
      constraintDist[k] = temp;
    }
  }
}

// runs every 50 milliseconds to recompute the vertex positions and redraw the scene
function animate () {
  shuffleConstraints();
  fast.update();
  drawScene();
  if (running)
    setTimeout(animate, 50);
}

// called once after the page has loaded
function initPage () {
  // set canvas to maximum size of the page, then store this as "W"
  // and "H"
  var canvas = document.getElementById("canvas");
  ctx = canvas.getContext("webgl");
  ctx.canvas.width = window.innerWidth;
  ctx.canvas.height = window.innerHeight;
  W = ctx.canvas.width;
  H = ctx.canvas.height;

  // we don't want the standard right-click behavior of the canvas
  canvas.addEventListener("contextmenu", function(event) {
    event.preventDefault();
  });

  // called when we click on the canvas
  canvas.addEventListener("mousedown", function(event) {
    if (!running)
      return;
    var mousePos = getMousePos(canvas, event);
    // right mouse button
    if (event.button == 2) {
      nudgeNearestVertex(mousePos[0], mousePos[1]);
    // left mouse button
    } else if (event.button == 0) {
      // if we move a vertex with the mouse it is marked as "fixed"
      // (see above) so that the simulation doesn't change its
      // position
      if (movIndex != -1)
        fixed[movIndex] = 0;
      movIndex = fast.getNearestVertex(mousePos[0], mousePos[1]);
      if (movIndex != -1) {
        if (fixed[movIndex])
          movIndex = -1;
        else {
          fixed[movIndex] = 1;
          // remember old position
          oldX = points[movIndex * 3];
          oldY = points[movIndex * 3 + 1];
        }
      }
    }
  }, false);

  // mouse up again, stop moving
  canvas.addEventListener("mouseup", function(event) {
    if (movIndex != -1) {
      fixed[movIndex] = 0;
      movIndex = -1;
    }
  }, false);

  // called whenever mouse moves
  canvas.addEventListener("mousemove", function(event) {
    if (!running)
      return;
    // we only need to do something if a vertex is currently moved
    if (movIndex != -1) {
      var mousePos = getMousePos(canvas, event);
      var vx = mousePos[0] - oldX;
      var vy = mousePos[1] - oldY;
      // hard-coded: if we pulled the vertex too far away from its
      // original position, it "slips away"
      var dist = vx * vx + vy * vy;
      if (dist > 0.01) {
        fixed[movIndex] = 0;
        movIndex = -1;
      } else {
        points[movIndex * 3] = mousePos[0];
        points[movIndex * 3 + 1] = mousePos[1];
        // simulate dragging the vertex "towards us"
        points[movIndex * 3 + 2] = dist * 10;
      }
    }
  }, false);

  // handlers for dragging a picture onto the canvas
  canvas.addEventListener("dragover", dragOverHandler, false);
  canvas.addEventListener("drop", dropHandler, false);

  // the heap that is shared with the asm.js Module above; this is
  // where most of the global arrays are situated
  var heap = new ArrayBuffer(0x80000);

  // put the arrays on the heap
  var offset = 0;
  var length = nPoints * 3;
  points = new Float32Array(heap, offset, length);

  offset += length * 4;
  length = nPoints;
  pointsPrevY = new Float32Array(heap, offset, length);

  offset += length * 4;
  length = nConstraints * 2;
  constraintIndices = new Uint32Array(heap, offset, length);

  offset += length * 4;
  length = nConstraints;
  constraintDist = new Float32Array(heap, offset, length);

  offset += length * 4;
  length = nPoints * 3;
  normals = new Float32Array(heap, offset, length);

  offset += length * 4;
  length = nTriangles * 3;
  indices = new Uint16Array(heap, offset, length);

  offset += length * 2;
  length = nPoints;
  fixed = new Uint8Array(heap, offset, length);

  // compile and initialize the asm.js module
  fast = ParticleSystem(window, null, heap);
  fast.init(nPoints, nConstraints, nEnforcements, nTriangles);

  // whenever "image" is assigned a new source location, we stop the
  // running simulation and start a new one
  image.onload = function() {
    if (running) {
      running = false;
      setTimeout(function () {
        initSimulation(image);
      }, 500);
    } else
      initSimulation(image);
  };
};

// show "copy" cursor when we drag something over the canvas
function dragOverHandler (event) {
  event.stopPropagation();
  event.preventDefault();
  event.dataTransfer.dropEffect = "copy";
}

// called when we drop something onto the canvas
function dropHandler (event) {
  event.stopPropagation();
  event.preventDefault();

  var file = event.dataTransfer.files[0];
  // only accept images
  if (file.type.match("image.*")) {
    // hide text
    document.getElementById("info").style.display = "none";
    readImage(file);
  }
}

// called once the dropped file has been read
function readImage (file) {
  var reader = new FileReader();
  reader.addEventListener("load", function () {
    // this will trigger the handler from above
    image.src = reader.result;
  });
  reader.readAsDataURL(file);  
}

// randomly selects one of several default images (instead of a
// dropped one); the user can click a link to do this
function defaultImage () {
  document.getElementById("info").style.display = "none";
  var r = Math.floor(Math.random() * 3);
  if (r == 0)
    image.src = "thunderstorm.jpg"; 
  else if (r == 1)
    image.src = "wall.jpg"; 
  else
    image.src = "mjosa.jpg";
}

window.onload = initPage;

// Copyright (c) 2018, Dr. Edmund Weitz.  All rights reserved.

// SVG element
var svg;
// SVG namespace
var svgNS;
// real and imaginary parts of Fourier coefficients
var coeffR, coeffI;
// coefficients range from -n+1 to n
var n;
// factor to adjust size of red pixel to size of viewbox
var factor;
// path element in SVG
var path;
// how much the "z" argument (between -pi and pi) is increased each step
// value is changed dynamically
var inc = 0.02;
// time in milliseconds between two frames
// value is changed dynamically
var time = 50;
// array to hold the circles in SVG
var circles = [];
// number of circles currently in use (not counting the red dot)
var noCircles = 0;
// used to stop the animation
var running = false;
// toggles fill mode
var fill = false;
// the maximum number of circles is 2^depth - 1
var depth = 7;
// for clipboard function
var lastChar = "";

// "dummy" event handler to disable default event handling
function prevent (event) {
  event.preventDefault();
}

// utility function to set several attributes of an element at once
function setAttributes (el, attrs) {
  for (let [name, val] of attrs) {
    el.setAttribute(name, val);
  }
}

// adjust size of SVG area to size of window, the viewBox will do the rest
function setSize () {
  svg.setAttribute("width", window.innerWidth);
  svg.setAttribute("height", window.innerHeight);
}

function randomImage () {
  newImage(Math.floor(Math.random() * 6));
}

// called when page has finished loading
function init () {
  svg = document.getElementById("svg");
  svgNS = svg.namespaceURI;
  ["contextmenu", "drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave", "drop"].forEach(function (event) {
    svg.addEventListener(event, prevent);
  });
  setSize();
  window.onresize = setSize;
  document.onkeydown = keyHandler;
  randomImage();
}

function updateInfo () {
  document.getElementById("number").innerHTML = `${noCircles}/${Math.pow(2,depth)-1}`;
}

function newImage (i) {
  clearSVG();
  noCircles = 1;
  updateInfo();
  coeffR = coeffsR[i];
  coeffI = coeffsI[i];
  n = coeffR.length / 2;
  svg.setAttribute("viewBox", viewBoxes[i]);
  factor = factors[i];
  startAnimation(1);
};

// computes the coefficient index for the counter c;
// c = 0 yields 0, c = 1 yields 1, c = 2 yields -1, c = 3 yields 2, ...
function indexFor (c) {
  if (c % 2 == 0)
    return -c / 2;
  else
    return (c + 1) / 2;
}

// recursively computes the center of the circle with the number c
// for the argument z (between -pi and pi);
// circle numbering starts at c = 0
function pos (c, z, Re, Im) {
  if (c == 0) {
    return [coeffR[n-1], coeffI[n-1]];
  } else {
    // only use recursion if argument not supplied
    if (!Re)
      [Re, Im] = pos(c - 1, z);
    let k = indexFor(c);
    let expRe = Math.cos(k*z);
    let expIm = Math.sin(k*z);
    let i = k + n - 1;
    Re += coeffR[i]*expRe - coeffI[i]*expIm;
    Im += coeffR[i]*expIm + coeffI[i]*expRe;
    return [Re, Im];
  }
}

// computes the radius for the circle with the number c;
// circle numbering starts at c = 0
function radius (c) {
  let i = indexFor(c + 1) + n - 1;
  let Re = coeffR[i];
  let Im = coeffI[i];
  return Math.sqrt(Re*Re + Im*Im);
}

// removes all SVG elements
function clearSVG () {
  if (path)
    svg.removeChild(path);
  path = null;
  for (let i = 0; i < circles.length; i++)
    svg.removeChild(circles[i]);
  circles = [];
}

// initiates new animation
function startAnimation (cm) {
  running = false;
  // wait a bit so animation can safely stop
  window.setTimeout(() => {reallyStartAnimation(cm);}, 500);
}

function reallyStartAnimation (cm) {
  if (running)
    return;
  inc =  1 / cm / (10 - Math.min(depth, 8));
  if (inc > 0.05)
    inc = 0.05;
  time = 30 / Math.log2(cm);
  clearSVG();
  path = svg.appendChild(document.createElementNS(svgNS, "path"));
  setAttributes(path, [["fill", fill ? "DarkOliveGreen" : "none"], ["stroke-width", "2px"],
                       ["stroke", "DarkOliveGreen"], ["vector-effect", "non-scaling-stroke"]]);
  let z = -Math.PI;
  // add circles with correct radius and in starting position (z=0)
  for (let c = 0; c <= cm; c++) {
    let circle = svg.appendChild(document.createElementNS(svgNS, "circle"));
    let [cx, cy] = pos(c, z);
    setAttributes(circle, [["cx", cx], ["cy", cy]]);
    if (c == cm) {
      // red dot
      setAttributes(circle, [["r", 8*factor], ["stroke", "none"], ["fill", "red"]]);
      // start of path
      path.setAttribute("d", `M${cx},${cy}`);
    } else {
      let r = radius(c);
      setAttributes(circle, [["r", r], ["stroke", "gray"],
                             ["vector-effect", "non-scaling-stroke"], ["fill", "none"]]);
    }
    circles[c] = circle;
  }
  running = true;
  updateInfo();
  window.setTimeout(() => {requestAnimationFrame(() => {animate(cm, z+inc);});}, time);
}

// animation code just adjusts the circle centers
function animate (cm, z) {
  if (!running)
    return;
  let cx, cy;
  for (let c = 0; c <= cm; c++) {
    [cx, cy] = pos(c, z, cx, cy);
    setAttributes(circles[c], [["cx", cx], ["cy", cy]]);
    // we also add to the path if we're in the "first round"
    if (c == cm && z <= Math.PI + inc)
      path.setAttribute("d", path.getAttribute("d") + ` L${cx},${cy}`);
  }
  window.setTimeout(() => {requestAnimationFrame(() => {animate(cm, z+inc);});}, time);
}

// hide "documentation"
function hideInfo () {
  document.getElementById("info").style.display = "none";
}

// keyboard handler
function keyHandler (e) {
  let c = e.keyCode ? e.keyCode : e.charCode;
  switch (c) {
  case 40: // down arrow
    hideInfo();
    noCircles = Math.floor(noCircles / 2);
    if (noCircles < 1)
      noCircles = 1;
    startAnimation(noCircles);
    break;
  case 38: // up arrow
    hideInfo();
    noCircles *= 2;
    if (noCircles > coeffR.length - 1)
      noCircles = coeffR.length - 1;
    startAnimation(noCircles);
    break;
  case 37: // left arrow
    hideInfo();
    noCircles--;
    if (noCircles < 1)
      noCircles = 1;
    startAnimation(noCircles);
    break;
  case 39: // right arrow
    hideInfo();
    noCircles++;
    if (noCircles > coeffR.length - 1)
      noCircles = coeffR.length - 1;
    startAnimation(noCircles);
    break;
  case 70: // 'f'
    hideInfo();
    fill = !fill;
    if (running && path)
      path.setAttribute("fill", fill ? "DarkOliveGreen" : "none");
    break;
  case 49: // '1'
  case 50: // '2'
  case 51: // '3'
  case 52: // '4'
  case 53: // '5'
  case 54: // '6'
    hideInfo();
    newImage(c - 49);
    break;
  case 82: // 'r'
    hideInfo();
    randomImage();
    break;
  case 77: // 'm'
    hideInfo();
    noCircles = coeffR.length - 1;
    startAnimation(noCircles);
    break;
  }
}

window.onload = init;

/// Copyright (c) 2018, Dr. Edmund Weitz. All rights reserved.

// the graph of the input function extends from -xMax to xMax
var xMax = 6;
// the factor (called "L") for the limits of the integral which are -factor*xMax and factor*xMax
var factor = 1;
// maximal value for factor
var maxFactor = 5;
// the graph of the integral function (the "almost Fourier transform") extends from 0 to fWidth
var fWidth = 5.2;
// the current value of t - as in f(t) - for the two moving dots
var pointT = -xMax;
// the current frequency (called "xi")
var frequency = 1;
// helper SVG point used in changeFreq event handler
var pt;
// SVG namespace
var svgNS;
// global variables for SVG elements of the same name
var svg, circlePath, transPath, inputDot, circleDot, integDot, freqLine;
// which part of the "almost Fourier transform" to show: real (0), imaginary (1), absolute value (2), nothing (3)
var transType = 3;
// the functions which can be used as input functions

// next two definitions use the fact that we don't deal with x values smaller than -xMax * maxFactor
function sawtooth (x) {
  return 2.8 * ((x + 1000) % 1) - 1.4;
}
function square (x) {
  return (x + 1000.5) % 2 > 1 ? -1.5 : 1.5;
}
var fns = [
  x => {return 1;},
  x => {return Math.cos(2*x*Math.PI);},
  x => {return 0.9*Math.cos(2*x*Math.PI) + 0.9;},
  x => {return 0.3*Math.cos(8*x*Math.PI) + 0.6*Math.cos(6*x*Math.PI) + 0.75*Math.sin(4*x*Math.PI) + 0.85*Math.cos(2*x*Math.PI);},
  x => {return Math.sin(10*x*Math.PI)/25 +  + Math.sin(6*x*Math.PI)/9 + Math.sin(4*x*Math.PI)/4 + Math.sin(2*x*Math.PI);},
  x => {return 1.7*Math.exp(-0.3*x*x)*Math.cos(2*x*Math.PI);},
  x => {return 1.4*Math.exp(-0.3*x*x)*Math.cos(2*x*Math.PI) + 0.5*Math.exp(-0.3*x*x)*Math.sin(4*x*Math.PI);},
  x => {return 1.4*Math.exp(-x*x);},
  sawtooth,
  x => {return 1.1*Math.exp(-0.3*x*x)*sawtooth(x);},
  square,
  x => {return 1.1*Math.exp(-0.3*x*x)*square(x);}
];
// the pointer into the fns array which selects the current function
var fnPtr = -1;
// the current input Function
var inputFunction;
// an array for the vertical dashed lines denoting the current period
var periodLines = [];
// the number of period lines we'll need for the highest frequency (rest will be clipped for smaller frequencies)
var noLines = Math.ceil(2 * xMax * fWidth);
// geometry data for "Trans" group
var transX = 620;
var transWidth = 960;
var transGap = 0.1;

// "dummy" event handler to disable default event handling
function prevent (event) {
  event.preventDefault();
}

// adjust size of SVG area to size of window, the viewBox (see index.html) will do the rest
function setSize () {
  svg.setAttribute("width", window.innerWidth);
  svg.setAttribute("height", window.innerHeight);
}

// called once after the page has loaded
function init () {
  // initialize global variables
  for (let name of ["svg", "circlePath", "transPath", "inputDot", "circleDot", "integDot", "freqLine"])
    window[name] = document.getElementById(name);
  svgNS = svg.namespaceURI;
  pt = svg.createSVGPoint();
  ["contextmenu", "drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave", "drop"].forEach(function (event) {
    svg.addEventListener(event, prevent);
  });

  // window size
  setSize();
  window.onresize = setSize;

  // prepare graphs, add coordinate lines, etc.
  let ratio = prepareGraph("Input", 20, 20, 1560, 300, -0.3-xMax, xMax+0.3, 2);
  setAttributes(inputDot, [["rx", 0.05], ["ry", 0.05*ratio]]);
  ratio = prepareGraph("Circle", 20, 350, 520, 520, -2, 2, 2);
  setAttributes(circleDot, [["rx", 0.05], ["ry", 0.05*ratio]]);
  setAttributes(integDot, [["rx", 0.05], ["ry", 0.05*ratio]]);
  prepareGraph("Trans", transX, 350, transWidth, 520, -transGap, fWidth+transGap, 20);
  setAttributes(freqLine, [["x1", frequency], ["x2", frequency], ["y1", -15], ["y2", 15]]);

  for (let i = 1; i < noLines; i++) {
    let line = thinLine(document.getElementById("Input"), document.getElementById("inputPath"), i, -1.6, 3.2, false);
    setAttributes(line, [["stroke", "gray"], ["stroke-dasharray", "4 2"]]);
    periodLines[i] = line;
    line = thinLine(document.getElementById("Input"), document.getElementById("inputPath"), -i, -1.6, 3.2, false);
    setAttributes(line, [["stroke", "gray"], ["stroke-dasharray", "4 2"]]);
    periodLines[noLines + i] = line;
  }

  // three event handlers
  document.getElementById("Input").addEventListener("mousedown", (event) => {
    event.preventDefault();
    switchInput(event.button == 0 ? 1 : -1);
    return false;
  });
  document.getElementById("Trans").addEventListener("mousedown", changeFreq);
  document.onkeydown = keyHandler;

  // draw graphs and dots
  fnPtr = -1;
  switchInput();
  updateIntegDot();
  // start animation
  updateDots();
  showInfo();
}

function setTransVisibility (state) {
  document.getElementById("coverTransRect").style.visibility = state;
}

// changes cyclically to another input function where dir is +1 or -1
function switchInput (dir = 1) {
  hideInfo();
  fnPtr = (fnPtr + dir + fns.length) % (fns.length);
  inputFunction = fns[fnPtr];
  updateInput();
  updateComplex();
  computeTransform();
  updateIntegDot();
}

// changes the frequency to f
function setFreq (f = frequency) {
  frequency = f;
  updateInput();
  updateComplex();
  computeTransform();
  updateIntegDot();
  // move frequency line below "almost Fourier transform" graph
  setAttributes(freqLine, [["x1", f], ["x2", f]]);
  // moves period lines below input graph
  for (let i = 1; i < noLines; i++) {
    let p = i/f;
    setAttributes(periodLines[i], [["x1", p], ["x2", p]]);
    setAttributes(periodLines[noLines+i], [["x1", -p], ["x2", -p]]);
  }
  // updates factor ("L") display as this function is also called when
  // the factor changes
  document.getElementById("LText").innerHTML = `${factor}`;
}

// mouse handler which is called for mouse clicks on the "almost
// Fourier transform" graph
function changeFreq (event) {
  hideInfo();
  // compute x position of click in terms of current transform
  pt.x = event.clientX;
  pt.y = event.clientY;

  var cursorpt =  pt.matrixTransform(svg.getScreenCTM().inverse());
  var x = (cursorpt.x - transX) / transWidth * (fWidth + 2 * transGap) - transGap;
  // if x > 0 use the frequency we clicked on
  if (x > 0)
    setFreq(x);
  // otherwise a cyclic change of the display type
  else {
    setTransType((transType + 1) % 4);
    updateIntegDot();
  }
}

// global arrays for the following function
var colors = ["red", "green", "brown", "white"];
var labels = ["Re", "Im", "abs", ""];

// sets the type of graph shown in the "almost Fourier transform" area, see documentation for transType variable above
function setTransType (t) {
  transType = t;
  computeTransform();
  // update label and colors
  let typeText = document.getElementById("typeText");
  typeText.innerHTML = labels[t];
  typeText.style.fill = colors[t];
  transPath.setAttribute("stroke", colors[t]);
}

// function to update the two animated dots which calls itself on a regular basis
function updateDots () {
  // value of input function f(t) at t=pointT
  let val = inputFunction(pointT);
  // update brown dot on input function graph
  setAttributes(inputDot, [["cx", pointT], ["cy", val]]);
  // update violet dot on complex plane graph
  setAttributes(circleDot, [["cx", val*Math.cos(-frequency*2*Math.PI*pointT)], ["cy", val*Math.sin(-frequency*2*Math.PI*pointT)]]);
  // increase pointT cyclically in [-xMax, xMax] interval
  pointT = (pointT + 0.02 + xMax) % (2*xMax) - xMax;
  // do this again after 0.1 seconds
  window.setTimeout(() => {requestAnimationFrame(updateDots);}, 100);
}

// updates position of black dot in complex plane
function updateIntegDot () {
  // we compute the integral...
  let [x, y] = computeIntegral();
  // but then divide by factor*xMax so that we don't show the integral but rather the "centroid"
  setAttributes(integDot, [["cx", x/xMax/factor], ["cy", y/xMax/factor]]);
}

// utility function to set several attributes of an SVG element at once
function setAttributes (el, attrs) {
  for (let [name, val] of attrs) {
    el.setAttribute(name, val);
  }
}

// draws a black arrow tip in SVG area at (x,y) and inserts it before beforeEL
// this is done in viewBox coordinates
// hor is true if this is an array for a horizontal line
function arrowTip (beforeEl, x, y, hor = true) {
  let d = `M${x},${y} `;
  if (hor) {
    d += `L${x-15},${y-5} L${x-15},${y+5}`;
  } else {
    d += `L${x-5},${y+15} L${x+5},${y+15}`;
  }
  let tip = svg.insertBefore(document.createElementNS(svgNS, "path"), beforeEl);
  tip.setAttribute("d", d + " z");
  return tip;
}

// draws a thin black line starting at (x1,y1) with length len
// line will be a child of el and inserted before beforeEl
// hor is true if this is a horizontal line
function thinLine (el, beforeEl, x1, y1, len, hor = true) {
  let line = el.insertBefore(document.createElementNS(svgNS, "line"), beforeEl);
  if (hor)
    setAttributes(line, [["x2", x1+len], ["y2", y1]]);
  else
    setAttributes(line, [["x2", x1], ["y2", y1+len]]);
  setAttributes(line, [["x1", x1], ["y1", y1], ["stroke-width", "1px"], ["stroke", "black"], ["vector-effect", "non-scaling-stroke"]]);
  return line;
}

// utility function to prepare one of the three graphs (the SVG group with ID id)
// sets up transform so that the graph's rectangle is described by xPos, yPos, width, height
// graph extends horizontally from x1 to x2 (in new coordinates) and vertically from -yW to yW
function prepareGraph (id, xPos, yPos, width, height, x1, x2, yW) {
  // clip to rectangle
  setAttributes(
    document.getElementById(`clip${id}Rect`),
    [["x", x1], ["y", -yW], ["width", x2-x1], ["height", 2*yW]]
  );

  let el = document.getElementById(id);
  let xScale = width/(x2-x1);
  let yScale = height/(2*yW);
  // transform
  el.setAttribute("transform", `translate(${xPos-x1*xScale},${yPos+height/2}) scale(${xScale},${-yScale})`);
  // coordinate lines with arrow tips
  arrowTip(el, xPos + width, yPos + height/2);
  arrowTip(el, xPos - x1*xScale, yPos, false);
  thinLine(svg, el, (1-x1) * xScale + xPos, yPos + height/2 - 8, 16, false);
  thinLine(svg, el, -x1 * xScale + xPos - 8, yPos + height/2 - yScale, 16);

  // markers for (1,0) and (0,1)
  thinLine(el, el.childNodes[0], x1, 0, x2-x1);
  thinLine(el, el.childNodes[0], 0, -yW, 2*yW, false);

  // cover with hidden rectangle for mouse clicks as pointer-events="bounding-box" doesn't work yet
  let rect = el.appendChild(document.createElementNS(svgNS, "rect"));
  rect.style.visibility = "hidden";
  rect.style.fill = "white";
  setAttributes(rect, [["id", `cover${id}Rect`], ["x", x1], ["y", -yW], ["width", x2-x1], ["height", 2*yW]]);
  // returns scale aspect ratio
  return xScale / yScale;
}

// updates graph of input function
function updateInput () {
  let f = inputFunction;
  let d = xMax / 1000;
  let x = -xMax;
  let char = "M";
  let path = "";
  while (x <= xMax) {
    path += char;
    path += `${x},${f(x)} `;
    x += d;
    char = "L";
  }
  document.getElementById("inputPath").setAttribute("d", path);
}

// updates path in complex plane
function updateComplex () {
  let f = inputFunction;
  // number of actual points depends on factor
  let dt = xMax / 2000;
  let dz = -frequency*2*Math.PI*dt;
  let t = -factor * xMax;
  let z = -frequency*2*Math.PI*t;
  let char = "M";
  let path = "";
  while (t <= factor * xMax) {
    path += char;
    let val = f(t);
    path += `${val * Math.cos(z)},${val * Math.sin(z)} `;
    t += dt;
    z += dz;
    char = "L";
  }
  circlePath.setAttribute("d", path);
}

// computes a numerical approximation of the integral for xi from -6*"L" to 6*"L"
// returns as two values the real part and the imaginary part
function computeIntegral (xi = frequency) {
  let f = inputFunction;
  let xw = factor * 2 * xMax;
  let t = -factor * xMax;
  let z = -xi*2*Math.PI*t;
  let n = 10000;
  let dt = xw / n;
  let dz = -xi*2*Math.PI*dt;
  let re = 0;
  let im = 0;
  for (let i = 0; i < n; i++) {
    let val = f(t);
    re += val * Math.cos(z);
    im += val * Math.sin(z);
    t += dt;
    z += dz;
  }
  return [re*xw/n, im*xw/n];
}

// draws the "almost Fourier transform"
function computeTransform () {
  let d = fWidth / 1000;
  let [re, im] = computeIntegral(0);
  let path;
  switch (transType) {
  case 0:
    path = `M0,${re} `;
    break;
  case 1:
    path = `M0,${im} `;
    break;
  case 2:
    path = `M0,${Math.sqrt(re*re+im*im)} `;
    break;
  }
  let xi = d;
  // a bit of redundancy to speed things up a little
  switch (transType) {
  case 0:
    while (xi <= fWidth) {
      [re, im] = computeIntegral(xi);
      path += `L${xi},${re} `;
      xi += d;
    }
    break;
  case 1:
    while (xi <= fWidth) {
      [re, im] = computeIntegral(xi);
      path += `L${xi},${im} `;
      xi += d;
    }
    break;
  case 2:
    while (xi <= fWidth) {
      [re, im] = computeIntegral(xi);
      path += `L${xi},${Math.sqrt(re*re+im*im)} `;
      xi += d;
    }
    break;
  }
  if (transType == 3)
    path = "";
  transPath.setAttribute("d", path);
}

// keyboard handler
function keyHandler (e) {
  let c = e.keyCode ? e.keyCode : e.charCode;
  switch (c) {
    // 1 to 5: change to corresponding integer frequency
  case 49: // '1'
  case 50: // '2'
  case 51: // '3'
  case 52: // '4'
  case 53: // '5'
    hideInfo();
    setFreq(c-48);
    break;
    // r: show real part of "almost Fourier transform"
  case 82: // 'r'
    hideInfo();
    setTransType(0);
    break;
    // i: show imaginary part of "almost Fourier transform"
  case 73: // 'i'
    hideInfo();
    setTransType(1);
    break;
    // a or b: show absolute value of "almost Fourier transform"
  case 65: // 'a'
  case 66: // 'b'
    hideInfo();
    setTransType(2);
    break;
    // n: show nothing
  case 78: // 'n'
    hideInfo();
    setTransType(3);
    break;
    // s or h: show/hide "almost Fourier transform"
  case 72: // 'h'
    hideInfo();
    setTransVisibility("visible");
    break;
  case 83: // 's'
    hideInfo();
    setTransVisibility("hidden");
    break;
    // q: reset animated dots to t=0
  case 81: // 'q'
    hideInfo();
    pointT = 0;
    break;
    // 0: reset factor ("L") to 1
  case 48: // '0'
    hideInfo();
    factor = 1;
    setFreq();
    break;
    // up or down: change factor ("L") by +1 or -1
  case 40: // down arrow
    hideInfo();
    if (factor > 1) {
      factor -= 1;
      setFreq();
    }
    break;
  case 38: // up arrow
    hideInfo();
    if (factor < maxFactor) {
      factor += 1;
      setFreq();
    }
    break;
    // left and right arrow: change input function cyclically
  case 37: // left arrow
    hideInfo();
    switchInput(-1);
    break;
  case 39: // right arrow
    hideInfo();
    switchInput();
    break;
  case 27: // ESC
    hideInfo();
    break;
  case 112: // F1
    showInfo();
    break;
  }
}

function hideInfo () {
  document.getElementById("info").style.display = "none";
}

function showInfo () {
  document.getElementById("info").style.display = "block";
}

window.onload = init;

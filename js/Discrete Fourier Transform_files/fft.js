// Copyright (c) 2015, Dr. Edmund Weitz.  All rights reserved.

// Simple and unoptimized Fast Fourier Transform (FFT) in Javascript

// Fallback for browser like MSIE which don't have "log2" yet.
Math.log2 = Math.log2 || function(x) {
  return Math.log(x) * Math.LOG2E;
};

// Global array for the reshuffle part below.  If this is not null,
// the caller has to supply an array which is large enough.
var fourierCoeffsTemp = null;

// Computes a 2^k-th primitive root of unity of 1 and returns it as a
// two-element array containing the real and the imaginary part.  If
// "invert" is false, the first such root (counterclockwise) will be
// returned, otherwise the last.
function omega (k, invert) {
  var angle = 2 * Math.PI / (1 << k);
  if (invert)
    angle = - angle;
  return [Math.cos(angle), Math.sin(angle)];
}

// Computes the product of the complex matrix M with the complex
// vector v.  M is a Vandermonde matrix with 2^k rows and columns
// where the entry in row i and column j is omega^(i*j) with rows and
// columns numbered beginning at zero.  omega must be a 2^k-th
// primitive root of unity with "om_r" and "om_i" being its real and
// imaginary parts.  "real" and "imag" are arrays for the real and
// imaginary parts of the vector v.  These arrays will be
// destructively modified because they'll return the result.  If
// "from" is specified it must be an index into these arrays.  In this
// case, the vectors are understood to start at this position within
// the array instead of at index 0.
function DFT (k, om_r, om_i, real, imag, from) {
  // If k is zero we have a 1x1 matrix and omega must be 1, so there's
  // nothing to do.
  if (k == 0)
    return;

  // Set pow to 2^k.
  var pow = 1 << k;
  // Make sure from is a number.
  from = from || 0;

  var i = from, j = i + pow/2, l,
      fac_r = 1, fac_i = 0,
      temp_r, temp_i;

  // In the upper half of v each entry v_i is set to v_i + v_j where j
  // is the corresponding entry from the lower half, i.e. j = i +
  // pow/2.  In the lower half each v_j is at the same time set to the
  // value omega^l * (v_i - v_j) where l starts at 0 and is increased
  // in each step.
  var to = from + pow/2;
  while (i < to) {
    // Because we modify v_i first, we need to remember its value.
    temp_r = real[i];
    temp_i = imag[i];

    // Set new value of v_i.
    real[i] += real[j];
    imag[i] += imag[j];

    // Compute difference v_i - v_j.
    temp_r -= real[j];
    temp_i -= imag[j];

    // v_j = fac * temp (complex multiplication)
    real[j] = temp_r * fac_r - temp_i * fac_i;
    imag[j] = temp_r * fac_i + temp_i * fac_r;

    // fac is initially 1 and is then multiplied by omega in each
    // step.
    temp_r = fac_r * om_r - fac_i * om_i;
    fac_i = fac_r * om_i + fac_i * om_r;
    fac_r = temp_r;

    // Iteration.
    i++;
    j++;
  }

  // Divide-and-conquer part: Decrease k, compute next root of unity
  // by squaring omega, then apply smaller version of DFT to parts.
  if (k > 1) {
    k -= 1;

    // Square omega to get "next" root of unity.
    temp_r = om_r * om_r - om_i * om_i;
    om_i = 2 * om_i * om_r;
    om_r = temp_r;

    // Compute upper half of result vector.
    DFT(k, om_r, om_i, real, imag, from);
    // Compute lower half of result vector.
    DFT(k, om_r, om_i, real, imag, from + pow/2);
  }

  // The entries in v are in the wrong order now and we have to
  // shuffle them around.  We're doing this in place and thus have to
  // shuffle quite a lot.  For larger k it might be worthwhile to use
  // an intermediate array instead.

  if (fourierCoeffsTemp) {
    i = from;
    j = i;
    to = from + pow/2;
    while (i < to) {
      fourierCoeffsTemp[j] = real[i++];
      j += 2;
    }
    to = from + pow;
    j = from + 1;
    while (i < to) {
      fourierCoeffsTemp[j] = real[i++];
      j += 2;
    }
    for (i = from; i < to; i++)
      real[i] = fourierCoeffsTemp[i];

    i = from;
    j = i;
    to = from + pow/2;
    while (i < to) {
      fourierCoeffsTemp[j] = imag[i++];
      j += 2;
    }
    to = from + pow;
    j = from + 1;
    while (i < to) {
      fourierCoeffsTemp[j] = imag[i++];
      j += 2;
    }
    for (i = from; i < to; i++)
      imag[i] = fourierCoeffsTemp[i];
  } else {
    // i starts at the first component in the lower half and will then
    // move upwards by one in each step.  (By "lower", "up", and so on
    // we mean the directions we experience if we write the vectors
    // vertically, so e.g. "down" means towards higher coefficients.)
    i = from + pow/2;
    // j is the place where the i-th component must move to.  j starts
    // at the second component in the upper half and is then moved
    // upwards by two in each step.
    j = from + 1;
    to = from + pow - 1;
    while (i < to) {
      // Remember i-th component.
      temp_r = real[i];
      temp_i = imag[i];

      // Now move all components between i and j down by one.  (Note
      // that this keeps the order intact.)
      for (l = i; l > j; l--) {
        real[l] = real[l-1];
        imag[l] = imag[l-1];
      }

      // Finally move i-th component to j-th place.
      real[j] = temp_r;
      imag[j] = temp_i;

      // Iteration.
      i++;
      j += 2;
    }
  }
}
// Computes the coefficients for the trigonometric polynomial
// interpolating the input values in "real" and "imag" (arrays for the
// real and imaginary parts).  Both arrays must have the same size and
// the size must be a power of two.  The arrays will be destroyed and
// used for the return values.  If the arrays have 2*n elements, then
// the returned coefficients are c_{-n+1} .. c_n where c_j is the
// coefficient for the function e^{j * I * z}.  If "imag" is
// undefined (and thus if all inputs are real) the return values will
// instead be two arrays with real coefficients for the cosine and
// sine functions (and a third array with the squares of the absolute
// values of the complex coefficients in the original order).  It is
// assumed that the input values belong to arguments from -pi to pi.
// If an absolute value is smaller than 1e-13 it will be set to zero
// unless "noCleanUp" is true.
function computeDFTcoeffs (real, imag, noCleanUp) {
  var length = real.length,
      // Flag for the presence of "imag".
      complexData = false;
  if (imag)
    complexData = true;
  else
    imag = new Array(length).fill(0);
  if (imag.length != length)
    throw "Arrays must have the same length.";
  var k = Math.log2(length);
  if (1 << k != length)
    throw "Length of Array must be power of two.";
  var om = omega(k, true);
  var om_r = om[0], om_i = om[1];

  var fac_r = 1, fac_i = 0;

  // Prepare input by multiplying with consecutive powers of omega.
  var i = 0;
  while (i < length) {
    var temp_r = real[i] * fac_r - imag[i] * fac_i;
    imag[i] = real[i] * fac_i + imag[i] * fac_r;
    real[i] = temp_r;

    // Note signs; we're actually multiplying with omega^{-1}.
    temp_r = fac_i * om_i - fac_r * om_r;
    fac_i = - fac_r * om_i - fac_i * om_r;
    fac_r = temp_r;

    i++;
  }

  // Now the actual Fourier transformation, see above.
  DFT(k, om_r, om_i, real, imag);

  // Finish by fixing signs of results and multiplying with the right
  // factor.
  var sign = k > 1 ? -1 : 1;
  var times = sign * (1 / length);

  for (i = 0; i < length; i++) {
    real[i] *= times;
    imag[i] *= times;
    times *= sign;
  }

  if (!noCleanUp)
    for (i = 0; i < length; i++) {
      if (Math.abs(real[i]) < 1e-13)
        real[i] = 0;
      if (Math.abs(imag[i]) < 1e-13)
        imag[i] = 0;
    }

  // Return result unless we want reals.
  if (complexData)
    return [real, imag];

  // Now combine two results each.  "s" is for sine and "c" for cosine
  i = length / 2 - 1;
  var j = i, c = new Array(i+2), s = new Array(i+2);
  k = 0;
  while (j < length) {
    if (i == j) {
      // The middle coefficient c_0; no sine here.
      c[k] = real[i];
      s[k] = 0;
    } else if (i < 0) {
      // The rightmost value; again no sine.
      c[k] = real[j];
      s[k] = 0;
    } else {
      // Otherwise the values will be complex conjugates.
      c[k] = real[i] + real[j];
      s[k] = imag[i] - imag[j];
    } 
    i--;
    j++;
    k++;
  }

  // Finally the squares of the absolute values returned in "real"
  for (i = 0; i < length; i++)
    real[i] = real[i] * real[i] + imag[i] * imag[i];
  
  return [c, s, real];
}

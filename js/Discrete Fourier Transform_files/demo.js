// Copyright (c) 2015-2017, Dr. Edmund Weitz.  All rights reserved.

var twoPi = 2 * Math.PI;

// size of the two upper SVG areas
var WIDTH = 1000, HEIGHT = 300;
// the relevant size (without margins); see "init" function
var width, height;
// size of the lower SVG area; width MUST be power of 2
var spectrumWidth = 512;
var spectrumHeight = 220;

// global data array for spectrum display
var spectrumData = new Array(spectrumWidth);

var step;
var functionData;

// the relevant areas of the three SVG elements (NOT the elements
// themselves!)
var inputSVG, windowSVG, spectrumSVG;
// D3 scale objects to map between SVG and real coordinates
var inputXScale, inputYScale;

// inner margin in SVG areas where we don't draw
var margin = { top: 5, right: 5, bottom: 5, left: 5 };

// whether to show the trigonometric polynomial
var showInterpolation = false;

// whether to show ticks on axes
var showTicks = false;

// binary logarithm of number of samples
var sampleBase = 6;
// compute number of samples
var samples = 1 << sampleBase;
var input = new Array(samples);

// frequency of input signal
var frequency = 2;

// start and end of input window in real coordinates
var windowStart = -Math.PI;
var windowEnd = Math.PI;

// global functions; see also "data.js"
var inputFunction, windowFunction;

// array of samples; samples are objects of the form {x: x, y: y}
var data = new Array(samples);
// array of samples multiplied with window function; array elements
// are objects of the form {x: x, y: y}
var filteredData;

// minimal value to show in spectrum
var spectrumMin = 1e-5;

// sets WIDTH and HEIGHT according to the URL parameter "size"
function getParams () {
  // hacky, but good enough in this case
  if (document.URL.match(/size=small/)) {
    WIDTH = 800;
    HEIGHT = 200;
    spectrumWidth = 256;
    spectrumHeight = 200;
  }
  d3.select("#bigTable").style("max-width", WIDTH + "px");
  d3.selectAll(".info").style("max-width", WIDTH + "px");
}

// draws a black rectangle in the corresponding SVG area; the default
// for opacity is 50%; you can provide a class name and/or an ID via
// "params"
function drawRect(svg, x, y, width, height, params) {
  params = params || {};
  params.opacity = params.opacity || 0.5;
  var rect = svg.append("rect")
    .attr({x: x, y: y, width: width, height: height, "fill-opacity": params.opacity});
  if (params.className)
    rect.attr("class", params.className);
  if (params.id)
    rect.attr("id", params.id);
}

// while we're resizing, this is an object with a flag "start" to
// denote whether we're moving the left or right border
var moving = false;
var lastMove = { left: true };

// this is called for "mousedown" events
function mouseDown () {
  // only act on upper SVG
  if (this == inputSVG.node().parentElement) {
    // coordinates of mouse relative to margins
    var x = d3.mouse(this)[0] - margin.left;
    var y = d3.mouse(this)[1] - margin.top;
    // only act if click was inside margins
    if (x >= 0 && x < width && y >= 0 && y < height) {
      // convert to real coordinates
      x = inputXScale.invert(x);
      // if near border, start movement
      if (Math.abs(x - windowStart) < .2) {
        moving = { left: true };
      } else if (Math.abs(x - windowEnd) < .2) {
        moving = { right: true };
      } else if (Math.abs(x - (windowStart + windowEnd) / 2) < .2) {
        moving = { center: true };
      }
    }
  }
  d3.event.stopPropagation();
}

// this is called for "mousemove" events
function mouseMove () {
  // only act on upper SVG
  if (this == inputSVG.node().parentElement) {
    // coordinates of mouse relative to margins
    var x = d3.mouse(this)[0] - margin.left;
    var y = d3.mouse(this)[1] - margin.top;
    // flag to remember whether we made a change
    var change = false;
    // only act if mouse is inside margins
    if (x >= 0 && x < width && y >= 0 && y < height) {
      // convert to real coordinates
      x = inputXScale.invert(x);
      // if near border, change cursor
      if (Math.abs(x - windowStart) < .2 || Math.abs(x - windowEnd) < .2 || Math.abs(x - (windowStart + windowEnd) / 2) < .2) {
        d3.select(this).style("cursor", "ew-resize");
      } else {
        d3.select(this).style("cursor", "default");
      }
      // if moving (i.e. if mouse was pressed near border)...
      if (!moving)
        return;
      // ...move the corresponding border; but keep minimal distance
      // of pi/4
      if (moving.left && x + Math.PI / 4 < windowEnd) {
        windowStart = x;
        lastMove = { left: true };
        change = true;
      } else if (moving.right && x - Math.PI / 4 > windowStart) {
        windowEnd = x;
        lastMove = { right: true };
        change = true;
      } else if (moving.center) {
        var halfWidth = (windowEnd - windowStart) / 2;
        if (x + halfWidth < twoPi && x - halfWidth > -twoPi) {
          windowStart = x - halfWidth;
          windowEnd = x + halfWidth;
          lastMove = { center: true };
          change = true;
        }
      }
      if (change) {
        drawWindowed();
        update();
      }
    }
  }
  d3.event.stopPropagation();
}

// clear selection, just in case
function clearSelection() {
  if (document.selection) {
    document.selection.empty();
  } else if (window.getSelection) {
    window.getSelection().removeAllRanges();
  }
}

// this is called for "mouseup" events
function mouseUp () {
  // cancel "moving" state
  moving = false;

  clearSelection();
}

// redraw everything
function update() {
  drawSamples();
  drawWindowFunction();
  drawResult();
  clearSelection();
}

// this function is called once at the beginning
function init () {
  // provide buffer for fft.js
  fourierCoeffsTemp = new Array(32768);
  
  // maybe adjust size of SVG areas
  getParams();

  // set relevant size (i.e. minus margins)
  width = WIDTH - margin.left - margin.right;
  height = HEIGHT - margin.top - margin.bottom;

  step = 4 * Math.PI / width;
  functionData = new Array(width);
  var x = -twoPi;
  for (var i = 0; i < width; i++, x += step)
    functionData[i] = {x: x};

  // creates an SVG area inside an element with ID "id" and with
  // dimensions "w" and "h" (defaulting to "width" and "height"); adds
  // mouse handlers and draws a dark gray border for the margin;
  // returns a group which is the relevant area inside the margins
  // with a corresponding transform
  function prepareSVG (id, w, h) {
    w = w || width;
    h = h || height;
    var svg = d3.select(id).append("svg")
        .attr("width", w + margin.left + margin.right)
        .attr("height", h + margin.top + margin.bottom);
    svg.on("mousedown", mouseDown)
      .on("mousemove", mouseMove)
      .on("mouseup", mouseUp);
    drawRect(svg, 0, 0, w + margin.left + margin.right, margin.top);
    drawRect(svg, 0, h + margin.top, w + margin.left + margin.right, margin.bottom);
    drawRect(svg, 0, margin.top, margin.left, h);
    drawRect(svg, w + margin.left, margin.top, margin.left, h);
    return svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  }

  // create upper two SVG areas
  inputSVG = prepareSVG("#input");
  windowSVG = prepareSVG("#window");

  // create D3 scale objects for the relevant area
  inputXScale = d3.scale.linear().domain([-2*Math.PI,2*Math.PI]).range([0,width]);
  inputYScale = d3.scale.linear().domain([-1.05,1.05]).range([height,0]);

  // create lower SVG (different size)
  spectrumSVG = prepareSVG("#spectrum", spectrumWidth, spectrumHeight);

  d3.select("body").on("keydown", function() {
    var change = false;
    if (!d3.event.ctrlKey)
      return;
    if (d3.event.keyCode == 37) {
      if ((lastMove.left || lastMove.center) && windowStart - step >= -twoPi) {
        windowStart -= step;
        if (lastMove.center)
          windowEnd -= step;
        change = true;
      } else if (lastMove.right && windowEnd - step >= windowStart + Math.PI/4) {
        windowEnd -= step;
        change = true;
      }
    } else if (d3.event.keyCode == 39) {
      if ((lastMove.right || lastMove.center) && windowEnd + step <= twoPi) {
        windowEnd += step;
        if (lastMove.center)
          windowStart += step;
        change = true;
      } else if (lastMove.left && windowStart + step <= windowEnd - Math.PI/4) {
        windowStart += step;
        change = true;
      }
    }
    if (change) {
      drawWindowed();
      update();
    }
  });
  
  // add tooltip to lower SVG
  d3.select("#spectrum")
    .attr("title",
          "Fourier series coefficients (at most " +
          (spectrumWidth/2) + " frequencies)");

  // if this checkbox is changed, redraw results because this changes
  // whether the polynomial is drawn
  d3.select("#showCheckbox").on("change", function () {
    showInterpolation = this.checked;
    drawResult();
  }).node().checked = false;

  // if this checkbox is changed, show/hide ticks on axes
  d3.select("#ticksCheckbox").on("change", function () {
    showTicks = this.checked;
    updateAllTicks();
  }).node().checked = false;

  // if this value is changed, redraw everything
  d3.select("#selectSamples").on("change", function () {
    sampleBase = +this.value;
    samples = 1 << Math.min(sampleBase, 9);
    input = new Array(1 << sampleBase);
    data = new Array(samples);
    if (sampleBase > 9) {
      d3.select("#showCheckbox").attr("disabled", "disabled").node().checked = false;
      showInterpolation = false;
    } else
      d3.select("#showCheckbox").attr("disabled", null);
    update();
  });

  // if this value is changed, redraw everything
  d3.select("#selectFrequency").on("change", function () {
    frequency = +this.value;
    drawInput();
    drawWindowed();
    update();
  });

  // if this value is changed, redraw the spectrum
  d3.select("#selectDB").on("change", function () {
    spectrumMin = +this.value;
    drawResult();
  });

  // if this button is clicked, set borders to initial values, then
  // redraw everything
  d3.select("#resetButton").on("click", function () {
    windowStart = -Math.PI;
    windowEnd = Math.PI;
    drawInput();
    drawWindowed();
    update();
  });

  // replace "codeFunction", see file data.js
  d3.select("#code").on("change", function () {
    generateCode();
    drawInput();
    drawWindowed();
    update();
  });

  // add a button to change screen size
  d3.select("#lastDIV").append("button")
    .attr("type", "button")
    .attr("title", WIDTH < 1000 ? "Enlarge graphics" : "Shrink graphics to accomodate smaller screens")
    .html(WIDTH < 1000 ? "Larger Screen" : "Smaller Screen")
    .on("click", function () {
      window.location.href = WIDTH < 1000 ? window.location.pathname
        : window.location.href + "?size=small";
    });

  // populate selection for change of input function
  d3.select("#selectInput")
    .on("change", function () {
      // if there's a generator (for the random functions), we have to
      // call it once
      var gen = inputFunctions[this.selectedIndex].gen;
      if (gen)
        gen();
      inputFunction = inputFunctions[this.selectedIndex].fn;
      drawInput();
      drawWindowed();
      update();
    })
    .selectAll("option")
    .data(inputFunctions)
    .enter()
    .append("option")
    .text(function (d) { return d.name; });
  // initial value
  inputFunction = inputFunctions[0].fn;
  
  // populate selection for change of window function
  d3.select("#selectWindow")
    .on("change", function () {
      windowFunction = windowFunctions[this.selectedIndex].fn;
      drawWindowed();
      update();
    })
    .selectAll("option")
    .data(windowFunctions)
    .enter()
    .append("option")
    .text(function (d) { return d.name; });
  // initial value
  windowFunction = windowFunctions[0].fn;

  // initial value
  windowFunction = windowFunctions[0].fn;

  // draw axes
  addAxes(inputSVG);
  // and ticks
//  updateAllTicks();

  inputSVG.append("path").attr("id", "inputFunction")
    .attr("stroke", "red").attr("fill", "none")
    .attr("stroke-width", 1.5)
    .attr("stroke-opacity", 1);

  // draw axes
  addAxes(windowSVG);

  windowSVG.append("path").attr("id", "windowedFunction")
    .attr("stroke", "red").attr("fill", "none")
    .attr("stroke-width", 1.5)
    .attr("stroke-opacity", 1);

  windowSVG.append("mask").attr({
    id: "windowMask",
    x: 0, y:0, width: width, height: height
    })
    .append("rect").attr({
      fill: "white",
      width: width, height: height
    });
  d3.select("#windowMask").append("path").attr("id", "windowPath");

  ['left', 'right', 'upperMiddle', 'lowerMiddle'].forEach( function (id) {
    drawRect(inputSVG, 0, 0, 0, height, { id: id, className: "windowFrame", opacity: 0.3});
  });
  d3.select("#upperMiddle").attr({
    height: inputYScale(1)
  });
  d3.select("#lowerMiddle").attr({
    y: inputYScale(-1),
    height: inputYScale(1)
  });
  
  spectrumSVG.append("g").attr("id", "spectralData").selectAll("rect").data(spectrumData).enter()
    .append("rect")
    .attr("fill", "green");

  // now draw everything
  drawInput();
  drawWindowed();
  update();
}

// draws points on SVG area "svg" according to data in array "points"
// using ID "id"
function drawPoints (svg, id, points) {
  d3.selectAll("#" + id).remove();
  if (sampleBase > 6)
    return;
  svg.append("g").attr("id", id).selectAll("circle")
    .data(points).enter().append("circle")
    .attr("r", 3)
    .attr("cx", function (d) { return inputXScale(d.x); })
    .attr("cy", function (d) { return inputYScale(d.y); });
}

// a D3 line which accepts values like {x: x, y: y} in real
// coordinates
var line = d3.svg.line()
  .x(function (d) { return inputXScale(d.x); })
  .y(function (d) { return inputYScale(d.y); })
  .interpolate("linear");

function drawInput () {
  for (var i = 0; i < width; i++) {
    var obj = functionData[i];
    obj.y = inputFunction(obj.x);
  }
  d3.select("#inputFunction").attr("d", line(functionData));
}

function drawWindowed () {
  for (var i = 0; i < width; i++) {
    var obj = functionData[i];
    var x = obj.x;
    obj.y = windowFunction(x, windowStart, windowEnd) * inputFunction(x);
  }
  d3.select("#windowedFunction").attr("d", line(functionData));
}

// draws samples in upper SVG (and axes and the borders around the
// active window); this function also computes the global array "data"
// which is used by other functions
function drawSamples () {
  // take that many samples between the window borders
  var step = (windowEnd - windowStart) / samples;
  var x = windowStart;
  for (i = 0; i < samples; i++) {
    var y = inputFunction(x);
    var d = data[i];
    if (d) {
      d.x = x;
      d.y = y;
    }
    else
      data[i] = {x: x, y: y};
    x += step;
  }

  drawPoints(inputSVG, "dataPoints", data);
  
  // fill area outside of window with gray to emphasize where the
  // "active" samples are
  d3.select("#left").attr({
    width: inputXScale(windowStart)
  });
  d3.select("#right").attr({
    x: inputXScale(windowEnd),
    width: width - inputXScale(windowEnd)
  });
  d3.select("#upperMiddle").attr({
    x: inputXScale(windowStart),
    width: inputXScale(windowEnd) - inputXScale(windowStart)
  });
  d3.select("#lowerMiddle").attr({
    x: inputXScale(windowStart),
    width: inputXScale(windowEnd) - inputXScale(windowStart)
  });
}

// draws samples multiplied by window function in second SVG (and axes
// and the window for the window function)
function drawWindowFunction () {
  // we create two arrays, "lineData" for the window function from
  // windowStart to windowEnd and at the same time "lineDataRev" with
  // the same values, but negative
  var lineData = new Array(), lineDataRev = new Array();
  var x = windowStart;
  while (x <= windowEnd) {
    var y = windowFunction(x, windowStart, windowEnd);
    lineData.push({x: x, y: y});
    lineDataRev.push({x: x, y: -y});
    x += step;
  }
  // now we concatenate both arrays with the second one reversed, so
  // we circle around the area once
  lineData = lineData.concat(lineDataRev.reverse());

  // from the data above create a clipping path
  d3.select("#windowPath").attr("d", line(lineData));

  d3.selectAll("#blackRect").remove();
  windowSVG.append("rect").attr("id", "blackRect")
    .attr({x: 0, y: 0, width: width, height: height, fill: "black", "fill-opacity": .3, "mask": "url(#windowMask)" });

  // convert "data" to "filteredData" by multiplying with window function
  filteredData = data.map(function (d) {
    return {
      x : d.x,
      y : d.y * windowFunction(d.x, windowStart, windowEnd)
    };
  });

  // now draw the points in the second SVG area
  drawPoints(windowSVG, "windowPoints", filteredData);
}

// adds an x and a y axis to the SVG are "svg"
function addAxes (svg) {
  svg.selectAll(".xaxis").remove();
  svg.selectAll(".yaxis").remove();
  var xAxis = d3.svg.axis().scale(inputXScale).orient("bottom")
      .tickValues(showTicks ? [-6,-5,-4,-3,-2,-1,1,2,3,4,5,6] : 0);
  if (!showTicks)
      xAxis.outerTickSize(0);
  svg.append("g").attr("class", "xaxis")
    .attr("transform", "translate(0," + height/2 + ")")
    .call(xAxis);
  var yAxis = d3.svg.axis().scale(inputYScale).orient("left")
      .tickValues(showTicks ? [0.5,-0.5] : 0);
  if (!showTicks)
      yAxis.outerTickSize(0);
  svg.append("g").attr("class", "yaxis")
    .attr("transform", "translate(" + width/2 + ",0)")
    .call(yAxis);
}

function updateAllTicks () {
  updateTicks(inputSVG);
  updateTicks(windowSVG);
}

function updateTicks (svg) {
  var xAxis = d3.svg.axis().scale(inputXScale).orient("bottom")
      .tickValues(showTicks ? [-6,-5,-4,-3,-2,-1,1,2,3,4,5,6] : 0);
  if (!showTicks)
      xAxis.outerTickSize(0);
  svg.selectAll(".xaxis").attr("transform", "translate(0," + height/2 + ")")
    .call(xAxis);
  var yAxis = d3.svg.axis().scale(inputYScale).orient("left")
      .tickValues(showTicks ? [0.5,-0.5] : 0);
  if (!showTicks)
      yAxis.outerTickSize(0);
  svg.selectAll(".yaxis").attr("transform", "translate(" + width/2 + ",0)")
    .call(yAxis);
}

var cosines, sines, trigArrayLength;

// the trigonometric polynomial based on the coefficients in the
// arrays "cosines" and "sines"
function trigPoly (x) {
  // transform x so that the interval [windowStart, windowEnd] is
  // mapped to [-pi, pi]
  x = 2 * Math.PI * (x - windowStart) / (windowEnd - windowStart) - Math.PI;
  var i, sum = 0;
  for (i = 0; i < trigArrayLength; i++)
    sum += cosines[i] * Math.cos(i * x) + sines[i] * Math.sin(i * x);
  return sum;
}

// draws the spectrum (lower SVG area) and maybe also the
// trigonometric polynomial
function drawResult () {
  input.fill(0);
  var len = filteredData.length;
  for (var i = 0; i < len; i++)
    input[i] = filteredData[i].y;

  // use this to compute the Fourier coefficients
  var result = computeDFTcoeffs(input);

  // the third return value is the spectrum
  var ampl = result[2];
  var amplLength = ampl.length;

  // remove outer quarters if there's more information than would fit
  // into the SVG area
  while (amplLength > spectrumWidth) {
    ampl = ampl.slice(amplLength / 4, 3 * amplLength / 4);
    amplLength = ampl.length;
  }

  // width of one bar in the bar chart
  var barWidth = spectrumWidth / amplLength;
  // maximal value in spectrum
  var amplMax = 1 * d3.max(ampl);
  // minimum is fraction of maximum, use logarithmic scale (base 10)
  var spectrumYScale = d3.scale.log()
    .domain([spectrumMin * amplMax, amplMax])
    .range([spectrumHeight,0]).clamp(true);

  for (i = 0; i < amplLength; i++)
    spectrumData[i] = ampl[i];
  for (; i < spectrumWidth; i++)
    spectrumData[i] = undefined;
  
  d3.select("#spectralData").selectAll("rect")
    .attr("x", function(d, i) {
      return spectrumData[i] === undefined ? spectrumWidth : i * barWidth;
    })
    .attr("y", function(d, i) {
      d = spectrumData[i]; return d === undefined ? spectrumHeight : spectrumYScale(d);
    })
    .attr("height", function(d, i) {
      d = spectrumData[i]; return d === undefined ? 0 : spectrumHeight - spectrumYScale(d);
    })
    .attr("width", barWidth);

  d3.selectAll(".interpol").remove();
  // only draw if requested
  if (showInterpolation) {
    // for the trigonometric polynomial, see "trigPoly" above
    cosines = result[0];
    sines = result[1];
    trigArrayLength = cosines.length;

    // collect enough point data
    var lineData = new Array();
    for (var x = -2 * Math.PI; x <= twoPi; x += step)
      lineData.push({x: x, y: trigPoly(x)});
    var dPath = line(lineData);

    // local function to draw polynomial
    function drawPolynomial (svg) {
      // do it as one line (see global variable "line" above)
      svg.append("path").attr("class", "interpol").attr("d", dPath)
        .attr("stroke", "green").attr("fill", "none")
      // draw thicker and more transparent if there aren't many samples
        .attr("stroke-width", sampleBase > 7 ? 1 : 8 - sampleBase)
        .attr("stroke-opacity", sampleBase > 6 ? .5 : .3);
    }

    // do it twice
    drawPolynomial(inputSVG);
    drawPolynomial(windowSVG);
  }
}

// call "init" once page loading has finished
window.onload = init;

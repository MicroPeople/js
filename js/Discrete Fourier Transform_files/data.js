// Copyright (c) 2015, Dr. Edmund Weitz.  All rights reserved.

// used by function "oscillationFunction" below to store random values
var randomSines = [], randomCosines = [];

// attenuation factor for function "oscillationFunction" below
var oscillationAttenuation;

// computes an oscillation composed of several sine and cosine waves;
// coefficients come from arrays above
function oscillationFunction (x) {
  var sum = 0;
  for (var i = 0; i < randomCosines.length; i++) {
    sum += randomCosines[i] * Math.cos(frequency * i * x);
    sum += randomSines[i] * Math.sin(frequency * i * x);
  }
  // make sure values are between -1 and 1
  return oscillationAttenuation * sum;
}

// placeholder function, to be changed by "generateCode" below
function codeFunction (x) {
  return Math.sin(frequency * x);
}

// tries to change "codeFunction" using input from user
function generateCode () {
  var code = d3.select("#code");
  try {
    eval("codeFunction = function (x) { return " + code.property("value") + "; } ");
  } catch (e) {
    code.property("value", e.message);
  }
}

// for "Input Function" selection; each function has a name and a "fn"
// property for the function itself; functions are supposed to behave
// as if periodic from -pi to pi (but may oscillate faster depending
// on value of "frequency"); if there's a "gen" property, then this
// function has to be called once when the function is selected
var inputFunctions = [
  {
    name: "Sine",
    fn: function (x) {
      return Math.sin(frequency * x);
    }
  },
  {
    name: "Sawtooth",
    fn: function (x) {
      x += Math.PI;
      x *= 0.5 / Math.PI;
      x *= frequency;
      return 2 * (x - Math.floor(x)) - 1;
    }
  },
  {
    name: "Square Wave",
    fn: function (x) {
      x += Math.PI;
      x *= 0.5 / Math.PI;
      x *= frequency;
      return x - Math.floor(x) > .5 ? .9 : -.9;
    }
  },
  {
    name: "Triangle",
    fn: function (x) {
      x += Math.PI;
      x *= 0.5 / Math.PI;
      x *= frequency;
      return x - Math.floor(x) > .5 ? -4 * (x - Math.floor(x)) + 3 : 4 * (x - Math.floor(x)) - 1;
    }
  },
  {
    name: "Capped Tangent",
    fn: function (x) {
      return Math.max(Math.min(Math.tan(frequency * x), 1), -1);
    }
  },
  {
    name: "Constant",
    fn: function (x) {
      return .8;
    }
  },
  {
    name: "Code",
    gen: generateCode,
    fn: function (x) {
      return codeFunction(frequency * x);
    }
  },
  {
    name: "Random Oscillation",
    gen: function () {
      for (var i = 0; i < 10; i++) {
        randomCosines[i] = 0.5 * Math.random();
        randomSines[i] = 0.5 * Math.random();
      };
      // reset attenuation so we can compute the maximum
      oscillationAttenuation = 1;
      // keep old value
      var freq = frequency;
      frequency = 1;
      var max = 0;
      for (var x = -Math.PI; x < Math.PI; x += .02) {
        max = Math.max(max, Math.abs(oscillationFunction(x)));
      };
      // set new attenuation
      oscillationAttenuation = 1 / (1.05 * max);
      // restore old value
      frequency = freq;
    },
    fn: oscillationFunction
  }
];

// for "Window Function" selection; each function has a name and a
// "fn" property for the function itself; functions are supposed to
// return 0 for values outside of [from, to] and otherwise values
// between 0 and 1
var windowFunctions = [
  {
    name: "Dirichlet",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      return 1;
    }
  },
  {
    name: "Triangular",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      var middle = .5 * (from + to);
      var half = .5 * (to - from);
      return 1 - Math.abs((x - middle) / half);
    }
  },
  {
    name: "Welch",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      return - 4 * (x - from) * (x - to) / ((from - to) * (from - to));
    }
  },
  {
    name: "Hann",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      return 0.5 * (1 - Math.cos(2 * Math.PI * (x - from) / (to - from)));
    }
  },
  {
    name: "Hamming",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      return 0.54 - 0.46 * Math.cos(2 * Math.PI * (x - from) / (to - from));
    }
  },
  {
    name: "Sine",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      return Math.sin(Math.PI * (x - from) / (to - from));
    }
  },
  {
    name: "Lanczos",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      x = Math.PI * (2 * (x - from) / (to - from) - 1);
      return Math.abs(x) < 1e-8 ? 1 : Math.sin(x) / x;
    }
  },
  {
    name: "Tukey",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      x = 2 * (x - from) / (to - from) - 1;
      if (Math.abs(x) <= 0.5)
        return 1;
      return 0.5 * (1 + Math.cos(2 * Math.PI * (Math.abs(x) - .5)));
    }
  },
  {
    name: "Blackman-Harris",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      x = Math.PI * (x - from) / (to - from);
      return y = 0.35875 - 0.48829 * Math.cos(2 * x) + 0.14128 * Math.cos(4 * x) - 0.01168 * Math.cos(6 * x);
    }
  },
  {
    name: "Flat Top",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      x = Math.PI * (x - from) / (to - from);
      var y = 1 - 1.93 * Math.cos(2 * x) + 1.29 * Math.cos(4 * x) - 0.388 * Math.cos(6 * x) + 0.028 * Math.cos(8 * x);
      return y / 4.636;
    }
  },
  {
    name: "Gauss",
    fn: function (x, from, to) {
      if (x < from || x >= to)
        return 0;
      x = 2.4477 * (2 * (x -from) / (to - from) - 1);
      return Math.exp(-0.5 * x * x);
    }
  }
];

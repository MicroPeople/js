// Copyright (c) 2016, Dr. Edmund Weitz

// Inspired by
// http://www.physicsandbox.com/projects/double-pendulum.html

var svg1, svg2, pend1, pend2, active = null, offX, offY, running = false, tracing = false;

var g = 9.8;
var time = 0.05;

function init () {
  createElements();
  document.onkeyup = keyHandler;
}

function mouseDown (p, c) {
  return function () {
    if (running)
      return;
    var circle, coord = d3.mouse(this);
    if (c == 1)
      circle = p.circle1;
    else
      circle = p.circle2;
    offX = coord[0] - circle.attr("cx");
    offY = coord[1] - circle.attr("cy");
    active = [p, c];
    removeDots();
  };
}

function mouseMove () {
  if (running || !active)
    return;

  var dx, dy, dist, newX, newY, coord;
  var p = active[0];
  var c = active[1];
  if (c == 1) {
    coord = d3.mouse(p.circle1.node());
    newX = coord[0] - offX;
    newY = coord[1] - offY;
    dx = newX;
    dy = newY;
    dist = Math.sqrt(dx * dx + dy * dy);
    if (dist > 10 && dist < 180) {
      p.l1 = dist;
      p.theta1 = Math.atan2(newX, newY);
      updatePendulum(p);
    }
  } else {
    coord = d3.mouse(p.circle2.node());
    newX = coord[0] - offX;
    newY = coord[1] - offY;
    dx = newX - p.line2.attr("x1");
    dy = newY - p.line2.attr("y1");
    dist = Math.sqrt(dx * dx + dy * dy);
    if (dist > 10 && dist < 180) {
      p.l2 = dist;
      p.theta2 = Math.atan2(dx, dy);
      updatePendulum(p);
    }
  }
}

function mouseUp () {
  active = null;
}

function mouseLeave () {
  active = null;
}

function createPendulum (svg) {
  var c1 = svg.append("circle")
      .attr("fill", "blue");
  var c2 = svg.append("circle")
      .attr("fill", "blue");
  var p = {
    svg: svg,
    dots: d3.select(svg.node().parentNode).select("#dots"),
    theta1: 0,
    theta2: 0,
    l1: 140,
    l2: 140,
    m1: 10,
    m2: 10,
    line1: svg.insert("line", ":first-child")
      .attr({
        stroke: "red",
        "stroke-width": 2,
        x1: 0,
        y1: 0
      }),
    line2: svg.insert("line", ":first-child")
      .attr({
        stroke: "red",
        "stroke-width": 2
      }),
    circle1: c1,
    circle2: c2
  };
  resetPendulum(p);
  c1.on({
    mousedown: mouseDown(p, 1),
    mouseup: mouseUp
  });
  c2.on({
    mousedown: mouseDown(p, 2),
    mouseup: mouseUp
  });
  return p;
}

function updatePendulum (p, drawDot) {
  var x2 = p.l1 * Math.sin(p.theta1),
      y2 = p.l1 * Math.cos(p.theta1);
  p.line1.attr({
    x2: x2,
    y2: y2
  });
  p.circle1.attr({
    cx: x2,
    cy: y2,
    r: p.m1
  });
  p.line2.attr({
    x1: x2,
    y1: y2
  });
  x2 = x2 + p.l2 * Math.sin(p.theta2);
  y2 = y2 + p.l2 * Math.cos(p.theta2);
  p.line2.attr({
    x2: x2,
    y2: y2
  });
  p.circle2.attr({
    cx: x2,
    cy: y2,
    r: p.m2
  });
  if (drawDot && running && tracing) {
    p.dots.append("circle")
      .attr({
        cx: x2,
        cy: y2,
        r: 1,
        class: "dot",
        fill: "#838383"
      });
  }
}

function copy () {
  if (running)
    return;
  removeDots();
  pend2.theta1 = pend1.theta1;
  pend2.theta2 = pend1.theta2;
  pend2.l1 = pend1.l1;
  pend2.l2 = pend1.l2;
  pend2.m1 = pend1.m1;
  pend2.m2 = pend1.m2;
  updatePendulum(pend2);
}

function createElements () {
  var w = window,
      d = document,
      e = d.documentElement,
      g = d.getElementsByTagName('body')[0],
      width = w.innerWidth || e.clientWidth || g.clientWidth,
      height = w.innerHeight|| e.clientHeight|| g.clientHeight;

  d3.select("body").selectAll("*").remove();
  d3.select("body").append("center");

  var size = Math.min(Math.round(height / 1.2), Math.round(width / 2.2));
  var sizeHalf = size / 2;
  function createSVG () {
    var svg = d3.select("center").append("svg")
      .on({
        mouseleave: mouseLeave,
        mousemove: mouseMove,
        mouseup: mouseUp
      })
      .attr({
        width: size,
        height: size
      });
    // disable standard interaction
    ["contextmenu", "drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave"].forEach(function (event) {
      svg.on(event, function () {
        d3.event.preventDefault();
      });
    });
    var g = svg.append("g")
      .attr("transform", "translate(" + sizeHalf + "," + sizeHalf + ") scale(" + (sizeHalf / 400) + ")");
    g.append("g").attr("id", "dots");
    return g.append("g");
  }
  svg1 = createSVG();
  svg2 = createSVG();

  pend1 = createPendulum(svg1);
  updatePendulum(pend1);
  pend2 = createPendulum(svg2);
  updatePendulum(pend2);

  var center = d3.select("body").append("center");
  center.append("input").attr({
    value: "Start/Stop",
    type: "button"
  }).on("click", toggle);
  center.append("input").attr({
    value: "Copy",
    type: "button"
  }).on("click", copy);
  center.append("input").attr({
    value: "Reset",
    type: "button"
  }).on("click", reset);
  center.append("input").attr({
    value: "Trace on/off",
    type: "button"
  }).on("click", toggleTrace);

  d3.select("body").append("center")
    .append("span")
    .style("font-size", "x-small")
    .html("Copyright (c) 2016, Prof. Dr. Edmund Weitz");
  d3.select("body").append("center")
    .append("span")
    .style("font-size", "x-small")
    .append("a")
    .attr("href", "http://weitz.de/imprint.html")
    .text("Impressum, Datenschutzerklärung");
}

function toggle () {
  if (running) {
    running = false;
    resetPendulum(pend1);
    resetPendulum(pend2);
    return;
  }
  running = true;
  movePendulums();
}

function movePendulum (p) {
  var mu = 1 + p.m1 / p.m2;
  p.d2theta1  =  (g * (Math.sin(p.theta2) * Math.cos(p.theta1 - p.theta2) - mu * Math.sin(p.theta1))
                  - (p.l2 * p.dtheta2 * p.dtheta2 + p.l1 * p.dtheta1 * p.dtheta1 * Math.cos(p.theta1 - p.theta2))
                  * Math.sin(p.theta1 - p.theta2))
    / (p.l1 * (mu - Math.cos(p.theta1 - p.theta2) * Math.cos(p.theta1 - p.theta2)));
  p.d2theta2  =  (mu * g * (Math.sin(p.theta1) * Math.cos(p.theta1 - p.theta2) - Math.sin(p.theta2))
                  + (mu * p.l1 * p.dtheta1 * p.dtheta1 + p.l2 * p.dtheta2 * p.dtheta2 * Math.cos(p.theta1 - p.theta2))
                  * Math.sin(p.theta1 - p.theta2)) / (p.l2 * (mu - Math.cos(p.theta1 - p.theta2) * Math.cos(p.theta1 - p.theta2)));
  p.dtheta1 += p.d2theta1 * time;
  p.dtheta2 += p.d2theta2 * time;
  p.theta1 += p.dtheta1 * time;
  p.theta2 += p.dtheta2 * time;
  updatePendulum(p, true);
}

function movePendulums () {
  if (!running)
    return;
  movePendulum(pend1);
  movePendulum(pend2);
  setTimeout(movePendulums, 10);
}

function removeDots () {
  var helper = function (svg) {
    d3.select(svg.node().parentNode).selectAll(".dot").remove();
  };
  helper(svg1);
  helper(svg2);
}

function resetPendulum (p, all) {
  p.dtheta1 = 0;
  p.dtheta2 = 0;
  p.d2theta1 = 0;
  p.d2theta2 = 0;
  if (all) {
    p.theta1 = 0;
    p.theta2 = 0;
    p.l1 = 140;
    p.l2 = 140;
    p.m1 = 10;
    p.m2 = 10;
  }
}

function adjustPendulum (p, factor) {
  p.dtheta1 = 0;
  p.dtheta2 = 0;
  p.d2theta1 = 0;
  p.d2theta2 = 0;
  p.theta1 = 0.4 * Math.PI;
  p.theta2 = factor * Math.PI;
  p.l1 = 160;
  p.l2 = 160;
  p.m1 = 10;
  p.m2 = 10;
}

function adjustPendulums () {
  if (running)
    return;
  removeDots();
  adjustPendulum(pend1, 0.615);
  adjustPendulum(pend2, 0.64);
  updatePendulum(pend1);
  updatePendulum(pend2);
}

function reset () {
  if (running)
    return;
  removeDots();
  resetPendulum(pend1, true);
  resetPendulum(pend2, true);
  updatePendulum(pend1);
  updatePendulum(pend2);
}

function toggleTrace () {
  if (tracing) {
    tracing = false;
    removeDots();
    return;
  }
  tracing = true;
}

window.onload = init;

// global keyboard handler
function keyHandler (e) {
  var unicode = e.keyCode? e.keyCode : e.charCode;
  switch (unicode) {
  case 112: // 'F1'
    adjustPendulums();
    break;
  }
}

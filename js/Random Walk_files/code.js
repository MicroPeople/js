// Copyright (c), Prof. Dr. Edmund Weitz, 2016

var g1, g2;
var scale = 4;
var MAX = 800 / scale;
var speed = 0.01;
var toggle = false;
var restart = false;
var cumulative = false;
var avg = new Array(MAX);
var curves = 0;
var typeToggle = false;
var type = 2;
var showDeviation = false;

function keyHandler (e) {
  switch (e.keyCode? e.keyCode : e.charCode) {
  case 50: // "2"
    typeToggle = 2;
    break;
  case 51: // "3"
    typeToggle = 3;
    break;
  case 82: // "r"
    if (!restart)
      restart = true;
    break;
  case 68: // "d"
    if (!cumulative) {
      showDeviation = !showDeviation;
      d3.select("#deviation1").attr("fill-opacity", showDeviation ? 0.3 : 0);
      d3.select("#deviation2").attr("fill-opacity", showDeviation ? 0.7 : 0);
    }
    break;
  case 67: // "c"
  case 32: // space
    if (!toggle)
      toggle = true;
    break;
  case 37: // left arrow
    if (speed >= 0.1)
      speed /= 10;
    break;
  case 39: // right arrow
    if (speed <= 10)
      speed *= 10;
    break;
  }
}

function init () {
  document.onkeydown = keyHandler;
  avg.fill(0);

  var main = d3.select("#main");
  main.selectAll("*").remove();
  var svg = main.append("svg");
  var width = window.innerWidth;
  var height = window.innerHeight;
  svg.style({
    width: width + "px",
    height: height + "px"
  });
  var grad = svg.append("defs").append("linearGradient").attr("id", "grad");
  grad.append("stop").attr({
    offset: "0%",
    "stop-color": "gray",
    "stop-opacity": "0.3"
  });
  grad.append("stop").attr({
    offset: "100%",
    "stop-color": "white",
    "stop-opacity": "0.3"
  });
  g1 = svg.append("g").attr("transform", "translate (" + (width - MAX * scale) / 2 + ", " + (0.3 * height) + ") scale(" + scale + ", -" + scale + ")");
  g1.append("line").attr({
    x1: 0,
    y1: 0,
    x2: MAX,
    y2: 0,
    stroke: "#b0b0ff",
    "stroke-width": (0.5 / scale)
  });
  g1.append("line").attr({
    x1: 0,
    y1: 2 * Math.sqrt(MAX),
    x2: 0,
    y2: -2 * Math.sqrt(MAX),
    stroke: "#b0b0ff",
    "stroke-width": (0.5 / scale)
  });
  var points = "";
  var x = 0;
  var y = 0;
  points += x + "," + y + " ";
  x = 2 * Math.sqrt(MAX);
  y = x;
  points += x + "," + y + " ";
  y = -x;
  points += x + "," + y + " ";
  x = 0;
  y = 0;
  points += x + "," + y + " ";
  g1.append("polyline").attr({
    points: points,
    stroke: "none",
    fill: "url(#grad)",
    "fill-opacity": 0,
    id: "deviation2"
  });
  points = "";
  for (var n = 0; n < 800; n++) {
    x = n / scale;
    y = Math.sqrt(x);
    points += x + "," + y + " ";
  }
  for (n = 799; n >= 0; n--) {
    x = n / scale;
    y = -Math.sqrt(x);
    points += x + "," + y + " ";
  }
  g1.append("polyline").attr({
    "stroke-width": (2 / scale),
    stroke: "none",
    fill: "#FFBB00",
    "fill-opacity": 0,
    points: points,
    id: "deviation1"
  });

  g2 = svg.append("g").attr("transform", "translate (" + (width - MAX * scale) / 2 + ", " + (0.9 * height) + ") scale(" + scale + ", -" + 2 * scale + ")");
  g2.append("line").attr({
    x1: 0,
    y1: 0,
    x2: MAX,
    y2: 0,
    stroke: "#b0b0ff",
    "stroke-width": (0.5 / scale)
  });
  g2.append("line").attr({
    x1: 0,
    y1: 1.3 * Math.sqrt(MAX),
    x2: 0,
    y2: 0,
    stroke: "#b0b0ff",
    "stroke-width": (0.5 / scale)
  });
  
  g2.append("polyline").attr({
    "stroke-width": (5 / scale),
    stroke: "orange",
    "stroke-opacity": 0,
    "stroke-linecap": "butt",
    fill: "none",
    id: "squareRoot"
  });
  squareRootCurve();
  g2.append("polyline").attr({
    points: "",
    "stroke-width": (1 / scale),
    stroke: "#375E97",
    "stroke-opacity": 1.0,
    fill: "none",
    id: "cumul"
  });

  var line = newLine();
  addToLine(line, 0, 0);
}

var factor = Math.sqrt(2 / Math.PI);

function sqFn (n) {
  return factor * Math.sqrt(n);
}

function squareRootCurve () {
  var points = "";
  for (var n = 0; n < 800; n++) {
    var x = n / scale;
    var y = sqFn(x);
    points += x + "," + y + " ";
  }
  g2.select("#squareRoot").attr({
    points: points
  });
}

function newLine () {
  return g1.append("polyline").attr({
    points: "0,0 ",
    "stroke-width": (1 / scale),
    stroke: cumulative ? "#FB6542" : "black",
    "stroke-opacity": 1.0,
    fill: "none",
    class: "line"
  });
}

function addToLine (line, x, y) {
  if (toggle || restart || typeToggle) {
    if (typeToggle == 3)
      factor = Math.sqrt(4 / 3 / Math.PI);
    else if (typeToggle == 2)
      factor = Math.sqrt(2 / Math.PI);
    if (typeToggle) {
      squareRootCurve();
      type = typeToggle;
    }
    g1.selectAll(".line").remove();
    if (toggle)
      cumulative = !cumulative;
    d3.select("#cumul").attr("points", "");
    if (cumulative) {
      avg.fill(0);
      curves = 0;
      d3.select("#squareRoot").attr("stroke-opacity", 0.4);
      d3.select("#deviation1").attr("fill-opacity", 0);
      d3.select("#deviation2").attr("fill-opacity", 0);
    } else {
      d3.select("#squareRoot").attr("stroke-opacity", 0);
      d3.select("#deviation1").attr("fill-opacity", showDeviation ? 0.3 : 0);
      d3.select("#deviation2").attr("fill-opacity", showDeviation ? 0.7 : 0);
    }
    line = newLine();
    x = 0;
    y = 0;
    toggle = false;
    restart = false;
    typeToggle = false;
  }

  var iter = speed <= 1 ? 1 : speed;
  var pointsAdd = "";
  for (var i = 0; i < iter; i++) {
    r = type == 3 ? Math.floor((Math.random() * 3) - 1) : 2 * Math.floor(Math.random() * 2) - 1;
    x += 1;
    pointsAdd += x + "," + y + " ";
    y += r;
    avg[x] = (curves * avg[x] + Math.abs(y)) / (curves + 1);
    pointsAdd += x + "," + y + " ";
    if (x > MAX)
      break;
  }
  if (cumulative) {
    var points = "0,0 ";
    var oldY = 0;
    for (i = 0; i < MAX; i++) {
      points += (i + 1) + "," + oldY + " ";
      points += (i + 1) + "," + avg[i] + " ";
      oldY = avg[i];
    }
    d3.select("#cumul").attr("points", points);
  }
  line.attr("points", line.attr("points") + pointsAdd);
  if (x > MAX) {
    if (cumulative) {
      curves++;
      line.attr({
        "stroke": "#909090",
        "stroke-opacity": 0.3
      });
      line = newLine();
    } else
      line.attr("points", "0,0 ");
    x = 0;
    y = 0;
  }  

  var timeOut = speed < 1 ? 1 / speed : 1;
  if (!cumulative && x > MAX && timeOut < 10)
    timeOut = 10;
  setTimeout(function () {
    addToLine(line, x, y);
  }, timeOut);
}

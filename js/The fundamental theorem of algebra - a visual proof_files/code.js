// Copyright (c) 2016, Dr. Edmund Weitz. All rights reserved.

var inc = Math.PI / 100;
var offset = Math.PI / 4;

var cos, sin, cosOff, sinOff;

function fillSinCos () {
  sin = new Float64Array(200);
  cos = new Float64Array(200);
  sinOff = new Float64Array(200);
  cosOff = new Float64Array(200);
  var angle = 0;
  for (var i = 0; i < 200; i++) {
    cos[i] = Math.cos(angle);
    sin[i] = Math.sin(angle);
    cosOff[i] = Math.cos(angle + offset);
    sinOff[i] = Math.sin(angle + offset);
    angle += inc;
  }
}

function createCircle (board, radius, color = "red", center = [0, 0]) {
  return board.create("circle", [center, radius], {
    strokeColor: color,
    strokeWidth: .3
  });
}

function createCurve (board, functionX, functionY, color = "grey", width = 0.3) {
  return board.create("curve", [functionX, functionY, 0, 2 * Math.PI], {
    strokeColor: color,
    strokeWidth: width
  });
}

function createPoint (board, coords, color = "black", options = {}) {
  return board.create("point", coords, {
    size: 1,
    fillColor: color,
    strokeColor: color,
    showInfobox: false,
    name: options.name || " ",
    label: options.label || null
  });
}

function createArrow (board, from, to, color, opacity = 1) {
  return board.create("arrow", [from, to], {
    strokeWidth: 1,
    strokeColor: color,
    strokeOpacity: opacity
  });
}

function updateBoards (boards) {
  boards.forEach(function (board) {
    board.updateQuality = board.BOARD_QUALITY_HIGH;
    board.update();
  });
}

function prepareBoard (id, options = {}) {
  var board = JXG.JSXGraph.initBoard(id, {
    boundingbox: (options.boundingbox || [-10, 10, 10, -10]),
    grid: false,
    axis: false,
    showCopyright: false,
    showNavigation: false,
    minimizeReflow: "none"
  });
  var xAxis = board.create('axis', [[0, 0], [1, 0]]);
  xAxis.removeAllTicks();
  board.create('ticks', [xAxis, [options.xTick || 1]], {
    majorHeight: 10,
    tickEndings: [0, 1],
    labels: [options.xLabel || "1"],
    drawLabels: true,
    label: { offset: options.xOffset || [-4, -15] }
  });
  var yAxis = board.create('axis', [[0, 0], [0, 1]]);
  yAxis.removeAllTicks();
  board.create('ticks', [yAxis, [options.yTick || 1]], {
    majorHeight: 10,
    tickEndings: [1, 0],
    labels: [options.yLabel || "i"],
    drawLabels: true,
    label: { offset: options.yOffset || [-12, 0] }
  });
  return board;
}

function initAddBoards () {
  var addBoard1 = prepareBoard ("addBox1");
  var p1w = createPoint(addBoard1, [6, 2], "black", {
    label: { offset: [7, 7] },
    name: "w"
  });
  var p1z = createPoint(addBoard1, [4, 5], "black", {
    label: { offset: [7, 7] },
    name: "z"
  });
  var addBoard2 = prepareBoard("addBox2", {
    boundingbox: [-20, 20, 20, -20]
  });
  addBoard1.addChild(addBoard2);
  var xCoord1 = function () { return p1w.X(); };
  var yCoord1 = function () { return p1w.Y(); };
  var xCoord2 = function () { return p1z.X(); };
  var yCoord2 = function () { return p1z.Y(); };
  var xCoordSum = function () { return p1w.X() + p1z.X(); };
  var yCoordSum = function () { return p1w.Y() + p1z.Y(); };
  createArrow(addBoard1, [0, 0], [xCoord1, yCoord1], "blue");
  createArrow(addBoard1, [0, 0], [xCoord2, yCoord2], "red");
  createPoint(addBoard2, [xCoordSum, yCoordSum], "black", {
    label: { offset: [7, 7] },
    name: "w + z"
  });
  createArrow(addBoard2, [0, 0], [xCoord1, yCoord1], "blue");
  createArrow(addBoard2, [0, 0], [xCoord2, yCoord2], "red");
  createArrow(addBoard2, [xCoord1, yCoord1], [xCoordSum, yCoordSum], "red");
  createArrow(addBoard2, [xCoord2, yCoord2], [xCoordSum, yCoordSum], "blue");
  var addBoard1Info = document.getElementById("addBox1Info");
  var addBoard2Info = document.getElementById("addBox2Info");
  var updateInfo = function () {
    addBoard1Info.innerHTML = "w = " + displayNum(xCoord1()) + " +  " + displayNum(yCoord1()) + "i<br>";
    addBoard1Info.innerHTML += "z = " + displayNum(xCoord2()) + " +  " + displayNum(yCoord2()) + "i";
    addBoard2Info.innerHTML = "w + z = " + displayNum(xCoordSum()) + " +  " + displayNum(yCoordSum()) + "i<br>&nbsp;";
  };
  updateInfo();
  addBoard1.on("update", updateInfo);
}

function initMultBoards () {
  var multBoard1 = prepareBoard ("multBox1", {
    boundingbox: [-4, 4, 4, -4]
  });
  var p1w = createPoint(multBoard1, [2.4, 1.2], "black", {
    label: { offset: [7, 7] },
    name: "w"
  });
  var p1z = createPoint(multBoard1, [3.2, 0.8], "black", {
    label: { offset: [7, 7] },
    name: "z"
  });
  var multBoard2 = prepareBoard ("multBox2", {
    boundingbox: [-16, 16, 16, -16]
  });
  multBoard1.addChild(multBoard2);
  var xCoord1 = function () { return p1w.X(); };
  var yCoord1 = function () { return p1w.Y(); };
  var xCoord2 = function () { return p1z.X(); };
  var yCoord2 = function () { return p1z.Y(); };
  var xCoordProd = function () { return p1w.X() * p1z.X() - p1w.Y() * p1z.Y(); };
  var yCoordProd = function () { return p1w.X() * p1z.Y() + p1w.Y() * p1z.X(); };
  createArrow(multBoard1, [0, 0], [xCoord1, yCoord1], "blue");
  createArrow(multBoard1, [0, 0], [xCoord2, yCoord2], "red");
  createPoint(multBoard2, [xCoordProd, yCoordProd], "black", {
    label: { offset: [7, 7] },
    name: "w &middot; z"
  });
  var xAxis = multBoard2.create("line", [[0, 0], [1,0]], {
    visible: false
  });
  var wLine = multBoard2.create("line", [[0, 0], [xCoord1, yCoord1]], {
    visible: false
  });
  var prodLine = multBoard2.create("line", [[0, 0], [xCoordProd, yCoordProd]], {
    visible: false
  });
  multBoard2.create("angle", [xAxis, wLine, 1, 1], {
    radius: 4,
    color: "blue",
    orthoType: "sector",
    name: " "
  });
  multBoard2.create("angle", [wLine, prodLine, 1, 1], {
    radius: 4,
    color: "red",
    orthoType: "sector",
    name: " "
  });
  createArrow(multBoard2, [0, 0], [xCoordProd, yCoordProd], "black");
  var multBoard1Info = document.getElementById("multBox1Info");
  var multBoard2Info = document.getElementById("multBox2Info");
  var updateInfo = function () {
    multBoard1Info.innerHTML = "z = " + displayNum(absP(p1z)) + " &middot; cis(" + displayNum(argP(p1z)) + ")<br>";
    multBoard1Info.innerHTML += "w = " + displayNum(absP(p1w)) + " &middot; cis(" + displayNum(argP(p1w)) + ")<br>";
    multBoard2Info.innerHTML = "w &middot; z = " + displayNum(absP(p1z) * absP(p1w), 1) + " &middot; cis("
      + displayNum(modPi(argP(p1z) + argP(p1w))) + ")<br>&nbsp;";
  };
  updateInfo();
  multBoard1.on("update", updateInfo);
}

function initRotateBoards () {
  var rotBoard1 = prepareBoard ("rotateBox1", {
    boundingbox: [-1.5, 1.5, 1.5, -1.5]
  });
  var slider = rotBoard1.create('slider', [[-1.4, -1.35], [-0.3, -1.35], [0.7, 1, 1.3]], {
    withTicks: false,
    withLabel: false,
    size: 4
  });
  var rotBoard2 = prepareBoard ("rotateBox2", {
    boundingbox: [-10, 10, 10, -10]
  });
  var rotBoard3 = prepareBoard ("rotateBox3", {
    boundingbox: [-10, 10, 10, -10]
  });
  var r = 1;
  var r2 = 2;
  var i = 0;
  var xCoord1 = function () { return r * cos[i]; };
  var yCoord1 = function () { return r * sin[i]; };
  var xCoord2 = function () { return r2 * cos[(i << 1) % 200]; };
  var yCoord2 = function () { return r2 * sin[(i << 1) % 200]; };
  var factor = Math.sqrt(18);
  var r3 = factor;
  var xCoord3 = function () { return r3 * cosOff[(3 * i) % 200]; }
  var yCoord3 = function () { return r3 * sinOff[(3 * i) % 200]; }
  createPoint(rotBoard1, [xCoord1, yCoord1], "red");
  createPoint(rotBoard2, [xCoord2, yCoord2], "green");
  createPoint(rotBoard3, [xCoord3, yCoord3], "blue");
  var radius0 = function () { return r; };
  var radius1 = function () { return r3; };
  var radius2 = function () { return r2; };
  createCircle(rotBoard1, radius0);
  createCircle(rotBoard3, radius1, "blue");
  createCircle(rotBoard2, radius2, "green");
  var boards = [rotBoard1, rotBoard2, rotBoard3];
  var action = function () {
    r = slider.Value();
    r2 = 2 * r * r;
    r3 = factor * r * r * r;
    i = (i + 2) % 200;
    updateBoards(boards);
    setTimeout(action, 40);
  };
  action();
}

function initCombinedBoards () {
  var combBoard1 = prepareBoard ("combinedBox1", {
    boundingbox: [-1.5, 1.5, 1.5, -1.5]
  });
  var slider = combBoard1.create('slider', [[-1.4, -1.35], [-0.3, -1.35], [0.7, 1, 1.3]], {
    withTicks: false,
    withLabel: false,
    size: 4
  });
  var combBoard2 = prepareBoard ("combinedBox2", {
    boundingbox: [-10, 10, 10, -10]
  });
  var combBoard3 = prepareBoard ("combinedBox3", {
    boundingbox: [-10, 10, 10, -10]
  });
  var r = 1;
  var i = 0;
  var xCoord1 = function () { return r * cos[i]; };
  var yCoord1 = function () { return r * sin[i]; };
  var factor = Math.sqrt(18);
  var r3 = factor;
  var r2 = 2;
  var radius0 = function () { return r; };
  var radius1 = function () { return r3; };
  var radius2 = function () { return r2; };
  var center = function () {
    return [
      r3 * cosOff[(3 * i) % 200],
      r3 * sinOff[(3 * i) % 200]
    ];
  };
  createCircle(combBoard1, radius0);
  createCircle(combBoard2, radius1, "blue");
  createCircle(combBoard2, radius2, "green", center);
  var curveX = function (phi) { return r2 * Math.cos(2 * phi) + r3 * Math.cos(3 * phi + offset); };
  var curveY = function (phi) { return r2 * Math.sin(2 * phi) + r3 * Math.sin(3 * phi + offset); };
  var xCoord2 = function () { return r2 * cos[(i << 1) % 200] + r3 * cosOff[(3 * i) % 200]; };
  var yCoord2 = function () { return r2 * sin[(i << 1) % 200] + r3 * sinOff[(3 * i) % 200]; };
  var xCoord3 = function () { return r3 * cosOff[(3 * i) % 200]; };
  var yCoord3 = function () { return r3 * sinOff[(3 * i) % 200]; };
  createArrow(combBoard2, [0, 0], [xCoord3, yCoord3], "blue", 0.3);
  createArrow(combBoard2, [xCoord3, yCoord3], [xCoord2, yCoord2], "green", 0.3);
  createCurve(combBoard3, curveX, curveY, "orange", 0.4);
  createPoint(combBoard1, [xCoord1, yCoord1], "red");
  createPoint(combBoard2, [xCoord2, yCoord2], "orange");
  createPoint(combBoard3, [xCoord2, yCoord2], "orange");
  var boards = [combBoard1, combBoard2, combBoard3];
  var action = function () {
    r = slider.Value();
    r3 = factor * r * r * r;
    r2 = 2 * r * r;
    i = (i + 1) % 200;
    updateBoards(boards);
    setTimeout(action, 40);
  };
  action();
}

function initFullBoards () {
  var fullBoard1 = prepareBoard ("fullBox1", {
    boundingbox: [-5.3, 5.3, 5.3, -5.3]
  });
  var r = 2;
  var r2 = 8;
  var r4 = 32;
  var i = 0;
  var slider = fullBoard1.create('slider', [[-4.7, -4.7], [-1, -4.7], [3, 3.4, 5]], {
    withTicks: false,
    withLabel: false,
    size: 4
  });
  var fullBoard2 = prepareBoard ("fullBox2", {
    boundingbox: [-1700, 1700, 1700, -1700],
    xTick: 500,
    yTick: 500,
    xLabel: "500",
    yLabel: "500i",
    xOffset: [-10, -15],
    yOffset: [-35, 0]
  });
  var xCoord1 = function () { return r * cos[i]; };
  var yCoord1 = function () { return r * sin[i]; };
  createPoint(fullBoard1, [xCoord1, yCoord1], "red");
  var factor = Math.sqrt(18);
  var r3 = factor * 8;
  var offset = Math.PI / 4;
  var outerCurveX = function (phi) { return r4 * Math.cos(4 * phi); };
  var outerCurveY = function (phi) { return r4 * Math.sin(4 * phi); };
  var innerCurveX = function (phi) {
    return 2 - 5 * r * Math.cos(phi) + r2 * Math.cos(2 * phi) + r3 * Math.cos(3 * phi + offset) + r4 * cos[(i << 2) % 200];
  };
  var innerCurveY = function (phi) {
    return - 1 - 5 * r * Math.sin(phi) + r2 * Math.sin(2 * phi) + r3 * Math.sin(3 * phi + offset) + r4 * sin[(i << 2) % 200];
  };
  createCurve(fullBoard2, innerCurveX, innerCurveY, "orange");
  createCurve(fullBoard2, outerCurveX, outerCurveY, "blue");
  var radius0 = function () { return r; };
  var radius = function () { return 0.5 * r4; };
  createCircle(fullBoard1, radius0, "red");
  fullBoard2.create("circle", [[0, 0], radius], {
    fillColor: "grey",
    fillOpacity: 0.3,
    strokeColor: "grey",
    strokeOpacity: 0.1
  });
  xCoord2 = function () { 
    return 2 - 5 * r * cos[i] + r2 * cos[(i << 1) % 200] + r3 * cosOff[(3 * i) % 200] + r4 * cos[(i << 2) % 200];
  };
  yCoord2 = function () {
    return - 1 - 5 * r * sin[i] + r2 * sin[(i << 1) % 200] + r3 * sinOff[(3 * i) % 200] + r4 * sin[(i << 2) % 200];
  };
  createPoint(fullBoard2, [xCoord2, yCoord2]);
  var boards = [fullBoard1, fullBoard2];
  var action = function () {
    r = slider.Value();
    r4 = 2 * r * r * r * r;
    r3 = factor * r * r * r;
    r2 = 2 * r * r;
    i = (i + 1) % 200;
    updateBoards(boards);
    setTimeout(action, 40);
  };
  action();
}

function initRootBoards () {
  var rootBoard1 = prepareBoard ("rootBox1", {
    boundingbox: [-5.3, 5.3, 5.3, -5.3]
  });
  var r = 2;
  var r2 = 8;
  var r4 = 32;
  var i = 0;
  var slider = rootBoard1.create('slider', [[-4.7, -4.4], [4.7, -4.4], [0, 4.8, 5]], {
    withTicks: false,
    withLabel: false,
    size: 4
  });
  var rootBoard2 = prepareBoard ("rootBox2", {
    boundingbox: [-1700, 1700, 1700, -1700],
    xTick: 500,
    yTick: 500,
    xLabel: "500",
    yLabel: "500i",
    xOffset: [-10, -15],
    yOffset: [-35, 0]
  });
  var rootBoard3 = prepareBoard ("rootBox3", {
    boundingbox: [-5, 5, 5, -5]
  });
  var xCoord1 = function () { return r * cos[i]; };
  var yCoord1 = function () { return r * sin[i]; };
  createPoint(rootBoard1, [xCoord1, yCoord1], "red");
  var factor = Math.sqrt(18);
  var r3 = factor * 8;
  var offset = Math.PI / 4;
  var curveX = function (phi) {
    return 2 - 5 * r * Math.cos(phi) + r2 * Math.cos(2 * phi) + r3 * Math.cos(3 * phi + offset) + r4 * Math.cos(4 * phi);
  };
  var curveY = function (phi) {
    return - 1 - 5 * r * Math.sin(phi) + r2 * Math.sin(2 * phi) + r3 * Math.sin(3 * phi + offset) + r4 * Math.sin(4 * phi);
  };
  createCurve(rootBoard2, curveX, curveY);
  createCurve(rootBoard3, curveX, curveY);
  var radius0 = function () { return r; };
  createCircle(rootBoard1, radius0);
  function xCoord2 () {
    return 2 - 5 * r * cos[i] + r2 * cos[(i << 1) % 200] + r3 * cosOff[(3 * i) % 200] + r4 * cos[(i << 2) % 200];
  }
  function yCoord2 () {
    return - 1 - 5 * r * sin[i] + r2 * sin[(i << 1) % 200] + r3 * sinOff[(3 * i) % 200] + r4 * sin[(i << 2) % 200];
  }
  createPoint(rootBoard2, [xCoord2, yCoord2]);
  createPoint(rootBoard3, [xCoord2, yCoord2]);
  createPoint(rootBoard3, [2, -1], "green");
  var boards = [rootBoard1, rootBoard2, rootBoard3];
  var action = function () {
    r = slider.Value();
    r2 = 2 * r * r;
    r3 = factor * r * r * r;
    r4 = r2 * r * r;
    i = (i + 1) % 200;
    updateBoards(boards);
    setTimeout(action, 40);
  };
  action();
}

function modPi (x) {
  while (x > Math.PI)
    x -= Math.PI;
  while (x <= -Math.PI)
    x += Math.PI;
  return x;
}

function absP (point) {
  return Math.sqrt(point.X() * point.X() + point.Y() * point.Y());
}

function argP (point) {
  return Math.atan2(point.Y(), point.X());
}

function displayNum (num, add) {
  add = add || 0;
  var str = num.toFixed(2);
  if (num >= 0)
    str = "&nbsp;" + str;
  while (add > 0) {
    add--;
    str = "&nbsp;" + str;
  }
  return str;
}

function initPage () {
  fillSinCos();
  initAddBoards();
  initMultBoards();
  initRotateBoards();
  initCombinedBoards();
  initFullBoards();
  initRootBoards();
}

window.onload = initPage;
